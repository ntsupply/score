<?php

namespace Score
{
    use Score\Server;

    /**
     * Validate Class used to take strings and numbers and validate with common uses
     * @author Ron Rebennack
     * @author Bradley Worrell-Smith
     */
    class Validate
    {
        /**
         * Check to see if it is a float and if it calls within the range
         * @param mixed $iFloat
         * @param int $iPrecision
         * @param int $iScale
         * @return bool
         */
        public static function isFloat($iFloat, $iPrecision, $iScale)
        {
            if (!is_numeric($iFloat))
                return false;

            $iFloat = (float)$iFloat;

            $xMax = (float)str_pad("", $iPrecision - $iScale, '9') . '.' . str_pad("", $iScale, '9');
            $xMin = (float)"-$xMax";

            if(($iFloat < $xMin) || ($iFloat > $xMax))
                return false;

            return true;
        }

        /**
         * Checks to see if the value is a valid date
         * @param DateTime $dateToCheck
         * @return boolean
         */
        public static function isDate($dateToCheck)
        {
            try
            {
                if($dateToCheck instanceof \DateTime)
                {
                    return true;
                }
                else
                {
                    $dataArray = date_parse($dateToCheck);

                    if(isset($dataArray["error_count"]) && $dataArray["error_count"] == 0)
                    {
                        return true;
                    }
                }
            }
            catch(\Exception $ex)
            {
                //Do nothing and the function will return false
            }

            return false;
        }

        /**
         * Validate the Email Address
         * @param string $iEMail
         * @return bool
         */
        public static function isEMail($iEMail)
        {
            $xRet = preg_match('/\A(?:[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z/i', $iEMail);

            return ($xRet > 0);
        }

        /**
         * Is this a local/LAN ip address
         * @param string $ip IP address ex 10.21.2.5 or ::1
         * @return bool
         */
        public static function isLANIP($ip = null)
        {
            if (is_null($ip))
                $ip = Server::GetClientIP();

            return !filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE);
        }

        public static function IsPhone($phoneNumber)
        {
            $retRes = preg_match('/^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$/', $phoneNumber);

            return ($retRes > 0);
        }

        public static function IsCreditCard($creditCardNumber)
        {
            $retRes = preg_match('/^((67\d{2})|(4\d{3})|(5[1-5]\d{2})|(6011))-?\s?\d{4}-?\s?\d{4}-?\s?\d{4}|3[4,7]\d{13}$/', $creditCardNumber);

            return ($retRes > 0);
        }
    }
}