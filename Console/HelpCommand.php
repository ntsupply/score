<?php

namespace Score\Console
{
    class HelpCommand extends \Score\Console\Command
    {
        public function __construct($shellGUID)
        {
            parent::__construct($shellGUID, "help");
        }

        public function Execute(array $args = array())
        {
            if (empty($args))
            {
                $keys = array_keys($this->Shell()->Commands());
                $pc = ceil(count($keys) / 3);

                $col = 0;
                $row = 0;
                $lns = array();

                for ($lp = 0; $lp < count($keys); $lp++)
                {
                    if (!isset($lns[$row]))
                    {
                        $lns[$row] = "";
                    }

                    $lns[$row] .= str_pad($keys[$lp], 20);

                    if ($lp % $pc == 0)
                    {
                        $row = 0;
                        $col++;
                    }
                    else
                    {
                        $row++;
                    }
                }

                foreach ($lns as $ln)
                {
                    \Score\Console::WriteLine($ln);
                }
            }
        }
    }
}