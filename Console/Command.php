<?php

namespace Score\Console
{
    class Command
    {
        const EXIT_GOOD = 0;
        const EXIT_WARN = 8;
        const EXIT_ERROR = 12;
        const EXIT_HALT = -1;

        public $name;
        public $anon;
        protected $_shell;

        public function __construct($shellGUID, $name = null, $anonymous = null)
        {
            if (!empty($name))
            {
                $this->name = $name;
            }

            $this->anon = $anonymous;
            $this->_shell = $shellGUID;
        }

        public function &Shell()
        {
            return \Score\Container::globalGet($this->_shell);
        }

        public function Execute(array $args = array())
        {
            if (!empty($this->anon))
            {
                $anon = $this->anon;
                return $anon($args);
            }

            return self::EXIT_GOOD;
        }
    }
}