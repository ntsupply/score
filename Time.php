<?php

namespace Score
{
    use Score\Strings;

    /**
     * Date/Time functions
     *
     * @author Bradley Worrell-Smith
     */
    class Time extends \DateTime
    {
        const EXCEPTION_BADDT = "(100101) Bad Date/Time string or object.";
        const EXCEPTION_BADTZ = "(100102) Bad Date Timezone string or object.";

        const DIFF_DAYS = "%d";
        const DIFF_YEARS = "%y";
        const DIFF_MONTHS = "%m";
        const DIFF_HOURS = "%h";
        const DIFF_MINUTES = "%i";
        const DIFF_SECONDS = "%s";
        const DIFF_AGG_DAYS = "%a";
        const DIFF_AGG_MONTHS = "%am";
        const DIFF_AGG_HOURS = "%ah";
        const DIFF_AGG_MINUTES = "%ai";
        const DIFF_AGG_SECONDS = "%as";

        const COMPARE_EQ = 0;
        const COMPARE_LT = -1;
        const COMPARE_GT = 1;

        const FORMAT_AT = "m/d/Y @ g:i A";
        const FORMAT_DT = "m/d/Y";
        const FORMAT_DTS = "Y-m-d H:i:s";
        const FORMAT_OD = "Y-m-d";
        const FORMAT_TF = "Y-m-d His";
        const FORMAT_BTF = "Ymd_His";
        const FORMAT_TS = "Y-m-d H:i:s";
        const FORMAT_FD = "jS of F Y";           // Full date spelled out         | 7th of November 1974
        const FORMAT_FDD = "l F jS";             // Full date with Day            | Thursday November 7, 1974
        const FORMAT_UTC = "Y-d-m\TG:i:sz";
        const FORMAT_UNIX = "UNIX_TIMESTAMP";

        public $defaultFormat = "Y-m-d";

        /**
         * Construct of DateTime which extends PHP's DateTime object
         * @param type $time
         * @param type $timezone
         * @throws \Score\Exception
         */
        public function __construct($time = "now", $timezone = null)
        {
            if ( is_string($timezone) )
            {
                $timezone = new \DateTimeZone($timezone);
            }

            if ( !empty($timezone) && !$timezone instanceof \DateTimeZone )
            {
                throw new \Score\Exception(self::EXCEPTION_BADTZ);
            }

            parent::__construct($time, $timezone);
        }

        /**
         * Quick Date/Time Stamp string method for the datetime of now.
         * @param type $iIncludeTime
         * @return string Formatted string of Now
         */
        public static function QuickStamp($iIncludeTime = true)
        {
            $xDT = new self();

            return $xDT->Format(($iIncludeTime ? "@DTS" : "@OD"));
        }

        /**
         * Quick Format DateTime
         * @param mixed $iDate
         * @param string $iFormat
         * @return string
         */
        public static function QuickFormat($iDate, $iFormat = self::FORMAT_OD)
        {
            $tmpDate = new self($iDate);

            return $tmpDate->Format($iFormat);
        }

        /**
         * Compare two dates and return the indicator.
         * @param type $Date1 DateTime or string of DateTime
         * @param type $Date2 DateTime or string of DateTime
         * @return int DateTime::COMPARE_EQ = Same. DateTime::COMPARE_GT = Date1 > Date2.  DateTime::COMPARE_LT = Date1 < Date2.
         */
        public static function Compare($Date1, $Date2 = "now")
        {
            if ( !$Date1 instanceof self )
            {
                $Date1 = new self($Date1);
            }

            return $Date1->CompareTo($Date2);
        }

        /**
         * Compare this instance to another DateTime
         * @param type $Date2 DateTime or string of DateTime
         * @return int DateTime::COMPARE_EQ = Same. DateTime::COMPARE_GT = Date1 > Date2.  DateTime::COMPARE_LT = Date1 < Date2.
         */
        public function CompareTo($Date = "now")
        {
            if ( !$Date instanceof self )
            {
                $Date = new self($Date);
            }

            if ( $this > $Date )
            {
                return self::COMPARE_GT;
            }

            if ( $this < $Date )
            {
                return self::COMPARE_LT;
            }

            return self::COMPARE_EQ;
        }

        /**
         * Are the two dates passed are Equal
         * @param type $iDate1 DateTime or string of DateTime
         * @param type $iDate2 DateTime or string of DateTime
         * @return bool True/False if dates are Equal
         */
        public static function AreEqual($iDate1, $iDate2 = "now")
        {
            return (self::Compare($iDate1, $iDate2) == self::COMPARE_EQ);
        }

        /**
         * Get end of month
         * @param mixed $date Date string or object
         * @return \Score\Time with date set to end of month
         */
        public static function EndOfMonth($date = "now")
        {
            if ( !$date instanceof self )
            {
                $date = new self($date);
            }

            $date->setDay(1);
            $date->addMonth(1);
            $date->subDay(1);

            return $date;
        }

        /**
         * Get the start of month
         * @param mixed $date Date string or object
         * @return \Score\Time with date set to start of month
         */
        public static function StartOfMonth($date = "now")
        {
            if ( !$date instanceof self )
            {
                $date = new self($date);
            }

            $date->setDay(1);
            return $date;
        }

        /**
         * Check if this date is between two another datetimes
         * @param mixed $begDate
         * @param mixed $endDate
         * @return bool
         */
        public function isBetween($begDate, $endDate)
        {
            if ( !$begDate instanceof self )
            {
                $begDate = new self($begDate);
            }

            if ( !$endDate instanceof self )
            {
                $endDate = new self($endDate);
            }

            return ($begDate <= $this && $endDate >= $this);
        }

        /**
         * Check if a datetime is between two another datetimes
         * @param mixed $begDate
         * @param mixed $endDate
         * @param mixed $checkDate
         * @return bool
         */
        public static function Between($begDate, $endDate, $checkDate = "now")
        {
            if ( !$checkDate instanceof self )
            {
                $checkDate = new self($checkDate);
            }

            return $checkDate->isBetween($begDate, $endDate);
        }

        /**
         * Get the difference as specified as a string
         * @param string $date1 DateTime or string of DateTime
         * @param string $date2 DateTime or string of DateTime
         * @param const $returnType The return type as defined in Score\Time\DIFF_*
         * @return int Returns a number based on the inputs.  For example: This returns 12: Difference("11/07/1974", "11/19/1974", Time::DIFF_DAYS)
         */
        public static function Difference($date1, $date2 = "now", $returnType = self::DIFF_DAYS)
        {
            if ( !$date1 instanceof self )
            {
                $date1 = new self($date1);
            }

            return $date1->DiffTo($date2, $returnType);
        }

        /**
         * Basically the same as DateTime::diff but this returns formatted values instead of DateIntervals
         * @param string $date DateTime or string of DateTime
         * @param const $returnType The return type as defined in Score\Time\DIFF_*
         * @return int Returns a number based on the inputs.  For example: This returns 12: Difference("11/07/1974", "11/19/1974", Time::DIFF_DAYS)
         */
        public function DiffTo($date = "now", $returnType = self::DIFF_DAYS)
        {
            if ( !$date instanceof self )
            {
                $date = new self($date);
            }

            $derDiff = $this->diff($date);

            return $derDiff->format($returnType);
        }

        /**
         * Date difference override
         * @param mixed $date
         * @param bool $absolute
         * @return \Score\Time\Interval
         */
        public function diff($date, $absolute = false)
        {
            if ( !$date instanceof self )
            {
                $date = new self($date);
            }

            $orig = parent::diff($date, $absolute);

            $period = \str_replace('-', '', \Score\Strings::Write("P{0}Y{1}M{2}DT{3}H{4}M{5}S", $orig->y, $orig->m, $orig->d, $orig->h, $orig->i, $orig->s));
            $new = new \Score\Time\Interval($period);

            return $new;

        }

        /**
         * Janky replication of parent DateTime but as a \Score\Time object
         * @param string $format
         * @param string $time
         * @param DateTimeZone $dtz
         * @return \Score\Time|boolean
         */
        public static function createFromFormat($format, $time, $dtz = null)
        {
            if ($format == self::FORMAT_UNIX)
            {
                $date = new self();
                $date->setTimestamp($time);
                return $date;
            }

            if (empty($dtz))
            {
                $date = parent::createFromFormat($format, $time);
            }
            else
            {
                $date = parent::createFromFormat($format, $time, $dtz);
            }

            if ($date === false)
            {
                return false;
            }

            return new self($date->format(self::ISO8601));
        }

        /**
         * Add Days
         * @param int $iDays Number of Days
         */
        public function addDay($iDays)
        {
            return $this->add(new \DateInterval("P" . $iDays . "D"));
        }

        /**
         * Subtract Days
         * @param int $iDays Number of Days
         */
        public function subDay($iDays)
        {
            return $this->sub(new \DateInterval("P" . $iDays . "D"));
        }

        /**
         * Set the Day
         * @param int $iDay Day of current month/year
         */
        public function setDay($iDay)
        {
            $yr = (int)$this->Format("Y");
            $mo = (int)$this->Format("n");

            $this->setDate($yr, $mo, $iDay);
        }

        /**
         * Add Months
         * @param int $iMonths
         */
        public function addMonth($iMonths)
        {
            return $this->add(new \DateInterval("P" . $iMonths . "M"));
        }

        /**
         * Subtract Months
         * @param int $iMonths
         */
        public function subMonth($iMonths)
        {
            $this->sub(new \DateInterval("P" . $iMonths . "M"));
        }

        /**
         * Set Month
         * @param int $iMonth
         */
        public function setMonth($iMonth)
        {
            $yr = (int)$this->Format("Y");
            $day = (int)$this->Format("d");

            $this->setDate($yr, $iMonth, $day);
        }

        /**
         * Add Years
         * @param int $iYears
         */
        public function addYear($iYears)
        {
            return $this->add(new \DateInterval("P" . $iYears . "Y"));
        }

        /**
         * Subtract Years
         * @param int $iYears
         */
        public function subYear($iYears)
        {
            return $this->sub(new \DateInterval("P" . $iYears . "Y"));
        }

        /**
         * Set Year
         * @param int $iYear
         */
        public function setYear($iYear)
        {
            $mo = (int)$this->Format("n");
            $day = (int)$this->Format("d");

            $this->setDate($iYear, $mo, $day);
        }

        /**
         * Set default format that can be used when __toString() and null format() is used.
         * @param string $iFormat
         */
        public function setFormat($iFormat)
        {
            $this->defaultFormat = $iFormat;
        }

        /**
         * Alias of the built-in DateTime::format() but with optional string
         * @param string $format Optional formatted string.
         * @param bool $setDefault Optional format string passed as the default for future display calls.
         */
        public function Display($format = null, $setDefault = false)
        {
            if ($setDefault)
            {
                $this->defaultFormat = $format;
            }

            return $this->Format($format);
        }

        /**
         * Return a formatted Date/Time string. Use FORMAT_* constants for Quick use.
         * See PHP manual for detail.
         * @param string $format Date Formatting.
         */
        public function Format($format)
        {
            if ( empty($format) )
            {
                $format = $this->defaultFormat;
            }

            if ( $format == "@@" )
            {
                $format = "@AT";
            }

            if ( Strings::Begins($format, "@") )
            {
                foreach ( \Score\Helper::GetClassConstants($this) as $key => $val )
                {
                    if ( !Strings::Begins($key, "FORMAT_") )
                    {
                        continue;
                    }

                    if ( Strings::Compare($key, "FORMAT_" . substr($format, 1)) == 0 )
                    {
                        $format = $val;
                        break;
                    }
                }
            }

            // do this after the format constants loop incase of @UNIX
            if ($format == self::FORMAT_UNIX)
            {
                return $this->getTimestamp();
            }

            return parent::format($format);
        }

        /**
         * Default Formatted string
         * @return string
         */
        public function __toString()
        {
            return $this->Format(null);
        }
    }
}
