<?php

namespace Score\Strings
{
    class DList implements \Iterator, \Countable, \ArrayAccess
    {
        protected $_array;
        protected $_delimiter;

        /**
         * Creates the DList object
         * @param type $iString
         * @param type $iDelimiter
         */
        public function __construct($iString = null, $iDelimiter = ",")
        {
            if ( empty($iString) )
            {
                $this->_array = array();
            }
            else
            {
                if ( is_array($iString) )
                {
                    $this->_array = $iString;
                }
                else
                {
                    $this->_array = explode($iDelimiter, $iString);
                }
            }

            $this->_delimiter = $iDelimiter;
        }

        /**
         * Get the Array from the list
         * @return array
         */
        public function toArray()
        {
            return $this->_array;
        }

        /**
         * Return the delimited list
         * @return string
         */
        public function __toString()
        {
            return implode($this->_delimiter, $this->_array);
        }

        /**
         * Traverse to the beginning of the list
         */
        public function rewind()
        {
            reset($this->_array);
            $this->_position = 0;
        }

        /**
         * Get the current valid of the position in the list
         * @return mixed
         */
        public function current()
        {
            current($this->_array);
            return $this->_array[$this->_position];
        }

        /**
         * Get position in list
         * @return int
         */
        public function key()
        {
            return $this->_position;
        }

        /**
         * Next Position in list
         */
        public function next()
        {
            next($this->_array);
            $this->_position++;
        }

        /**
         * Traverse to the last record and return
         * @return mixed Last record in the Data Table or false if no record
         */
        public function end()
        {
            $item = end($this->_array);
            $this->_position = count($this->_array) - 1;

            return $item;
        }

        /**
         * Is element valid?
         * @return bool
         */
        public function valid()
        {
            return isset($this->_array[$this->_position]);
        }

        /**
         * Add a new item to the list from the given position. Ex: $dlist[] = "Hi"
         * @param int $offset  Position
         * @param array $value Array of column values
         * @return void
         */
        public function offsetSet($offset, $value)
        {
            if (!is_int($offset) && !is_null($offset))
            {
                return;
            }

            if (is_null($offset))
            {
                $this->_array[count($this->_array)] = $value;
            }
            else
            {
                $this->_array[($offset > count($this->_array) ? count($this->_array) : $offset)] = $value;
            }
        }

        /**
         * Whether a offset exists
         * @param mixed $offset The Position number
         * @return boolean Returns true on success or false on failure.
         */
        public function offsetExists($offset)
        {
            return isset($this->_array[$offset]);
        }

        /**
         * Remove the indexed item from the list Ex: unset()
         * @param type $offset The Position number
         */
        public function offsetUnset($offset)
        {
            unset($this->_array[$offset]);
        }

        /**
         * Offset to retrieve
         * @param mixed $offset The Position number
         * @return mixed Can return all value types.
         */
        public function offsetGet($offset)
        {
            if (!is_int($offset))
            {
                return null;
            }

            return isset($this->_array[$offset]) ? $this->_array[$offset] : null;
        }

        /**
         * Get the number items list the list, Impliments Countable. Can be used with count. Ex: count()
         * @return int Number of rows
         */
        public function count()
        {
            return count($this->_array);
        }

        /**
         * Get an item in the List
         * @param int    $iIndex     Item
         * @return string
         */
        public function GetItem($iIndex)
        {
            if ( empty($this->_array) || !is_int($iIndex) || $iIndex >= count($this->_array))
            {
                return null;
            }

            return $this->_array[$iIndex - 1];
        }

        /**
         * Does a list contain a specific item
         * @param string $iWhat        Item
         * @param bool $iCaseSensitive
         * @return bool
         */
        public function Contains($iWhat, $iCaseSensitive = false)
        {
            if ( empty($this->_array) || empty($iWhat))
            {
                return false;
            }

            if ($iCaseSensitive)
            {
                return in_array($iWhat, $this->_array);
            }

            return in_array(strtolower($iWhat), array_map('strtolower', $this->_array));
        }

        /**
         * Add a item to a string list
         * @param string $iWhat      Item to add
         * @return bool
         */
        public function Add($iWhat)
        {
            $this->_array[] = $iWhat;
            return true;
        }

        /**
         * Add a unique item to a string list
         * @param string $iWhat      Item to add
         * @param bool $iCaseSensitive
         * @return bool
         */
        public function AddUnique($iWhat, $iCaseSensitive = false)
        {
            if ( empty($iWhat) )
            {
                return false;
            }

            if ($this->Contains($iWhat, $iCaseSensitive))
            {
                return false;
            }

            $this->_array[] = $iWhat;
            return true;
        }

        /**
         * Remove an item from list.
         * @param string $iWhat    Item to remove
         * @param bool $iCaseSensitive
         * @return bool
         */
        public function Remove($iWhat, $iCaseSensitive = false)
        {
            $xFound = false;

            foreach ($this->_array as $xIndex => $xItem)
            {
                if (($iCaseSensitive && $xItem == $iWhat) || (!$iCaseSensitive && strtolower($xItem) == strtolower($iWhat)))
                {
                    $xFound = true;
                    unset($this->_array[$xIndex]);
                }
            }

            return $xFound;
        }

        /**
         * Replace items in a list
         * @param  string $iSearch    What looking for in list
         * @param  string $iReplace   What to replace with
         * @return bool
         */
        public function Replace($iSearch, $iReplace, $iCaseSensitive = false)
        {
            $xFound = false;

            foreach ($this->_array as &$xItem)
            {
                if (($iCaseSensitive && $xItem == $iSearch) || (!$iCaseSensitive && strtolower($xItem) == strtolower($iSearch)))
                {
                    $xFound = true;
                    $xItem = $iReplace;
                }
            }

            return $xFound;
        }
    }
}