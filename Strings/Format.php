<?php

namespace Score\Strings
{
    use \Score\Validate;

    /**
     * String Formatting Class
     * @author Ron Rebennack
     * @author Bradley Worrell-Smith
     */
    class Format
    {
        const MOD_CLASS = "C";
        const MOD_FUNCTION = "F";

        protected static $_modlist = null;

        /**
         * Check Modifiers array and add default
         * @return voild
         */
        protected static function _checkMods()
        {
            if (isset(self::$_modlist))
            {
                return;
            }

            \Score\Logger::ByGlobal(__METHOD__, "Load default modifications.");

            self::$_modlist = array(array("call" => __CLASS__, "type" => self::MOD_CLASS, "prefix" => "mod_"));
        }

        /**
         * Run a string against a specified modifier in the modifiers list
         * @param string $string   Base string to run modifier against
         * @param string $iModifier Name of modifier
         * @param string $iParams   String of parameters
         */
        public static function RunModifier(&$string, $iModifier, $iParams)
        {
            self::_checkMods();

            foreach (array_reverse(self::$_modlist, true) as $xMod)
            {
                $xRes = false;
                $xCall = $xMod["call"];

                if ($xMod["type"] == self::MOD_CLASS)
                {
                    $xCall .= "::" . $xMod["prefix"] . $iModifier;
                }
                else if (strtoupper($iModifier) != strtoupper($xMod["4mod"]))
                {
                    continue;
                }

                try
                {
                    if (is_callable($xCall))
                    {
                        $xParams = array();

                        if (!is_null($iParams))
                        {
                            $xParams = explode(",", $iParams);
                        }

                        array_unshift($xParams, null);
                        $xParams[0] =& $string;
                        $xRes = @call_user_func_array($xCall, $xParams);
                    }
                }
                catch (Score\Exception $err) { }

                if ($xRes === true)
                    break;
            }
        }

        /**
         * Add a Class containing static methods to the Modifier stack
         * @param string $iClass      Class Name
         * @param string $iFuncPrefix Prefix of methods in Class
         * @return void
         */
        public static function addModClass($iClass, $iFuncPrefix = "mod_")
        {
            self::_checkMods();
            self::$_modlist[] = array("call" => $iClass, "type" => self::MOD_CLASS, "prefix" => $iFuncPrefix);
        }

        /**
         * Add a function to the Modifier stack
         * @param callback $iCallback   A callback function or method
         * @param string $iForModifier  Name of modifier you are trying to catch
         * @return void
         */
        public static function addModFunc($iCallback, $iForModifier)
        {
            self::_checkMods();
            self::$_modlist[] = array("call" => $iCallback, "type" => self::MOD_FUNCTION, "4mod" => $iForModifier);
        }

        /**
         * Replacement for sprintf but with a friendlier syntax and cool thingies
         * @example Write("Hello \{42} {0}! My name is {1:caps}. I rock %{2:float(4,2)} of the time.!", "Ron", "Brad", 69.9)
         * @param string $string   String with formatting
         * @param string $iReplace1 Replacement value for {0}.
         * @return Formatted string value
         */
        public static function Write($string, $iReplace1 = null)
        {
            $xData = null;
            $xOffset = 0;
            $xValues = func_get_args();
            array_shift($xValues);

            while (preg_match('/(?P<brace>\\\\\{|\{)(?P<replace>(?P<Key>[0-9]|[1-9][0-9])|(?P<Key2>[0-9]|[1-9][0-9]):(?P<Mod>.*?))\}/simx', $string, $xData, PREG_OFFSET_CAPTURE, $xOffset) > 0)
            {
                // First entry contains the full match, now lets validate
                $xPos = $xData[0][1];
                $xNew = "";

                if ($xData[0][0][0] == "\\")
                {
                    $xNew = str_replace ("\\", "", $xData[0][0]);
                    $string = substr_replace($string, $xNew, $xPos, strlen($xData[0][0]));
                }
                else
                {
                    $xNew = (array_key_exists("Key2", $xData) ? $xData["Key2"][0] : $xData["Key"][0]);

                    $xNew = $xValues[$xNew];
                    $xParams = null;

                    if (array_key_exists("Mod", $xData))
                    {
                        $xMod = $xData["Mod"][0];
                        $xPPos = strpos($xMod, "(");

                        if ($xPPos)
                        {
                            $xPPos = (strlen($xMod) - $xPPos);
                            $xParams = substr($xMod, -(($xPPos - 1)), $xPPos - 2);
                            $xMod = substr($xMod, 0, $xPPos - 2);
                        }

                        self::RunModifier($xNew, $xMod, $xParams);
                    }

                    $string = substr_replace($string, $xNew, $xPos, strlen($xData[0][0]));
                }

                $xOffset = $xPos + strlen($xNew);
            }

            return $string;
        }

        /**
         * String formatter and replace based on a named array
         * @example WriteNamed("Hello \{{42}} {{msg}}! My name is {{name:caps}}. I rock %{{percent:float(4,2)}} of the time.!", array("msg" => "Ron", "name" => "Brad", "percent" => 69.9))
         * @param string $string   String with formatting
         * @param string $namedReplace Associative array of values to replace.
         * @return Formatted string value
         */
        public static function WriteNamed($string, array $namedReplace = array())
        {
            $data = null;
            $offset = 0;

            while (preg_match('/(?<!\\\\)\{\{(?P<key>.*?)\}\}/simx', $string, $data, PREG_OFFSET_CAPTURE, $offset) > 0)
            {
                // First entry contains the full match, now lets validate
                $pos = $data[0][1];
                $key = $data["key"][0];
                $mod = null;

                if (strpos($key, ":") !== false)
                {
                    $mod = substr($key, strpos($key, ":") + 1);
                    $key = substr($key, 0, strpos($key, ":"));
                }

                $new = (array_key_exists($key, $namedReplace) ? $namedReplace[$key] : "");
                $params = null;

                if (!empty($mod))
                {
                    $para_pos = strpos($mod, "(");

                    if ($para_pos)
                    {
                        $runmod = substr($mod, 0, $para_pos);
                        $para_pos = (strlen($mod) - $para_pos);
                        $params = substr($mod, -(($para_pos - 1)), $para_pos - 2);
                    }
                    else
                    {
                        $runmod = $mod;
                    }

                    self::RunModifier($new, $runmod, $params);
                }

                $string = substr_replace($string, $new, $pos, strlen($data[0][0]));

                $offset = $pos + strlen($new);
            }

            return $string;
        }

        /**
         * Built-In Modifier Mysql Escape String
         * @param type $string
         */
        public static function mod_mysqlesc(&$string)
        {
            if (function_exists("mysql_escape_string"))
            {
                $string = mysql_escape_string($string);
            }
            else
            {
                // ghetto way
                $replacements = array(
                    "\x00"=>'\x00',
                    "\n"=>'\n',
                    "\r"=>'\r',
                    "\\"=>'\\\\',
                    "'"=>"\'",
                    '"'=>'\"',
                    "\x1a"=>'\x1a'
                );

                $string = strtr($string,$replacements);
            }
        }

        /**
         * Built-In Modifier Padding
         * @param string $string String/Number to pad
         * @param int $iSize Length to pad to... total amount including the string.
         * @param string $iChar Character to pad
         * @return void
         */
        public static function mod_pad(&$string, $iSize, $iChar = null)
        {
            // sprintf should auto detect number, but does not, so check and set $iChar to zero if numeric
            if (is_numeric($string) && is_null($iChar))
            {
                $iChar = 0;
            }

            $string = sprintf("%" . (is_null($iChar) ? "" : "'$iChar") . $iSize . "s", $string);
        }

        /**
         * Built-In Modifier CAPS
         * @param string $string String to Capitalize
         * @return void
         */
        public static function mod_caps(&$string)
        {
            $string = strtoupper($string);
        }

        /**
         * Show integer formatted string as human readable bytes
         * @param string $string
         * @param int $iPrecision
         */
        public static function mod_bytes(&$string, $iPrecision = 2)
        {
            $size = (int)$string;
            $base = log($size) / log(1024);
            $suffixes = array('', 'k', 'MB', 'GB', 'TB');

            $string = round(pow(1024, $base - floor($base)), $iPrecision) . $suffixes[floor($base)];
        }

        /**
         * This function is a shortcut to the number_format function to convert a number to #,###.00 notation
         * @author Tyler Dishman
         * @param string $string
         * @param int $dec_places
         * @param string $dec_point
         * @param string $thous_separator
         * @return void
         */
        public function mod_dollars(&$string, $dec_places = 2, $dec_point=".", $thous_separator=",")
        {
            $val = round((double) $string, $dec_places);
            $string = number_format((double) $val, $dec_places, $dec_point, $thous_separator);
        }

        /**
         * Built-In Modifier Float
         * @param string $string    String to format
         * @param string $precision How many decimals including after the point
         * @param string $scale     How many decimals after the point
         * @return void
         */
        public static function mod_float(&$string, $precision, $scale)
        {
            if (!Validate::isFloat($string, $precision, $scale))
            {
                $string = "";
                return;
            }

            $string = sprintf("%01.{$scale}f", (float)$string);
        }

        /**
         * Format a telephone number
         * @author Derek Piper
         * @param string $string    String to format
         * @return void
         */
        public static function mod_phone(&$string, $intermediate = '-')
        {
            $telenum = preg_replace('~[^0-9x]~', '', $string);
            $regexp = "/
                (1)?\D*     # optional country code
                (\d{3})?\D* # optional area code
                (\d{3})\D*  # first three
                (\d{4})     # last four
                (?:\D+|$)   # extension delimiter or EOL
                (\d*)       # optional extension
                /x";
            preg_match($regexp, $telenum, $matches);
            if (!isset($matches[0]))
            {
                return $string;
            }

            $country = $matches[1];
            $area = $matches[2];
            $three = $matches[3];
            $four = $matches[4];
            $ext = $matches[5];

            $string = $three . $intermediate . $four;
            if (!empty($area))
            {
                $string = "($area) $string";
            }
            if (!empty($country))
            {
                $string = "+$country $string";
            }
            if (!empty($ext))
            {
                $string .= " x$ext";
            }

            return $string;
        }

        public static function mod_append (&$string, $append = null)
        {
            if (!empty($string))
            {
                $string .= $append;
            }
        }
    }
}
