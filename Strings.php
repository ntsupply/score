<?php

namespace Score
{
    /**
     * Strings class used that does almost everything and a bag of chips
     * @author Ron Rebennack
     * @author Bradley Worrell-Smith
     */
    class Strings
    {
        const SEARCH_BEGINS = 'b';
        const SEARCH_ENDS = 'e';
        const SEARCH_CONTAINS = 'c';

        const QUOTE_DOUBLE = '"';
        const QUOTE_SINGLE = '\'';
        const QUOTE_BACKQUOTE = '`';

        const BYTE_SUFFIX_WITHBYTES = 1;
        const BYTE_SUFFIX_NOBYTES = 2;

        /**
         * Check a string for another string using compare.
         * @param string $iString       Contents to search
         * @param string $iSearch       What to search for
         * @param string $iType         Where to search the string
         * @param bool $iCaseSensitive  Search the string with case sensitive
         * @return bool Did it find anything
         */
        protected static function _contains($iString, $iSearch, $iType = self::SEARCH_CONTAINS, $iCaseSensitive = true)
        {
            $cmpString = $iString;

            if ($iType == self::SEARCH_BEGINS)
            {
                $cmpString = substr($iString, 0, strlen($iSearch));
            }

            if ($iType == self::SEARCH_ENDS)
            {
                $cmpString = substr($iString, -(strlen($iSearch)));
            }

            if ($iCaseSensitive)
            {
                $cmpRes = strpos($cmpString, $iSearch);
            }
            else
            {
                $cmpRes = stripos($cmpString, $iSearch);
            }

            return $cmpRes !== false;
        }

        /**
         * Does the string start with the searched value.
         * @param string $iString
         * @param string $iSearch
         * @param bool $iCaseSensitive
         * @return bool
         */
        public static function Begins($iString, $iSearch, $iCaseSensitive = false)
        {
            return self::_contains($iString, $iSearch, self::SEARCH_BEGINS, $iCaseSensitive);
        }

        /**
         * Does the string end with the searched value
         * @param string $iString
         * @param string $iSearch
         * @param bool $iCaseSensitive
         * @return bool
         */
        public static function Ends($iString, $iSearch, $iCaseSensitive = false)
        {
            return self::_contains($iString, $iSearch, self::SEARCH_ENDS, $iCaseSensitive);
        }

        /**
         * Basic comparison, but easy to use than strcmp.
         * @param string $iLeft
         * @param string $iRight
         * @param bool $iCaseSensitive
         * @return int
         */
        public static function Compare($iLeft, $iRight, $iCaseSensitive = false)
        {
            return ($iCaseSensitive ? strcmp($iLeft, $iRight) : strcasecmp($iLeft, $iRight));
        }

        /**
         * Find if a string contains a part of another string
         * @param string $iString
         * @param string $iSearch
         * @return bool
         */
        public static function Contains($iString, $iSearch, $iCaseSensitive = false)
        {
            if (empty($iString) || is_null($iSearch))
            {
                return false;
            }

            return self::_contains($iString, $iSearch, self::SEARCH_CONTAINS, $iCaseSensitive);
        }

        /**
         * DEPRECIATED: Check string that starts with another string
         * Use String::Begins()
         * @param string $iString
         * @param string $iWhat
         * @return bool
         */
        public static function StartsWith($iString, $iWhat)
        {
            return self::Begins($iString, $iWhat);
        }

        /**
         * DEPRECIATED: Check string that ends with another string
         * Use Strings::Ends()
         * @param string $iString
         * @param string $iWhat
         * @return bool
         */
        public static function EndsWith($iString, $iWhat)
        {
            $xBeg = strlen($iWhat) * -1;
            $iWhat = strtolower($iWhat);
            $iString = strtolower($iString);

            return (substr($iString, $xBeg) === $iWhat);
        }

        /**
         * Return a human readable string from number of bytes
         * @param int $iNumber
         * @param int $byteSuffix
         * @return string
         */
        public static function NumberInBytes($iNumber, $byteSuffixType = self::BYTE_SUFFIX_WITHBYTES)
        {
            switch ($byteSuffixType)
            {
                case self::BYTE_SUFFIX_WITHBYTES:
                    $unit = array(" b", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
                    break;
                case self::BYTE_SUFFIX_NOBYTES:
                    $unit = array("", "K", "M", "G", "T", "P", "E", "Z", "Y");
                    break;
            }

            if ($iNumber == 0)
            {
                return("0 b");
            }

            return (round($iNumber / pow(1024, ($i = floor(log($iNumber, 1024)))), $i > 1 ? 2 : 0) . $unit[$i]);
        }

        /**
         * Convert string representation to bytes
         * @param type $string
         */
        public static function NumberFromBytes($string)
        {
            if (preg_match('/(?P<number>\d+)\s*(?P<suffix>[a-z]*)/ix', $string, $matches))
            {
                if (empty($matches["suffix"]))
                {
                    $matches["suffix"] = "b";
                }

                $unit = array("b", "k", "m", "g", "t", "p", "e", "z", "y");
                $pow = array_search(strtolower(substr($matches["suffix"], 0, 1)), $unit);

                if ($pow !== false)
                {
                    return $matches["number"] * ($pow === 0 ? 1 : pow(1024, $pow));
                }
            }

            return null;
        }

        /**
         * A count of elements with a stringed array
         * @param string $iList      List
         * @param string $iDelimiter Glue or delimiter of string
         * @return int
         */
        public static function ListCount($iList, $iDelimiter = ",")
        {
            if (empty($iList))
            {
                return 0;
            }

            return sizeof(explode($iDelimiter, $iList));
        }

        /**
         * Alias for Strings::ListEntry - Get an item of a stringed array
         * @param string $iList      List
         * @param string $iIndex     Item
         * @param string $iDelimiter Delimiter in string
         * @return string
         */
        public static function ListGetItem($iList, $iIndex, $iDelimiter = ",")
        {
            self::ListEntry($iList, $iIndex, $iDelimiter);
        }

        /**
         * Get an item of a stringed array
         * @param string $iList      List
         * @param string $iIndex     Item
         * @param string $iDelimiter Delimiter in string
         * @return string
         */
        public static function ListEntry($iList, $iIndex, $iDelimiter = ",")
        {
            if (empty($iList) || !is_numeric($iIndex))
            {
                return null;
            }

            $xA_Val = explode($iDelimiter, $iList);
            if ($iIndex >= count($xA_Val))
            {
                return null;
            }

            if ($iIndex < 0)
            {
                $iIndex = count($xA_Val) - 1;
            }

            return $xA_Val[$iIndex];
        }

        /**
         * Does a list contain a specific item
         * @param string $list      List
         * @param string $what      Item
         * @param string $delimiter Delimiter
         * @return bool
         */
        public static function ListContains($list, $what, $delimiter = ",", $trim = false)
        {
            if (empty($list) || empty($what))
            {
                return false;
            }

            if ($trim)
            {
                return in_array(strtolower($what), array_map(
                    function ($item)
                    {
                        return trim(strtolower($item));
                    }, explode($delimiter, $list)));
            }
            else
            {
                return in_array(strtolower($what), array_map('strtolower', explode($delimiter, $list)));
            }
        }

        /**
         * Add a item to a string list
         * @param string $list      List
         * @param string $what      Item to add
         * @param string $delimiter Delimiter
         * @return string
         */
        public static function ListAdd($list, $what, $delimiter = ",")
        {
            if (empty($list))
            {
                return $what;
            }

            $aList = explode($delimiter, $list);
            if (empty($aList))
            {
                $aList = array();
            }

            array_push($aList, $what);

            return implode($delimiter, $aList);
        }

        /**
         * Add a unique item to a string list
         * @param string $list      List
         * @param string $iWhat      Item to add
         * @param string $delimiter Delimiter
         * @return string
         */
        public static function ListAddUnique($list, $add, $delimiter = ",", $trim = false)
        {
            if (empty($add))
            {
                return $list;
            }

            if (empty($list))
            {
                return $add;
            }

            if (self::ListContains($list, $add, $delimiter, $trim))
            {
                return $list;
            }

            return self::ListAdd($list, $add, $delimiter);
        }

        /**
         *  Remove an item from a stringed list.
         * @param string $list      List
         * @param string $remove    Item to remove
         * @param string $delimiter Delimiter
         * @return string
         */
        public static function ListRemove($list, $remove, $delimiter = ",", $trim = false)
        {
            if (!self::ListContains($list, $remove, $delimiter))
            {
                return $list;
            }

            if ($list == $remove)
            {
                return null;
            }

            $aList = explode($delimiter, $list);

            foreach ($aList as $key => &$item)
            {
                $item = strtolower($item);
                if ($trim)
                {
                    $item = trim($item);
                }

                if ($item == strtolower($remove))
                {
                    unset($aList[$key]);
                }
            }

            return implode($delimiter, $aList);
        }

        /**
         * Replace items in a list
         * @param  string $list      List
         * @param  string $search    What looking for in list
         * @param  string $replace   What to replace with
         * @param  string $delimiter Delimiter
         * @return string
         */
        public static function ListReplace($list, $search, $replace, $delimiter = ",", $trim = false)
        {
            if (!self::ListContains($list, $search, $delimiter))
            {
                return $list;
            }

            $aList = explode($delimiter, $list);

            foreach ($aList as &$item)
            {
                $item = strtolower($item);
                if ($trim)
                {
                    $item = trim($item);
                }

                if ($item == strtolower($search))
                {
                    $item = $replace;
                }
            }

            return implode($delimiter, $aList);
        }

        /**
         * Combine 2 list into 1
         * @param string $iList1
         * @param string $iList2
         * @param string $iDelimiter
         * @return string
         */
        public static function ListCombine($iList1, $iList2, $iDelimiter = ",")
        {
            if (empty($iList1) && empty($iList2))
            {
                return null;
            }

            if (empty($iList1))
            {
                return $iList2;
            }

            if (empty($iList2))
            {
                return $iList1;
            }

            $xA_Dude1 = explode($iDelimiter, $iList1);
            $xA_Dude2 = explode($iDelimiter, $iList2);

            $xA_New1 = array_diff($xA_Dude1, $xA_Dude2);
            $xA_New2 = array_diff($xA_Dude2, $xA_Dude1);
            $xA_New3 = array_intersect($xA_Dude1, $xA_Dude2);

            return implode($iDelimiter, array_merge($xA_New1, $xA_New2, $xA_New3));
        }

        /**
         *  Hiccup a string
         * @param string $iString     Source String
         * @param bool   $iCapFirst   Force capitalize first character?
         * @param type   $iAllowedVal Allowed Characters
         * @return string Modified string value
         */
        public static function Hiccup($iString, $iCapFirst = true, $iAllowedVal = 'A-Za-z0-9')
        {
            $iString = \preg_replace_callback('/([a-zA-Z])([a-zA-Z]*)/m', function ($replace) { return strtoupper($replace[1]) . strtolower($replace[2]);}, $iString);
            $iString = \preg_replace('/[^'.$iAllowedVal.']+/', '', $iString);
            $iString = \preg_replace_callback('/^[a-zA-Z]/m', function ($replace) use ($iCapFirst) { return ($iCapFirst ? strtoupper($replace[0]) : strtolower($replace[0]));}, $iString);
            return $iString;
        }

        /**
         * Replacement for sprintf but with a friendlier syntax and cool thingies
         * @example Write("Hello \{42} {0}! My name is {1:caps}. I rock %{2:float(4,2)} of the time.!", "Ron", "Brad", 69.9)
         * @param string $string   String with formatting
         * @param string $iReplace1 Replacement value for {0}.
         * @return Formatted string value
         */
        public static function Write($string, $iReplace1 = null)
        {
            return call_user_func_array("\Score\Strings\Format::Write", func_get_args());
        }

        /**
         * String formatter and replace based on a named array
         * @example WriteNamed("Hello \{{42}} {{msg}}! My name is {{name:caps}}. I rock %{{percent:float(4,2)}} of the time.!", array("msg" => "Ron", "name" => "Brad", "percent" => 69.9))
         * @param string $string   String with formatting
         * @param string $namedReplace Associative array of values to replace.
         * @return Formatted string value
         */
        public static function WriteNamed($string, array $namedReplace = array())
        {
            return \Score\Strings\Format::WriteNamed($string, $namedReplace);
        }

        /**
         *
         * @param string $iString    Source String
         * @param string $iQuoteType Quote Type
         * @param bool   $iTrim      Trim the Quotes off
         * @return string new string value
         */
        public static function Quote($iString, $iQuoteType = self::QUOTE_DOUBLE, $iTrim = false)
        {
            if (is_null($iQuoteType))
            {
                $iQuoteType = self::QUOTE_DOUBLE;
            }

            return $iQuoteType . ($iTrim ? trim($iString, $iQuoteType) : $iString) . $iQuoteType;
        }

        /**
         * Explode a string into an Array. Much like PHP's but it will support Associative array
         * @param string $string        Base Sting
         * @param string $delimiter     Glue or delimiter used between items
         * @param string $pairDelimiter Glue or delimiter used between key and value of each item
         * @return bool|array The array of items. False if not properly formatted or error.
         */
        public static function Explode($string, $delimiter = ",", $pairDelimiter = null)
        {
            $base = explode($delimiter, $string);

            if ($base === false)
            {
                return false;
            }

            if (empty($pairDelimiter))
            {
                return $base;
            }

            $data = array();
            foreach ($base as $item)
            {
                $kv = explode($pairDelimiter, $item);

                if ($kv !== false)
                {
                    if (count($kv) == 1)
                    {
                        $data[$kv[0]] = true;
                    }

                    if (count($kv) == 2)
                    {
                        $data[$kv[0]] = $kv[1];
                    }
                }
            }

            return $data;
        }

        /**
         * Implode an array into a string with optional key/value pairs
         * @param array $array
         * @param string $delimiter     Glue or delimiter used between items
         * @param string $pairDelimiter Glue or delimiter used between key and value of each item
         * @return string
         */
        public static function Implode(array $array = array(), $delimiter = ",", $pairDelimiter = null)
        {
            if (!empty($pairDelimiter))
            {
                array_walk($array, function (&$item, $key) use ($pairDelimiter) { $item = $key . $pairDelimiter . $item; });
            }

            return implode($delimiter, $array);
        }

        /**
         * Return unicode char by its code
         * @param int $u
         * @return char
         * @author voromax
         */
        public static function UniChr($u)
        {
            return mb_convert_encoding('&#x' . intval($u) . ';', 'UTF-8', 'HTML-ENTITIES');
        }

        /**
         * Substitute string with another, but preserve case
         * @param string $old
         * @param string $new
         * @return string
         * @author Mike <http://stackoverflow.com/users/340355/mike>
         */
        public static function Substitute($old, $new)
        {
            $mask = strtoupper($old) ^ $old;
            return strtoupper($new) | $mask . str_repeat(substr($mask, -1), strlen($new) - strlen($old));
        }

        /**
         * Returns a decimal number based on a faction given in form of a string
         * The string cannot contain anything other than forwardslash (/) and integers
         *
         * ex : "19 3/4"
         *
         * @param string $str
         * @return float|int
         * @author LukePOLO
         */
        public static function getDeci($str)
        {
            // Clean out bad characters
            $str = preg_replace("%[^\d/ \\.]%", "", $str);

            if(!is_numeric($str))
            {
                // Try to match a fraction, if so convert it!
                if (preg_match('/(\d+)\s+(\d+)\/(\d+)/', $str, $match))
                {
                    return $match[1] + $match[2] / $match[3];
                }
                elseif (preg_match('/(\d+)\/(\d+)/', $str, $match))
                {
                    return $match[1] / $match[2];
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return $str * 1;
            }
        }

        /**
         * Encode into nested objects or arrays and encode all strings.
         * @param mixed $input  Object or variaible to encode
         * @return void
         * @author Oscar Broman
         * @link http://php.net/manual/en/function.utf8-encode.php#109965
         */
        public static function EncodeUTF8Deep(&$input)
        {
            if (is_string($input))
            {
                $input = utf8_encode($input);
            }
            elseif (is_array($input))
            {
                foreach ($input as &$value)
                {
                    self::EncodeUTF8Deep($value);
                }

                unset($value);
            }
            elseif (is_object($input))
            {
                $vars = array_keys(get_object_vars($input));

                foreach ($vars as $var)
                {
                    self::EncodeUTF8Deep($input->$var);
                }
            }
        }

        /**
         * Pretty much the same as json_encode, but calls makes sure it is utf8 sanitized
         * @param string $input
         * @param int $options
         */
        public static function EncodeJsonUTF8($input, $options = null)
        {
            self::EncodeUTF8Deep($input);
            return json_encode($input, $options);
        }

        /*
         * Strip out anything not numeric. Optionally allow other characters
         * e.g. stripNonNumeric('float string', false, '.')
         * e.g. stripNonNumeric('float negative string', false, '.-')
         * @author Derek Piper
         * @param string $inText
         * @param boolean allowAlpha
         * @param string allowChars
         */
        public static function stripNonNumeric($inText, $allowAlpha = false, $allowChars = '')
        {
            $pattern = '~[^0-9';
            if ($allowAlpha)
            {
                $pattern .= 'a-zA-Z';
            }
            $pattern .= $allowChars . ']~';
            return preg_replace($pattern, '', str_replace('\\', '', stripslashes(trim($inText))));
        }

    }
}
