<?php

namespace Score
{
    use \Score\Router\Route;

    /**
     * Router class for Dispatching to the controller
     * @author Bradley Worrell-Smith
     */
    class Router
    {
        protected $_rebase;
        protected $_fallback = null;
        protected $_cacheWkr;
        protected $_dispatcher;
        /**
         * @var \Score\Router\Cache
         */
        protected $_cache;

        protected static $_params;

        CONST _INDEX_KEYS = "keys";

        /**
         * @param string $rebase
         * @param string $cacheKey
         * @param string $cacheNewerDate
         */
        public function __construct($rebase = null, $cacheKey = null, $cacheNewerDate = null)
        {
            $this->setFallback(array(new \Score\Router\Fallback(), "serve404"));
            $this->_cache = new \Score\Router\Cache($cacheKey, $cacheNewerDate);

            if (!empty($rebase))
            {
                $this->_rebase = '/' . ltrim($rebase, '/');
            }
        }

        /**
         * Setup a callback function if dispatch doesn't find any matches
         * @param mixed $callback
         */
        public function setFallback($callback)
        {
            if (!is_callable($callback))
            {
                // @todo Add exception here
            }

            $this->_fallback = $callback;
        }

        /**
         * Router Cache Object
         * @return \Score\Router\Cache
         */
        public function &Cache()
        {
            return $this->_cache;
        }

        /**
         * Are the current routes cached
         * @return bool
         */
        public function Cached()
        {
            return $this->Cache()->isCached();
        }

        /**
         * Add a route object to the router
         * @param \Score\Router\Route $route
         */
        public function addRoute(Route $route)
        {
            $entry = array_search($route->path, $this->Cache()->index[self::_INDEX_KEYS]);

            if ($entry === false)
            {
                $entry = sizeof($this->Cache()->routes);
                $method = strtolower($route->getOption(Route::OPT_ACCEPT_METHOD));

                if (empty($method))
                {
                    $method = 'any';
                }

                switch ($route->getOption(Route::OPT_ACCEPT_SCHEMA))
                {
                    case Route::SCHEMA_HTTP_ONLY:
                        $method .= "_http";
                        break;
                    case Route::SCHEMA_HTTPS_ONLY:
                        $method .= "_https";
                        break;
                }

                $this->Cache()->addIndex(self::_INDEX_KEYS, $entry, $route->path);

                if ($route->getOption(Route::OPT_PATTERN_MATCH))
                {
                    $this->Cache()->addIndex(array("pattern_{$method}", "pattern_base"), $route->match, $entry);
                }
                elseif ($route->getOption(Route::OPT_CASE_SENSITIVE) === Route::CASE_SENSITIVE)
                {
                    $this->Cache()->addIndex(array("abs_path_{$method}", "abs_path_base"), $route->match, $entry);
                }
                else
                {
                    $this->Cache()->addIndex(array("lc_path_{$method}", "lc_path_base"), strtolower($route->match), $entry);
                }
            }

            $this->Cache()->routes[$entry] = $route;
        }

        /**
         * Add a route
         * @param Route|string $path
         * @param type $controller
         * @param array $params
         * @param array $options
         * @return void
         * <code>
         *   $router->add("# POST https+ /hash_means_something/{p=int,n=id}/", 'SomethingController::do_something');
         * </code>
         */
        public function add($path, $controller = null, array $params = array(), array $options = array())
        {
            if ($path instanceof Route)
            {
                $this->addRoute($path);
                return;
            }

            $this->addRoute(new Route($path, $controller, $params, $options));
        }

        /**
         * Get the uri with the route
         * @param string $uri Optional: URI to parse for routing
         * @param string $method Optional: Method of the request
         * @param string $scheme Optional: Scheme of the request
         * @return \Score\Router\Info Breakdown of what a Requ resolves to
         */
        public function getRouteRequestInfo($uri = null, $method = null, $scheme = null)
        {
            $limit = 1;
            $script = filter_input(INPUT_SERVER, "SCRIPT_NAME");

            if (empty($uri))
            {
                $query = filter_input(INPUT_SERVER, "QUERY_STRING");
                $path = filter_input(INPUT_SERVER, "REQUEST_URI"); // use SCRIPT_URL because QUERY STRING is appended in SCRIPT_URI

                if (empty($scheme))
                {
                    $scheme = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] ? 'https' : 'http');
                }
            }
            else
            {
                $parse = parse_url($uri);
                $path = $parse["path"];
                $query = $parse["query"];

                if (empty($scheme))
                {
                    $scheme = $parse["scheme"];
                }
            }

            $route = $path;
            $result = new \Score\Router\Info();
            $result->script = $script;
            $result->query = $query;
            $result->request = $path;
            $result->method = strtolower(empty($method) ? $_SERVER["REQUEST_METHOD"] : $method);

            if (empty($scheme))
            {
                $scheme = substr($_SERVER["SERVER_PROTOCOL"], 0, strpos($_SERVER["SERVER_PROTOCOL"], '/'));
            }

            $result->scheme = strtolower($scheme);

            if (!empty($query))
            {
                $path = str_replace($query, '', $path);
            }

            $path = rtrim($path, "?");

            $route = preg_replace("%//+%", "/", pathinfo($script, PATHINFO_DIRNAME));
            $route = str_replace("/", "\\", $route);
            $route = str_replace($route, '', $path, $limit);
            // $result["base"] = $route;

            if (!empty($this->_rebase))
            {
                $route = str_replace($this->_rebase, "", $route, $limit);
                $result->rebase = $this->_rebase;
            }

            $result->route = $route;
            // 2015-09-13 Lets not trim slash and let the
            // $route = ($route == '/' ? $route : rtrim($route, '/'));

            return $result;
        }

        /**
         * Get Route Entry
         * @param int $entry
         * @return \Score\Router\Route
         */
        public function getRoute($entry)
        {
            if ($entry === false)
            {
                return false;
            }

            return $this->Cache()->routes[$entry];
        }

        /**
         * Dispatcher
         * @param bool $renew Force to create a new instance of the Dispatcher
         * @return \Score\Router\Dispatcher
         */
        public function Dispatcher($renew = false)
        {
            if ($renew === true || empty($this->_dispatcher))
            {
                $this->_dispatcher = new \Score\Router\Dispatcher($this);
            }

            return $this->_dispatcher;
        }

        /**
         * Dispatch the Router to find the correct Route/Controller
         * @param string $uri Optional: URI to route
         */
        public function Dispatch($uri = null)
        {
            $dispatcher = $this->Dispatcher();
            $dispatcher->Resolve($uri);

            $route = $dispatcher->getInfo()->route;

            $dispatch = $dispatcher->Dispatch($this);

            if ($dispatch === false)
            {
                \Score\Logger::ByGlobal(__METHOD__, "No matches found for {$route}.  Using fallback function");
                call_user_func($this->_fallback, $dispatcher);
            }
        }
    }
}
