<?php

namespace Score
{
    /**
     * Simpel Encryption class and helpers
     */
    class Encryption
    {
        const CYPHER = MCRYPT_RIJNDAEL_256;
        const MODE = MCRYPT_MODE_CBC;

        /**
         * Create a MS Compatible GUID
         * @param string $namespace
         * @param bool   $braces
         * @author redtraider@gmail.com
         * @author Twitter: @andris
         */
        public static function CreateGUID($namespace = "", $braces = true, $useCOM = true)
        {
            if (function_exists("com_create_guid") && $useCOM)
            {
                $guid = com_create_guid();

                if ($braces === true)
                {
                   $guid = "{$guid}";
                }
            }
            else
            {
                $guid = "";
                $uid = uniqid("", true);
                $data = $namespace;

                if ( isset($_SERVER['HTTP_USER_AGENT']) && !empty($_SERVER['HTTP_USER_AGENT']) )
                {
                    $data .= $_SERVER['HTTP_USER_AGENT'];
                }

                $data .= $_SERVER['REQUEST_TIME'];
                $data .= \Score\Server::GetClientIP();
                $data .= mt_rand(0, 1000000);
                $data .= mt_rand(0, 1000000);
                $hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
                //$hash = strtoupper(md5($data));
                $guid = substr($hash,  0,  8) . '-' .
                        substr($hash,  8,  4) . '-' .
                        substr($hash, 12,  4) . '-' .
                        substr($hash, 16,  4) . '-' .
                        substr($hash, 20, 12);

                if ($braces)
                {
                    $guid = '{' . $guid . '}';
                }
            }

            return $guid;
        }

        public static function Encrypt($key, $plaintext)
        {
            $td = mcrypt_module_open(self::CYPHER, '', self::MODE, '');
            $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
            mcrypt_generic_init($td, $key, $iv);
            $crypttext = mcrypt_generic($td, $plaintext);
            mcrypt_generic_deinit($td);

            return base64_encode($iv . $crypttext);
        }

        public static function Decrypt($key, $crypttext)
        {
            $crypttext = base64_decode($crypttext);
            $plaintext = '';
            $td = mcrypt_module_open(self::CYPHER, '', self::MODE, '');
            $ivsize = mcrypt_enc_get_iv_size($td);
            $iv = substr($crypttext, 0, $ivsize);
            $crypttext = substr($crypttext, $ivsize);

            if ( $iv )
            {
                mcrypt_generic_init($td, $key, $iv);
                $plaintext = mdecrypt_generic($td, $crypttext);
            }

            return trim($plaintext);
        }
    }
}