<?php

namespace Score\Image
{
    abstract class Layer
    {
        protected $_width;
        protected $_height;

        public function __construct($width = null, $height = null)
        {
            $this->_width = $width;
            $this->_height = $height;
        }

        abstract public function apply(\Score\Image &$img, $dst_x = 1, $dst_y = 1);
    }
}