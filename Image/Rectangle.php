<?php

namespace Score\Image
{
    class Rectangle extends \Score\Image\Layer
    {
        protected $_fc;
        protected $_bc;
        protected $_bt;
        protected $_br = array(self::TOP_LEFT => 0, self::TOP_RIGHT => 0, self::BOTTOM_LEFT => 0, self::BOTTOM_RIGHT => 0);

        const TOP_LEFT = "tl";
        const TOP_RIGHT = "tr";
        const BOTTOM_LEFT = "bl";
        const BOTTOM_RIGHT = "br";

        public function __construct($width, $height = null)
        {
            $height = (empty($height) ? $width : $height);
            parent::__construct($width, $height);
        }

        public function setBorder($thickness, $color, $radius = 0)
        {
            if (empty($thickness))
            {
                $this->_bc = null;
                $this->_bt = 0;
            }
            else
            {
                $this->_bt = $thickness;
                $this->_bc = $color;
            }

            if (!is_array($radius))
            {
                $radius = \array_fill_keys(array_keys($this->_br), $radius);
            }

            foreach ($radius as $loc => $val)
            {
                if (isset($this->_br[$loc]))
                {
                    $this->_br[$loc] = $val;
                }
            }
        }

        public function setColor($color)
        {
            $this->_fc = $color;
        }

        public function apply(\Score\Image &$img, $dst_x = 1, $dst_y = 1)
        {
            if ($this->_bt && $this->_bc)
            {
                $new = new \Score\Image($this->_width, $this->_height);
                $bc = $new->getColor($this->_bc);

                imagesetthickness($new->resource(), $this->_bt);

                // top border
                $x2 = ($this->_width - $this->_br[self::TOP_RIGHT]);
                imageline($new->resource(), $this->_br[self::TOP_LEFT], 1, $x2, 1, $bc);

                // bottom border
                $y2 = $this->_height - 1;
                $x2 = ($this->_width - ($this->_br[self::BOTTOM_LEFT] + $this->_br[self::BOTTOM_RIGHT]));
                imageline($new->resource(), $this->_br[self::BOTTOM_LEFT], $y2, $this->_width - $this->_br[self::BOTTOM_LEFT], $y2, $bc);

                // left border
                imageline($new->resource(), 1, $this->_br[self::TOP_LEFT], 1, ($this->_height - $this->_br[self::BOTTOM_LEFT]), $bc);
                
                // right border
                imageline($new->resource(), $this->_width - 1, $this->_br[self::TOP_RIGHT], $this->_width - 1, $this->_height - $this->_br[self::BOTTOM_RIGHT], $bc);

                // top-left arc
                $r = $this->_br[self::TOP_LEFT];
                imagearc($new->resource(), $r, $r, $r*2, $r*2, 180, 270, $bc);

                // top-right arc
                $r = $this->_br[self::TOP_RIGHT];
                imagearc($new->resource(), $this->_width - $r, $r, $r*2, $r*2, 270, 0, $bc);

                // bottom-right arc
                $r = $this->_br[self::BOTTOM_RIGHT];
                imagearc($new->resource(), $this->_width - $r, $this->_height - $r, $r*2, $r*2, 0, 90, $bc);

                // bottom-left arc
                $r = $this->_br[self::BOTTOM_LEFT];
                imagearc($new->resource(), $r, $this->_height - $r, $r*2, $r*2, 90, 180, $bc);

                if ($this->_fc)
                {
                    $fc = $new->getColor($this->_fc);
                    imagefilltoborder($new->resource(), $this->_width / 2, $this->_height / 2, $bc, $fc);
                }

                $img->addLayer($new, $dst_x, $dst_y);
            }
            else
            {
                $fc = $img->getColor($this->_fc);
                imagefilledrectangle($img->resource(), $dst_x, $dst_y, $dst_x + $this->_width, $dst_y + $this->_height, $fc);
            }
        }
    }
}
