<?php

namespace Score\Image
{
    class Resource extends \Score\Image\Layer
    {
        protected $_image;
        protected $_tc = -1;
        protected $_ab = false;

        const SCALE_PERCENT = 1;
        const SCALE_PIXELS_AUTO = 2;
        const SCALE_PIXELS_WIDTH = 3;
        const SCALE_PIXELS_HEIGHT = 4;

        const X_ALIGN_CENTER = "center";
        const X_ALIGN_LEFT = "left";
        const X_ALIGN_RIGHT = "right";

        const Y_ALIGN_TOP = "top";
        const Y_ALIGN_MIDDLE = "middle";
        const Y_ALIGN_BOTTOM = "bottom";

        public function __construct($width = null, $height = null)
        {
            parent::__construct($width, $height);
            $this->_reset();
        }

        public function __destruct()
        {
            \imagedestroy($this->_image);
        }

        public function &resource()
        {
            return $this->_image;
        }

        protected function _populateDetail()
        {
            if (!is_resource($this->_image))
            {
                return;
            }
        }

        protected function _reset($image = null)
        {
            if (is_resource($this->_image))
            {
                \imagedestroy($this->_image);
            }

            if (is_resource($image))
            {
                $this->_image = $image;
            }
            else
            {
                $this->_image = $this->_blank();
            }

            $this->_populateDetail();
        }

        protected function _blank($nw = null, $nh = null)
        {
            if (empty($nw))
            {
                $nw = empty($this->_width) ? 1 : $this->_width;
            }

            $nh = (empty($nh) ? (empty($this->_height) ? 1 : $this->_height) : $nh);
            
            $new = \imagecreatetruecolor($nw, $nh);
/*
            $bg = \imagecolorallocatealpha($new, 255, 255, 255, 127);

            \imagefill($new, 0, 0, $bg);
            */
            \imagealphablending($new, false);
            \imagesavealpha($new, true);

            return $new;
        }

        public function width()
        {
            return (is_resource($this->_image) ? \imagesx($this->_image) : 1);
        }

        public function height()
        {
            return (is_resource($this->_image) ? \imagesy($this->_image) : 1);
        }

        public function rotate($deg)
        {
            error_log("degrees $deg");
            \imagerotate($this->resource(), $deg, 0);
        }

        public function getColor($red, $green = null, $blue = null, $alpha = false)
        {
            if ($red instanceof \Score\Image\Color)
            {
                $c = $red;
            }
            else
            {
                $c = new \Score\Image\Color($red, $blue, $green, $alpha);
            }

            return $c->alloc($this);
        }

        public function fillColor($color, $dst_x = 0, $dst_y = 0)
        {
            $c = $this->getColor($color);
            \imagefill($this->resource(), $dst_x, $dst_y, $c);
        }

        public function getTransparentColor()
        {
            return $this->_tc;
        }

        public function setTransparentColor($color)
        {
            $this->_tc = $color;
            \imagecolortransparent($this->resource(), $this->_tc);
        }

        public function crop($cropWidth, $cropHeight, $x = self::X_ALIGN_CENTER, $y = self::Y_ALIGN_MIDDLE) {
            $width = imagesx($this->_image);
            $height = imagesy($this->_image);

            $horizontalAlignPixels = (is_numeric($x) ? [$x, $width] : self::_calculatePixelsForAlign($width, $cropWidth, $x));
            $verticalAlignPixels = (is_numeric($y) ? [$y, $height] : self::_calculatePixelsForAlign($height, $cropHeight, $y));

            $new = \imageCrop($this->_image, [
                'x' => $horizontalAlignPixels[0],
                'y' => $verticalAlignPixels[0],
                'width' => $horizontalAlignPixels[1],
                'height' => $verticalAlignPixels[1]
            ]);

            if ($new !== false)
            {
                $this->_image = $new;
                return true;
            }

            return false;
        }
        
        protected static function _calculatePixelsForAlign($imageSize, $cropSize, $align) {
            switch ($align) {
                case self::X_ALIGN_LEFT:
                case self::Y_ALIGN_TOP:
                    return [0, min($cropSize, $imageSize)];
                case self::X_ALIGN_RIGHT:
                case self::Y_ALIGN_BOTTOM:
                    return [max(0, $imageSize - $cropSize), min($cropSize, $imageSize)];
                case self::X_ALIGN_CENTER:
                case self::Y_ALIGN_MIDDLE:
                    return [
                        max(0, floor(($imageSize / 2) - ($cropSize / 2))),
                        min($cropSize, $imageSize),
                    ];
                default: return [0, $imageSize];
            }
        }        
        public function rescale($scale, $type = self::SCALE_PIXELS_AUTO)
        {
            $width = $this->width();
            $height = $this->height();

            switch ($type)
            {
                case self::SCALE_PERCENT:
                    $size = intval($width * (abs($scale) / 100));
                    $use = self::SCALE_PIXELS_WIDTH;
                    break;
                case self::SCALE_PIXELS_AUTO:
                    $use = ($width >= $height ? self::SCALE_PIXELS_WIDTH : self::SCALE_PIXELS_HEIGHT);
                default:
                    $size = intval(abs($scale));
            }

            if ($use == self::SCALE_PIXELS_WIDTH)
            {
                $nw = $size;
                $nh = ($nw / $width) * $height;
            }
            else
            {
                $nh = $size;
                $nw = ($nh / $height) * $width;
            }

            $new = $this->_blank($nw, $nh);

            \imagecopyresampled($new, $this->resource(), 0, 0, 0, 0, $nw, $nh, $width, $height);

            $this->_reset($new);
        }

        public function setAlphaBlending($on = true)
        {
            $this->_ab = $on;
        }

        public function apply(\Score\Image &$img, $dst_x = 1, $dst_y = 1)
        {
            $pct = 100;

            if ($this->_ab)
            {
                \imagealphablending($img->resource(), false);
            }

            if ($this->_tc == -1)
            {
                \imagesavealpha($img->resource(), true);
            }

            $src_w = $this->width();
            $src_h = $this->height();

            $cut = \imagecreatetruecolor($src_w, $src_h);

            // copying relevant section from background to the cut resource 
            imagecopy($cut, $img->resource(), 0, 0, $dst_x, $dst_y, $src_w, $src_h); 

            // copying relevant section from watermark to the cut resource 
            imagecopy($cut, $this->resource(), 0, 0, 0, 0, $src_w, $src_h); 

            // insert cut resource to destination image 
            imagecopymerge($img->resource(), $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
             

//            imagecopy($this->_image, $img->Handle(), $dst_x, $dst_y, 0, 0, $src_w, $src_h); 

        }
    }
}
