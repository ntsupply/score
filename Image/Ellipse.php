<?php

namespace Score\Image
{
    class Ellipse extends \Score\Image\Layer
    {
        protected $_c;

        public function __construct($width, $height = null)
        {
            $height = (empty($height) ? $width : $height);
            parent::__construct($width, $height);
        }

        public function setColor($color)
        {
            $this->_c = $color;
        }

        public function apply(\Score\Image &$img, $dst_x = 1, $dst_y = 1)
        {
            $c = $img->getColor($this->_c);
            \imagefilledellipse($img->resource(), $dst_x, $dst_y, $this->_width, $this->_height, $c);
        }
    }
}