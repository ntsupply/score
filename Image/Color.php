<?php

namespace Score\Image
{
    class Color
    {
        protected $red;
        protected $blue;
        protected $green;
        protected $alpha = false;

        public function __construct($red, $green = null, $blue = null, $alpha = false)
        {
            $this->set($red, $green, $blue, $alpha);
        }

        public function set($red, $green = null, $blue = null, $alpha = false)
        {
            if (\Score\Strings::Begins($red, "#"))
            {
                $red = self::hex2rgb($red);
            }

            if (is_array($red) && sizeof($red) == 3)
            {
                list($red,$green,$blue) = array_values($red);
            }

            $this->red = $red;
            $this->blue = $blue;
            $this->green = $green;
            $this->alpha = $alpha;
        }

        public function alloc(\Score\Image &$img)
        {
            $c = \imagecolorallocate($img->resource(), $this->red, $this->green, $this->blue);

            if ($this->alpha !== false)
            {
                $c = \imagecolorallocatealpha($img->resource(), $this->red, $this->green, $this->blue, $this->alpha);
            }

            return $c;
        }

        public function getAlpha()
        {
            return $this->alpha;
        }

        public static function random(\Score\Image &$img, $alloc = false)
        {
             // picking the unique colour
            $found = false;
            while($found == false) {
                $r = rand(0, 255);
                $g = rand(0, 255);
                $b = rand(0, 255);

                if(\imagecolorexact($img->resource(), $r, $g, $b) != (-1)) {
                    $found = true;
                }
            }

            $new = new self($r, $b, $g);

            if ($alloc)
            {
                $new->alloc($img);
            }

            return $new;
        }

        public static function hex2rgb($hex)
        {
            return sscanf(trim($hex, '#'), "%02x%02x%02x");
        }

        public static function fromHex($hex)
        {
            return new self(self::hex2rgb($hex));
        }

    }
}