<?php

namespace Score\Image
{
    /**
     * @todo add free type font support
     */
    class Text extends \Score\Image\Layer
    {
        protected $_text;
        protected $_spacing = 1;
        protected $_angle;
        protected $_align;
        protected $_size;
        protected $_fontfile;
        protected $_color;
        protected $_shaddowColor;

        const ALIGN_LEFT = 1;
        const ALIGN_RIGHT = 2;
        const ALIGN_CENTER = 3;

        public function setText($text, $justify = self::ALIGN_LEFT, $angle = 0)
        {
            $this->_text = $text;
            $this->_angle = $angle;
            $this->_align = $justify;
        }

        public function setFont($size, $fontfile = null, $spacing = null)
        {
            $this->_size = $size;

            if (!empty($fontfile))
            {
                $this->_fontfile = $fontfile;
            }

            if (!empty($spacing))
            {
                $this->_spacing = $spacing;
            }
        }

        public function setColor($color, $shaddowColor = null)
        {
            $this->_color = $color;
            $this->_shaddowColor = $shaddowColor;
        }

        public function apply(\Score\Image &$img, $dst_x = 1, $dst_y = 1)
        {
            $ln = 0;
            $body = explode("\n", $this->_text);
            $shaddowColor = null;

            $textColor = $img->getColor($this->_color);
            if ($this->_shaddowColor)
            {
                $shaddowColor = $img->getColor($this->_shaddowColor);
            }

            imageSaveAlpha($img->resource(), true);
            imageAlphaBlending($img->resource(), false);

            if (!$img->getTransparentColor())
            {
                $tc = \Score\Image\Color::random($img);
                $img->setTransparentColor($tc);
            }

            imageAlphaBlending($img->resource(), true);

            foreach ($body as $paragraph)
            {
                $words = explode(' ', $paragraph);
                $string = "";

                foreach ($words as $word)
                {
                    $teststring = $string . ' ' . $word;
                    $testbox = \imagettfbbox($this->_size, $this->_angle, $this->_fontfile, $teststring);

                    $string .= ($string == "" ? "" : ($testbox[2] > $this->_width ? "\n" : " ")) . $word;
                }

                foreach (explode("\n", $string) as $line)
                {
                    $newY = $dst_y + ($ln * $this->_size * $this->_spacing);
                    $newX = $dst_x;

                    $alignBox = imagettfbbox($this->_size, $this->_angle, $this->_fontfile, $line);
                    $width = abs($alignBox[4] - $alignBox[0]);

                   // @todo add alignment

                    switch ($this->_align)
                    {
                        case self::ALIGN_CENTER:
                            $newX = $newX + ($this->_width / 2) - ($width / 2);
                            break;
                        case self::ALIGN_RIGHT:
                            $newX = $newX + $this->_width - $width;
                            break;
                    }

                    if ($shaddowColor)
                    {
                        \imagettftext($img->resource(), $this->_size, $this->_angle, $newX + 1, $newY + 1, $shaddowColor, $this->_fontfile,  $line);
                    }

                    \imagettftext($img->resource(), $this->_size, $this->_angle, $newX, $newY, $textColor, $this->_fontfile,  $line);

                    $ln++;
                }
            }

        }
    }
}