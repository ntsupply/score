<?php

namespace Score
{

    use \Score\Strings;

    /**
     * System and OS specfic calls and helpers
     *
     * @author Ron Rebennack
     * @author Bradley Worrell-Smith
     */
    class Server
    {
        const EXCEPTION_LOOPERROR = "Looped too many times trying to create a unique file name";
        const GETVAR_ALL = 0; // Only includes GET, POST, COOKIE, and CARG
        const GETVAR_GET = 1;
        const GETVAR_POST = 2;
        const GETVAR_COOKIE = 3;
        const GETVAR_CARG = 4;
        const GETVAR_ENV = 5;
        const GETVAR_SESSION = 6;
        const MK_DIR = 1;
        const MK_FILE = 2;
        const DEFAULT_CHMOD = 0777;

        /**
         * Is the OS for this php session
         * @return bool
         */
        public static function IsWindows()
        {
            return (stripos(PHP_OS, 'WIN') !== false ? true : false);
        }

        /**
         * Find the Client's IP or the Proxy IP
         * @return string IP Address
         */
        public static function GetClientIP()
        {
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) )           // Check ip from share internet
            {
                return $_SERVER['HTTP_CLIENT_IP'];
            }

            if ( !empty($_SERVER['HTTP_X_FORWARDED_FOR']) )     // Check ip is pass from proxy
            {
                $for = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
                return trim($for[0]);
            }

            return filter_input(INPUT_SERVER, 'REMOTE_ADDR');
        }

        public static function IsPOST()
        {
            return ($_SERVER['REQUEST_METHOD'] == "POST");
        }

        public static function IsGET()
        {
            return ($_SERVER['REQUEST_METHOD'] == "GET");
        }

        /**
         * Awesome function to get the a values from COOKIE,GET,POST, and command line args. If
         * array is passed, it will return the value of the first matching with name in the array.
         * @param mixed $Who  String name of what your looking for or array of strings.
         * @param int $Method
         * @param int   $FilterID   PHP's filter_input Type
         * @param array $FilterOpts PHP's filter_input options
         * @return mixed
         */
        public static function GetVar($Who, $Method = self::GETVAR_ALL)
        {
            if ( !is_array($Who) )
            {
                $Who = array($Who);
            }

            foreach ( $Who as $xName )
            {
                if ( !is_string($xName) )
                {
                    continue;
                }

                if ( (empty($Method) || $Method == self::GETVAR_POST) && array_key_exists($xName, $_POST) )
                {
                    return $_POST[$xName];
                }

                if ( (empty($Method) || $Method == self::GETVAR_GET) && array_key_exists($xName, $_GET) )
                {
                    return $_GET[$xName];
                }

                if ( (empty($Method) || $Method == self::GETVAR_COOKIE) && array_key_exists($xName, $_COOKIE) )
                {
                    return $_COOKIE[$xName];
                }
                
                if ( (empty($Method) || $Method == self::GETVAR_SESSION) && isset($_SESSION) && array_key_exists($xName, $_SESSION) )
                {
                    return $_SESSION[$xName];
                }

                if ( (empty($Method) || $Method == self::GETVAR_CARG ) )
                {
                    return Console::GetCArgs($xName);
                }

                if ($Method == self::GETVAR_ENV && array_key_exists($xName, $_ENV))
                {
                    return $_ENV[$xName];
                }
            }

            return null;
        }

        /**
         * Awesome function to get the a values from COOKIE,GET,POST, and command line args. If
         * array is passed, it will return the value of the first matching with name in the array.
         * @param mixed $Who  String name of what your looking for or array of strings.
         * @param int $Method
         * @param int   $FilterID   PHP's filter_input Type
         * @param array $FilterOpts PHP's filter_input options
         * @return mixed
         */
        public static function FilterVar($Who, $Method = self::GETVAR_ALL, $FilterID = FILTER_UNSAFE_RAW, array $FilterOpts = array())
        {
            if ( !is_array($Who) )
            {
                $Who = array($Who);
            }

            foreach ( $Who as $xName )
            {
                if ( !is_string($xName) )
                {
                    continue;
                }

                if ( (empty($Method) || $Method == self::GETVAR_COOKIE) && array_key_exists($xName, $_COOKIE) )
                {
                    return filter_input(INPUT_COOKIE, $xName, $FilterID, $FilterOpts);
                }

                if ( (empty($Method) || $Method == self::GETVAR_POST) && array_key_exists($xName, $_POST) )
                {
                    return filter_input(INPUT_POST, $xName, $FilterID, $FilterOpts);
                }

                if ( (empty($Method) || $Method == self::GETVAR_GET) && array_key_exists($xName, $_GET) )
                {
                    return filter_input(INPUT_GET, $xName, $FilterID, $FilterOpts);
                }

                if ( (empty($Method) || $Method == self::GETVAR_CARG ) )
                {
                    return filter_var(Console::GetCArgs($xName), $FilterID, $FilterOpts);
                }

                if ($Method == self::GETVAR_ENV && array_key_exists($xName, $_ENV))
                {
                    return filter_input(INPUT_ENV, $xName, $FilterID, $FilterOpts);
                }
            }

            return null;
        }

        /**
         * If Is an Ajax Session
         * NOTE this only works if the header is being passed.  jQuery and most Ajax
         * libs do this, but custom calls do not. (ex: new XMLHttpRequest();
         * @return bool
         */
        public static function IsAjax()
        {
            $xreq = strtolower(filter_input(INPUT_SERVER, "HTTP_X_REQUESTED_WITH", FILTER_SANITIZE_STRING));
            return ($xreq == 'xmlhttprequest');
        }

        public static function JsonFromInput($quickVar = null)
        {
            $sentData = json_decode(file_get_contents("php://input"), true);

            if (empty($quickVar))
            {
                return $sentData;
            }

            return $sentData[$quickVar];
        }

        /**
         * Setup the path to output file or directory
         * @param type $iDirOrFile
         * @param type $iType
         * @param type $iMod
         * @return boolean
         */
        public static function SetupFilePath($iDirOrFile, $iType = self::MK_FILE, $iMod = self::DEFAULT_CHMOD, $touch = false)
        {
            $xRes = true;
            $iDirOrFile = self::GetFilePath($iDirOrFile);

            if ( !file_exists($iDirOrFile) )
            {
                if ( $iType == self::MK_FILE )
                {
                    $xDir = dirname($iDirOrFile);
                }
                else
                {
                    $xDir = $iDirOrFile;
                }

                if (!file_exists($xDir))
                {
                    $xRes = mkdir($xDir, $iMod, true);
                }

                if ($iType == self::MK_FILE && $touch)
                {
                    touch($iDirOrFile) && chmod($iDirOrFile, $iMod);
                }
                else
                {
                    $iDirOrFile = $xDir;
                }
            }

            if (file_exists($iDirOrFile) && is_writable($iDirOrFile) )
            {
                $xRes = true;
            }

            return $xRes;
        }

        /**
         * GetFilePath gets the realpath of a file/dir seaching a path
         * @param string $iFile File/Dir Name
         * @param mixed  $iPath String of Path. null will use get_include_path from php environment
         * @return mixed Full Path of file/dir or false for not found.
         */
        public static function GetFilePath($iFile, $iPath = null)
        {
            if ( !is_array($iFile) )
            {
                // Check if root or absolute path
                if (preg_match('%^([A-z]:\\\\+?|\\\\+|/+)%im', $iFile))
                {
                    return $iFile;
                }

                $iFile = array($iFile);
                $xIsArray = false;
            }
            else
            {
                $xIsArray = true;
            }

            if ( is_null($iPath) )
            {
                $iPath = get_include_path();
            }

            if ( is_string($iPath) )
            {
                $xDirList = explode(PATH_SEPARATOR, $iPath);
            }

            foreach ( $iFile as &$xFile )
            {
                $xPath = false;

                foreach ( $xDirList as $xDirPath )
                {
                    $tmpFile = sprintf("%s/%s", $xDirPath, $xFile);

                    if ( file_exists($tmpFile) )
                    {
                        $xPath = realpath($tmpFile);
                        break;
                    }
                }

                if ( empty($xPath) && file_exists($xFile) )
                {
                    $xPath = realpath($xFile);
                }

                if ( is_dir($xPath) )
                {
                    $xPath = rtrim($xPath, "\\/") . DIRECTORY_SEPARATOR;
                }

                $xFile = $xPath;
            }

            if ( $xIsArray )
            {
                return $iFile;
            }

            return $iFile[0];
        }

        /**
         * Generate and create a temp file name to write to. Don't forget to unlink or blank file will still be there
         * @param type $iPrefix  Beginning of filename
         * @param type $iSuffix  And of a filename
         * @param type $iDir     It's a Directory!
         * @return string File name & path
         */
        public static function GetTempName($iPrefix = null, $iSuffix = null, $iDir = null)
        {
            $iPrefix = trim($iPrefix);
            $iSuffix = trim($iSuffix);
            $iDir = trim($iDir);

            if ( empty($iDir) )
            {
                $iDir = trim(sys_get_temp_dir());
            }

            if ( empty($iDir) || !is_dir($iDir) )
            {
                $iDir = ".";
            }

            // valid filename characters. exclude "similar" characters 0, O, 1, l, I to enhance readability. add underscore
            $xChars = array_flip(array_diff(array_merge(range(50, 57), range(65, 90), range(97, 122), array(95, 45)), array(73, 79, 108)));

            // create random filename 20 chars long for security
            for ( $xFileName = rtrim($iDir, '/') . '/' . $iPrefix, $loop = 0, $xlp = 0; $xlp++ < 20; $xFileName .= chr(array_rand($xChars)) );

            while ( file_exists($xFileName . $iSuffix) )
            {
                $xFileName .= chr(array_rand($xChars));
                if ( $loop++ > 10 )
                {
                    throw new \Score\Exception(self::EXCEPTION_LOOPERROR);
                }

                clearstatcache();
            }

            $xFileName = $xFileName . $iSuffix;
            touch($xFileName);

            return $xFileName;
        }

        /**
         * Fix string with environment vars
         * @param string $iDirOrFile Directory or file string to convert Environment vars using %name%
         * @return string Fixed string/path
         */
        public static function FixEnvVars($iDirOrFile)
        {
            $xData = null;
            $xOffset = 0;

            while ( preg_match('/%.*?%/simx', $iDirOrFile, $xData, PREG_OFFSET_CAPTURE, $xOffset) > 0 )
            {
                // First entry contains the full match, now lets validate
                $xNew = getenv(trim($xData[0][0], '%'));
                $xPos = $xData[0][1];

                if ( !empty($xNew) )
                {
                    $iDirOrFile = substr_replace($iDirOrFile, $xNew, $xPos, strlen($xData[0][0]));
                    $xOffset = $xPos + strlen($xNew);
                    echo(var_export($iDirOrFile, true));
                }
                else
                {
                    $xOffset = $xPos + strlen($xData[0][0]);
                }
            }

            if ( Strings::Contains($iDirOrFile, "%DT%") )
            {
                $iDirOrFile = str_replace("%DT%", \Score\Time::QuickStamp(false), $iDirOrFile);
            }

            return $iDirOrFile;
        }

        /**
         * Pass a file or directory, trim up the slashes, check env vars, and more
         * @param string    $iDirOrFile     Directory or File to Fix
         * @param bool      $iAddSlash      Append DIRECTORY_SEPARATOR to $iDirOrFile
         * @param bool      $iWindowsQuote  Add quotes around if windows?
         * @param string    $iAppend        Append a file or directory which will add a slash between
         * @return string   The expanded file or directory
         */
        public static function FixDirFile($iDirOrFile, $iAddSlash = false, $iWindowsQuote = false, $iAppend = null)
        {
            $iDirOrFile = rtrim(self::FixEnvVars($iDirOrFile), DIRECTORY_SEPARATOR);

            if ( !empty($iAppend) )
            {
                $iDirOrFile .= DIRECTORY_SEPARATOR . trim(self::FixEnvVars($iAppend), DIRECTORY_SEPARATOR);
            }

            if ( $iAddSlash )
            {
                $iDirOrFile .= DIRECTORY_SEPARATOR;
            }

            if ( self::IsWindows() && $iWindowsQuote )
            {
                $iDirOrFile = Strings::Quote($iDirOrFile, Strings::QUOTE_DOUBLE, true);
            }

            return $iDirOrFile;
        }

        /**
         * DEPRECIATED: Use \Score\Process::PidRunning()
         * Is process running based on Process ID?
         * @param int $iPID Integer of process running
         * @return boolean
         */
        public static function IsProcessRunning($iPID)
        {
            return \Score\Process::PidRunning($iPID);
        }

        /**
         * Locate the PHP Binary used for running
         * @return string|boolean Location of PHP binary or false for not found
         */
        public static function LocatePhpBin($iUsePathEnv = true)
        {
            $find = (Server::IsWindows() ? "php.exe" : "php");

            if ($iUsePathEnv)
            {
                $path = getenv("PATH");
                $path = explode(PATH_SEPARATOR, $path);

                foreach ($path as $dir)
                {
                    $dir = realpath($dir);

                    if (file_exists($dir . DIRECTORY_SEPARATOR . $find))
                    {
                        return $dir . DIRECTORY_SEPARATOR . $find;
                    }
                }
            }

            $dir = realpath(ini_get("extension_dir"));

            while (dirname($dir) !== $dir)
            {
                if (file_exists($dir . DIRECTORY_SEPARATOR . $find))
                {
                    return $dir . DIRECTORY_SEPARATOR . $find;
                }

                $dir = dirname($dir);
            }

            return false;
        }

        /**
         * Increase this PHP's session memory limit to the max given.  (If allowed)
         * @param string $max
         */
        public static function IncreaseMemLimit($max)
        {
            $curr = ini_get("memory_limit");

            if ($curr === -1)
            {
                return;
            }

            if ($max === -1)
            {
                ini_set("memory_limit", -1);
                return;
            }

            $old = Strings::NumberFromBytes($curr);
            $new = Strings::NumberFromBytes($max);

            if ($new > $old)
            {
                ini_set("memory_limit", Strings::NumberInBytes($new, Strings::BYTE_SUFFIX_NOBYTES));
            }
        }
    }
}
