<?php

declare(strict_types=1);

namespace Score\Filesystem
{
    class Helpers
    {
        /**
         * Slugify a string.
         * 
         * @param string $str
         * @return string
         */
        public function Slugify(string $str): string
        {
            $search = array('Ș', 'Ț', 'ş', 'ţ', 'Ş', 'Ţ', 'ș', 'ț', 'î', 'â', 'ă', 'Î', 'Â', 'Ă', 'ë', 'Ë');
            $replace = array('s', 't', 's', 't', 's', 't', 's', 't', 'i', 'a', 'a', 'i', 'a', 'a', 'e', 'E');
            $str = str_ireplace($search, $replace, strtolower(trim($str)));
            $str = preg_replace('/[^\w\d\-\ ]/', '', $str);
            $str = str_replace(' ', '-', $str);

            return preg_replace('/-{2,}/', '-', $str);
        }

        /**
         * Return the value of bytes into actually size MB/KB/GB etc.
         *
         * @param int $bytes
         * @param boolean $withText
         * @return mixed
         */
        public function FormatSizeUnits(int $bytes, bool $withText = true)
        {
            if ($bytes >= 1073741824) {
                $bytes = number_format($bytes / 1073741824, 2);

                if ($withText) {
                    $bytes .= ' GB';
                }
            } elseif ($bytes >= 1048576) {
                $bytes = number_format($bytes / 1048576, 2);

                if ($withText) {
                    $bytes .= ' MB';
                }
            } elseif ($bytes >= 1024) {
                $bytes = number_format($bytes / 1024, 2);

                if ($withText) {
                    $bytes .= ' KB';
                }
            } elseif ($bytes > 1) {
                $bytes = $bytes;

                if ($withText) {
                    $bytes .= ' bytes';
                }
            } elseif ($bytes == 1) {
                $bytes = $bytes;

                if ($withText) {
                    $bytes .= ' byte';
                }
            } else {
                $bytes = 0;

                if ($withText) {
                    $bytes .= ' bytes';
                }
            }

            return $bytes;
        }

        /**
         * Sort Array Based of Columns in array
         *
         * @param  array | $array - ["name" => "christian", "name" => "brad"]
         * @param  array | $col - ["name" => self::SORT_ASC]
         * @return array | output - ["name" => "brad", "name" => "christian"]
         */
        public function ArrayMultiSort(array $array, array $cols): array
        {
            $colarr = [];

            foreach ($cols as $col => $order) 
            {
                $colarr[$col] = [];

                foreach ($array as $k => $row) 
                {
                    $colarr[$col]['_' . $k] = strtolower($row[$col]);
                }
            }

            $eval = 'array_multisort(';

            foreach ($cols as $col => $order) 
            {
                $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
            }

            $eval = substr($eval, 0, -1) . ');';

            eval($eval);

            $ret = [];

            foreach ($colarr as $col => $arr) 
            {
                foreach ($arr as $k => $v) 
                {
                    $k = substr($k, 1);
                    if (!isset($ret[$k])) $ret[$k] = $array[$k];
                    $ret[$k][$col] = $array[$k][$col];
                }
            }

            return $ret;
        }
    }
}