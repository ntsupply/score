<?php

declare(strict_types=1);

namespace Score\Filesystem\Interfaces
{
    interface HashInterface
    {
        /**
         * Make sure file hash doesn't exist someone where else in the media manager
         * 
         * @param string $pathOfFile - path of the 
         * @return array
         */
        function isFileHashExist(string $pathOfFile);

        /**
         * Store the md5_file to hash in database
         * 
         * @param string $pathOfFile
         * @return bool 
         */
        function addFileHash(string $pathOfFile);

        /**
         * Delete the stored md5_file hash in the database
         * 
         * @param string $pathOfFile
         * @return bool 
         */
        function deleteFileHash(string $pathOfFile);

        /**
         * findSlugByPath - find the slug from the full path
         *
         * @param string $pathOfFile
         * @return void
         */
        function findSlugByPath(string $pathOfFile);
    }
}