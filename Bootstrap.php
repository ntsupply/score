<?php

namespace Score
{
    require_once(__DIR__ . "/Loader.php");
    \Score\Loader::Register();

    use \Score\Session;
    use \Score\Container;

    /**
     * Bootstrap helper that handles Loader and things that must be done
     * @todo I have no idea what I am doing here
     * @author bsmith
     */
    class Bootstrap
    {
        public $Session;
        public $Contianer;

        public function __construct($Namespace)
        {
            $this->Session = new Session($Namespace);
            $this->Contianer = new Container();
        }

/*
        protected $_callbacks = array();
        public static function addCallback($Callback)
        {
            $this->_callbacks[] = $Callback;
            return true;
        }

        public function Init()
        {
            foreach ($this->_callbacks as $callback)
            {
                if (is_callable($callback))
                    call_user_func($callback);
            }
        }

 */
    }
}

