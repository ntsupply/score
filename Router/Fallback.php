<?php

namespace Score\Router
{
    class Fallback
    {
        /**
         * Default Fallback for a 404 page/response
         */
        public function serve404()
        {
            header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
            header("Status: 404 Not Found");
            $_SERVER['REDIRECT_STATUS'] = 404;

            if (empty($_SERVER["CONTENT_TYPE"]) || \Score\Strings::ListContains("multipart/form-data,text/html,application/x-www-form-urlencoded", $_SERVER["CONTENT_TYPE"]))
            {
                echo("<h1>404 Not Found</h1>\nThe page that you have requested could not be found.");
            }

            exit();
        }
    }
}