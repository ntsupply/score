<?php

namespace Score\Router
{
    /**
     * Object used for storing information about a Route being Resolved
     */
    final class Info
    {
        public $script;
        public $query;
        public $request;
        public $rebase;
        public $route;
        public $check_index;
        public $entry;
        public $callback;
        public $matches;
        public $method;
        public $scheme;
        public $redirect_code;
        public $redirect_location;
    }
}