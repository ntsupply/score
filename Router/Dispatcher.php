<?php

namespace Score\Router
{
    use \Score\Container\Cookie;

    class Dispatcher
    {
        /**
         * @var \Score\Router\Route
         */
        protected $_entry;
        /**
         * @var \Score\Router\Info
         */
        protected $_info;
        protected $_matches;
        protected $_route;
        protected $_router;

        /**
         * Create instance of Dispatcher from Router
         * @param \Score\Router $router
         */
        public function __construct(\Score\Router &$router)
        {
            $cookie = new Cookie($router, Cookie::NAME_GUID, Cookie::INSTANCE_MULTI);
            \Score\Container::globalSet($cookie);
            $this->_router = $cookie->name;
        }

        /**
         *
         * @return \Score\Router
         */
        protected function &Router()
        {
            return \Score\Container::globalGet($this->_router);
        }

        /**
         * Creates a list of indices it tried to find a match on
         * @param string $whatIndex What index it just tried
         */
        protected function _checkIndexLog($whatIndex)
        {
            if (empty($this->_info->check_index))
            {
                $this->_info->check_index = $whatIndex;
            }
            else
            {
                $this->_info->check_index .= ',' . $whatIndex;
            }
        }

        /**
         *
         * @param string $suffix
         * @param string $route
         * @return bool|\Score\Router\Route Route if possible or false if it doesn't find it
         */
        protected function _checkIndex($suffix, $route)
        {
            $base =& $this->Router()->Cache()->index;
            $entry = false;
            $matches = null;
            $lc_route = strtolower($route);

            // Check to see if has an absolute path match
            $this->_checkIndexLog("abs_path{$suffix}");
            if (isset($base["abs_path{$suffix}"]) && array_key_exists($route, $base["abs_path{$suffix}"]))
            {
                $entry = $base["abs_path{$suffix}"][$route];
                \Score\Logger::ByGlobal(__METHOD__, "Route match case sensitive for {$route} #{$entry}");
            }

            // Check to see if has a path that isn't case sensitive
            $this->_checkIndexLog("lc_path{$suffix}");
            if ($entry === false && isset($base["lc_path{$suffix}"]) && array_key_exists($lc_route, $base["lc_path{$suffix}"]))
            {
                $entry = $base["lc_path{$suffix}"][$lc_route];
                \Score\Logger::ByGlobal(__METHOD__, "Route match case insensitive for {$route} #{$entry}");
            }

            // Check to is if has an pattern path match
            $this->_checkIndexLog("pattern{$suffix}");

            if ($entry === false && isset($base["pattern{$suffix}"]))
            {
                foreach ($base["pattern{$suffix}"] as $pattern => $num)
                {
                    if (preg_match($pattern, $route, $matches))
                    {
                        $entry = $num;
                        \Score\Logger::ByGlobal(__METHOD__, "Route match for {$route} based on {$pattern} #{$entry}");
                        break;
                    }
                }
            }

            if ($entry !== false)
            {
                $this->_info->matches = $matches;
                $this->_entry = $this->Router()->getRoute($entry);
            }

            return $entry;
        }

        /**
         * Get info about the route
         * @return \Score\Router\Info
         */
        public function getInfo()
        {
            return $this->_info;
        }

        /**
         * Find the Route it belongs to and ready it for Dispatching
         * @param string $uri
         * @param string $method
         * @param string $schema
         */
        public function Resolve($uri = null, $method = null, $schema = null)
        {
            $this->Router()->Cache()->Load();

            // Breakdown url and get the route
            $this->_info = $this->Router()->getRouteRequestInfo($uri, $method, $schema);
            $resolve = preg_replace("%//+%", "/", $this->_info->route);

            $this->_route = ($resolve == '/' ? $resolve : rtrim($resolve, '/'));
            $method = $this->_info->method;
            $schema = $this->_info->scheme;
            $matches = null;

            /**
             * The following is a nasty bit of code.  I would prefer something more
             * dynamic, but nothing was coming to me on how to handle it. Damn alergies!
             */

            $entry = $this->_checkIndex("_{$method}_{$schema}", $this->_route, $matches);
            if ($entry === false)
            {
                $entry = $this->_checkIndex("_{$method}", $this->_route);
            }

            // Lets change method to ANY
            $method = strtolower(Route::METHOD_ANY);
            if ($entry === false)
            {
                $entry = $this->_checkIndex("_{$method}_{$schema}", $this->_route);
            }

            if ($entry === false)
            {
                $entry = $this->_checkIndex("_{$method}", $this->_route);
            }

            if ($entry === false)
            {
                $entry = $this->_checkIndex("_base", $this->_route);
            }

            $this->_entry = $this->Router()->getRoute($entry);
            $this->_info->entry = var_export($entry, true);
            $this->_info->callback = $this->_entry->controller;
            // $this->_info->matches = print_r($matches, true);

            if ($this->_entry instanceof Route)
            {
                $this->_entry->PreDispatch($this->_info);
            }
        }

        /**
         * Dispatcher doin it's job!
         * @return boolean Did it dispatch?
         */
        public function Dispatch()
        {
            if ($this->_entry instanceof \Score\Router\Route)
            {
                $this->_entry->Dispatch($this->_info);
                return true;
            }

            return false;
        }
    }
}