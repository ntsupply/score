<?php

namespace Score\Router
{
    class Route
    {
        public $path;
        public $match;
        public $controller;
        public $params = array();
        protected $_options;

        protected static  $_defaultOpt;

        const OPT_CASE_SENSITIVE = "case_sensitive";
        const OPT_PATTERN_MATCH = "pattern_match";
        const OPT_ACCEPT_SCHEMA = "accept_schema";
        const OPT_ACCEPT_METHOD = "accept_method";
        const OPT_REDIR_CODE = "redir_code";
        const OPT_TRAILING_SLASH = "trailing_slash";
        const OPT_PARAMS_STORE = "params_store";
        const OPT_OPTIONS_REQ = "options_request";

        const PARAMS_OBJECT = 1;
        const PARAMS_GET = 2;
        const PARAMS_POST = 3;

        const CASE_SENSITIVE = true;
        const CASE_NOT_SENSITIVE = false;
        const CASE_FORCE_LOWER = "LC";

        const SCHEMA_HTTP_HTTPS = "https?";
        const SCHEMA_HTTP_ONLY = "http";
        const SCHEMA_HTTPS_ONLY = "https";
        const SCHEMA_HTTP_PREFERED = "http+";
        const SCHEMA_HTTPS_PREFERED = "https+"; // Can't find a good way for PHP to detect if https is enabled, will redir to https no matter what

        const METHOD_GET = "GET";
        const METHOD_POST = "POST";
        const METHOD_DELETE = "DELETE";
        const METHOD_PUT = "PUT";
        const METHOD_ANY = "ANY";

        const TRAILING_SLASH_USE = 1;
        const TRAILING_SLASH_UNUSED = 0;

        const OPTIONS_REQ_IGNORE = 0;
        const OPTIONS_REQ_AUTO = 1;

        /**
         * Instance of Route object
         * @param type $path Path of the Route
         * @param type $controller What runs for this route when Dispatched
         * @param type $params Data to save additionally to those parsed
         * @param array $options Override Route Options
         */
        public function __construct($path, $controller = null, $params = array(), array $options = array())
        {
            $this->path = $path;
            $this->controller = $controller;
            $this->params = (!is_array($params) ? array() : $params);

            if (!is_array($options))
            {
                $options = array();
            }

            if (substr($path, -1) === '/')
            {
                $options[self::OPT_TRAILING_SLASH] = self::TRAILING_SLASH_USE;
            }

            $this->_options = self::getDefaultOptions($options);

            $this->_parsePath($this->path);
        }

        /**
         * Get option from the Route
         * @param string $option
         * @return mixed
         */
        public function getOption($option = null)
        {
            if (!empty($option))
            {
                if (isset($this->_options[$option]))
                {
                    return $this->_options[$option];
                }
                else
                {
                    return null;
                }
            }

            return $this->_options;
        }

        /**
         * Parse the route path and set Options
         * @param string $path
         */
        protected function _parsePath($path)
        {
            $offset = 0;

            if (preg_match('%(?P<case_sensitive>\#|LC)?\s?(?P<method>GET|POST|DELETE|PUT)?\s?(?P<schema>https?[+?]?)?\s?(?P<realpath>/.*)%imx', $path, $regs))
            {
                if (!empty($regs["case_sensitive"]))
                {
                    $this->_options[self::OPT_CASE_SENSITIVE] = (strtolower($regs["case_sensitive"]) == 'lc' ? self::CASE_FORCE_LOWER : ($regs["case_sensitive"] ? self::CASE_SENSITIVE : self::CASE_NOT_SENSITIVE));
                }

                if (isset($regs["method"]) && !empty($regs["method"]))
                {
                    $this->_options[self::OPT_ACCEPT_METHOD] = $regs["method"];
                }

                if (isset($regs["schema"]) && !empty($regs["schema"]))
                {
                    $this->_options[self::OPT_ACCEPT_SCHEMA] = $regs["schema"];
                }

                $realpath = $regs["realpath"];
            }
            else
            {
                $realpath = trim($path);
            }

            while (preg_match('/(?P<params>(?<!\\\\)\{.*?(?<!\\\\)\})/ix', $realpath, $regs, PREG_OFFSET_CAPTURE, $offset) > 0)
            {
                list($orig_params, $pos) = $regs["params"];

                $params = \Score\Strings::Explode(substr($orig_params, 1, -1), ";", "=");
                $params["regex"] = self::_parampattern(empty($params["p"]) ? "any" : $params["p"], (!empty($params["n"]) ? $params["n"] : null));

                if (!empty($params["n"]))
                {
                    $this->params[$params["n"]] = $params;
                }

                $orig_seg = substr($realpath, $offset, ($pos - $offset));
                $repl = preg_quote($orig_seg, '%');

                $repl .= $params["regex"];

                $realpath = substr_replace($realpath, $repl, $offset, strlen($orig_seg) + strlen($orig_params));

                $offset = $offset + strlen($repl) + 1;
            }

            if ($offset > 0)
            {
                $realpath = rtrim($realpath, '/');
                // $this->match = "%{$realpath}/?\Z%x" . ($this->getOption(self::OPT_CASE_SENSITIVE) === true ? '' : 'i');
                $this->match = "%\A{$realpath}/?\Z%x" . ($this->getOption(self::OPT_CASE_SENSITIVE) === true ? '' : 'i');
                $this->_options[self::OPT_PATTERN_MATCH] = true;
            }
            else
            {
                if ($this->getOption(self::OPT_TRAILING_SLASH) && substr($realpath, -1) == '/' && $realpath != '/')
                {
                    $realpath = rtrim($realpath, '/');
                }

                $this->match = $realpath;
            }
        }

        /**
         * So this route must be the one we want to dispatch, but we need to make sure if there are any redirects involved
         * @param \Score\Router\Info $routeInfo
         */
        public function PreDispatch(\Score\Router\Info &$routeInfo)
        {
            // Check optional options for schema.
            $https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on");
            $schema = $this->getOption(Route::OPT_ACCEPT_SCHEMA);
            $redir = $routeInfo->route;

            if (array_search(strtoupper($routeInfo->method), array(self::METHOD_POST, self::METHOD_PUT)) === false)
            {
                // Lower case preferred check
                if ($this->getOption(self::OPT_CASE_SENSITIVE) == self::CASE_FORCE_LOWER)
                {
                    $bsize = strlen($routeInfo->rebase);
                    $repl = strtolower(substr($redir, $bsize));
                    $redir = substr_replace($redir, $repl, $bsize);
                }

                // Add trailing space
                if (substr($redir, -1) !== '/' && $this->getOption(self::OPT_TRAILING_SLASH))
                {
                    $redir = $redir . '/';
                }

                // HTTP/HTTPS redirect
                if (($schema == Route::SCHEMA_HTTPS_PREFERED && !$https) || ($schema == Route::SCHEMA_HTTP_PREFERED && $https))
                {
                    $redir = \Score\Strings::Write("{0}://{1}{2}", trim($schema, '+?'), $_SERVER["HTTP_HOST"], $redir);
                }
            }

            // Lets log, set query params, and http code
            if ($redir != $routeInfo->route)
            {
                $routeInfo->redirect_code = $this->getOption(self::OPT_REDIR_CODE);
                $routeInfo->redirect_location = preg_replace("%^//+%", "/", $redir); // Remove double slashes because that would be a fqdn and not just a redirect of the same domain

                if (!empty($routeInfo->query))
                {
                    $routeInfo->redirect_location .= "?" . $routeInfo->query;
                }

                \Score\Logger::ByGlobal(__METHOD__, "Route Object Redirect " . $routeInfo->redirect_location);
            }
        }

        /**
         * Dispatch thr route to the assigned controller or redirect if necessary
         * @param \Score\Router\Info $routeInfo
         * @throws \Exception
         * @throws \Score\Exception
         */
        public function Dispatch(\Score\Router\Info &$routeInfo)
        {
            // Start Dispatch to controller
            \Score\Logger::ByGlobal(__METHOD__, "Route Object Dispatch " . $this->path);

            if ($this->getOption(self::OPT_OPTIONS_REQ) == self::OPTIONS_REQ_AUTO && $_SERVER['REQUEST_METHOD'] == "OPTIONS")
            {
                header("Content-Length: 0");
                exit();
            }

            if (!empty($routeInfo->redirect_location))
            {
                self::_redirect($routeInfo->redirect_location, $routeInfo->redirect_code);
            }


            $params = $this->params;
            $patternMatches = $routeInfo->matches;

            if ($patternMatches)
            {
                foreach ($params as $name => &$detail)
                {
                    if (is_array($detail))
                    {
                        $params[$name] = (isset($patternMatches[$name]) ? $patternMatches[$name] : null);
                    }
                    else
                    {
                        $params[$name] = $detail;
                    }

                    if (empty($name))
                    {
                        unset($params[$name]);
                    }
                }
            }

            // Load params to Params Storage area
            Params::load($params);

            // Load to GET or POST if needed
            switch ($this->getOption(self::OPT_PARAMS_STORE))
            {
                case self::PARAMS_GET:
                    foreach (Params::getAll() as $pn => $pv)
                    {
                        $_GET[$pn] = $pv;
                    }
                    break;
                case self::PARAMS_POST:
                    foreach ($params as $pn => $pv)
                    {
                        $_POST[$pn] = $pv;
                    }
                    break;
            }

            if (empty($this->controller))
            {
                throw new \Exception("Invalid Route Controller");
            }

            $ctype = gettype($this->controller);

            if ($ctype == "string" && $this->controller[0] == "@")  // Controller string begins @ means include file
            {
                $file = substr($this->controller, 1);
                \Score\Logger::ByGlobal(__METHOD__, "Route Object Include Controller " . $file);

                if (file_exists($file))
                {
                    include($file);
                }
                else
                {
                    throw new \Score\Exception();
                }
            }
            elseif ($ctype == "string" && preg_match('%\A(https?://|/{1,2})%ix', $this->controller)) // Controller string is a url, so redirect
            {
                // @todo add var replacer for things like {{query_string}} or {{schema}}
                $location = str_replace("{query_string}", $routeInfo->query, $this->controller);
                \Score\Logger::ByGlobal(__METHOD__, "Route Object Include Redirect To {$location}");

                self::_redirect($location, $this->getOption(self::OPT_REDIR_CODE));
            }
            elseif ($ctype == "string" && $this->controller[0] == '/') // Controller string is a url on current site, so redirect
            {
                $location = str_replace("{{query_string}}", $routeInfo->query, $this->controller);
                \Score\Logger::ByGlobal(__METHOD__, "Route Object Include Redirect To {$location}");

                self::_redirect($location, $this->getOption(self::OPT_REDIR_CODE));
            }
            elseif ($ctype == "string" && preg_match('/(?P<class>[a-z0-9\\\\_]+)(?P<access>(?P<type>@{1}|:{2})(?P<method>[a-z0-9_]*)(?P<params>\(.*\)*)?|\z)/ix', $this->controller, $matches))
            {
                // (?<class>[a-z0-9\\_]+)(?<access>(?<type>@{1}|:{2})(?<method>[a-z0-9_]*)(?<params>\(.*\)*)?|\z)
                \Score\Logger::ByGlobal(__METHOD__, "Route Object Call Controller " . var_export($this->controller, true));

                $class = $matches["class"];

                if (!class_exists($class))
                {
                    \Score\Logger::ByGlobal(__METHOD__, "Unable to locate class for: " . var_export($this->controller, true));
                }

                if ($matches["type"] == "::")
                {
                    $callable = array($class, $matches["method"]);
                }
                else
                {
                    $class = new $class();
                }

                if (!empty($matches["method"]))
                {
                    $callable = array($class, $matches["method"]);

                    if (!is_callable($callable))
                    {
                        \Score\Logger::ByGlobal(__METHOD__, "Controller object and/or method is not callable: " . var_export($this->controller, true));
                        return;
                    }

                    $params = array(); // Recycled var from above, but wipe it out anyways and re-fetch
                    $parameters = explode(",", trim($matches["params"], "()"));

                    foreach ($parameters as $parameter)
                    {
                        $parameter = trim($parameter);

                        if (preg_match('/\{(.*?)\}/ix', $parameter, $pmatch))
                        {
                            $parameter = Params::get($pmatch[1]);
                        }

                        $params[] = $parameter;
                    }

                    call_user_func_array($callable, $params);
                }
            }
            elseif ($ctype == "object")
            {
                if (!is_callable($this->controller, false))
                {
                    \Score\Logger::ByGlobal(__METHOD__, "Controller object and/or method is not callable: " . var_export($this->controller, true));
                    return;
                }

                call_user_func($this->controller);
            }
        }

        /**
         * Merges the base route options with any new route options
         * @param array $baseOptions
         * @param array $newOptions
         * @return boolean
         */
        protected static function _setupOptions(&$baseOptions, $newOptions)
        {
            if (empty($baseOptions) || !is_array($baseOptions))
            {
                $baseOptions = array();
            }

            foreach ($newOptions as $option => $value)
            {
                if ($option == self::OPT_PATTERN_MATCH)
                {
                    continue;
                }

                $baseOptions[$option] = $value;
            }

            return true;
        }

        /**
         * Set default options that all new instances of a route would use unless overridden
         * @param array $defaultOptions
         * @return boolean
         */
        public static function setDefaultOptions($defaultOptions = null)
        {
            if ($defaultOptions === true)
            {
                $defaultOptions = array();
                $defaultOptions[self::OPT_PARAMS_STORE] = self::PARAMS_OBJECT;
                $defaultOptions[self::OPT_CASE_SENSITIVE] = self::CASE_NOT_SENSITIVE;
                $defaultOptions[self::OPT_REDIR_CODE] = 301;
                $defaultOptions[self::OPT_ACCEPT_SCHEMA] = self::SCHEMA_HTTP_HTTPS;
                $defaultOptions[self::OPT_ACCEPT_METHOD] = self::METHOD_ANY;
                $defaultOptions[self::OPT_PATTERN_MATCH] = false;
            }

            self::_setupOptions(self::$_defaultOpt, $defaultOptions);

            return true;
        }

        /**
         * Get the default optioons and pass any override options
         * @param array $overrideOptions
         * @return array
         */
        public static function getDefaultOptions(array $overrideOptions)
        {
            $baseOptions = self::$_defaultOpt;
            self::_setupOptions($baseOptions, $overrideOptions);
            return $baseOptions;
        }

        /**
         * Output Redirect header information and exit PHP
         * @param string $location
         * @param string $code
         */
        protected static function _redirect($location, $code)
        {
            header("HTTP/1.1 $code Moved Permanently");
            header("Location: {$location}");
            exit();
        }

        /**
         * Take simple names/styles and turn them into regular expression patters
         * @param string $pattern
         * @param bool $named
         * @return string
         */
        protected static function _parampattern($pattern, $named = null)
        {
            $regex = $pattern;

            switch ($pattern)
            {
                case 'dec':
                case 'decimal':
                case 'float':
                    $regex = '\d*\.*\d*';
                    break;
                case 'num':
                case 'number':
                case 'int':
                    $regex = '\d*';
                    break;
                case 'any':
                    $regex = $pattern = '[^/]*';
                    break;
                default:
                    // Do a little cleanup from escaping the params pattern
                    $regex = \str_replace(array('\{', '\}'), array('{', '}'), $pattern);
                    break;
            }

            if ($named)
            {
                $regex = "(?P<{$named}>{$regex})";
            }

            return $regex;
        }
    }

    Route::setDefaultOptions(true);
}
