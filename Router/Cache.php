<?php

namespace Score\Router
{
    use \Score\Cache\Worker as CacheWorker;

    /**
     * Needed a common way area for indexing the routes and caching them
     */
    class Cache
    {
        public $key;
        public $newer;
        public $data;
        public $index = array(\Score\Router::_INDEX_KEYS => array());
        public $routes = array();
        public $_worker;

        /**
         * Create instance of Cacheing object
         * @param string $cacheKey Unique key to cache routes under \Score\Cache\Worker
         * @param string $cacheNewerDate Datetime of when it would have changed
         * @return boolean
         */
        public function __construct($cacheKey = null, $cacheNewerDate = null)
        {
            $this->key = $cacheKey;
            $this->newer = $cacheNewerDate;

            if (!empty($this->key))
            {
                // Start Cache Rules
                $this->data = $this->Worker()->getNewer($this->key, $this->newer);
            }

            if (!empty($this->data))
            {
                $this->routes = $this->data["routes"];
                $this->index = $this->data["index"];
                return true;
            }
        }

        /**
         *
         * @param type $index
         * @param type $key
         * @param type $value
         */
        public function addIndex($index, $key, $value)
        {
            if (!is_array($index))
            {
                $index = array($index);
            }

            foreach ($index as $idx)
            {
                if (!array_key_exists($idx, $this->index))
                {
                    $this->index[$idx] = array();
                }

                $this->index[$idx][$key] = $value;
            }
        }

        /**
         * Is it cached?
         * @return boolean
         */
        public function isCached()
        {
            if (empty($this->key) || empty($this->routes) || empty($this->data))
            {
                return false;
            }

            return true;
        }

        /**
         * Get instance of the Cache Worker
         * @return \Score\Cache\Worker
         */
        public function &Worker()
        {
            if (!$this->_worker instanceof CacheWorker)
            {
                $this->_worker = new CacheWorker(__CLASS__);
                $this->_worker->Lifetime(210);
            }

            return $this->_worker;
        }

        /**
         * Loads from the cache store the Router Cache object
         */
        public function Load()
        {
            if (empty($this->data))
            {
                $data = array();
                $data["routes"] = $this->routes;
                $data["index"] = $this->index;

                try
                {
                    $this->Worker()->set($this->key, $data);
                }
                catch (\Exception $e) { }
            }
        }
    }
}