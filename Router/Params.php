<?php

namespace Score\Router
{
    final class Params
    {
        protected static $_data = array();

        public static function get($name, $default = null)
        {
            if (!is_null(self::$_data[$name]))
            {
                return self::$_data[$name];
            }

            return $default;
        }

        public static function set($name, $value)
        {
            \Score\Helper::Sanitize($value);

            if (empty($name))
            {
                return false;
            }

            self::$_data[$name] = $value;
        }

        public static function getAll()
        {
            return self::$_data;
        }

        public static function load(array $data = array())
        {
            self::$_data = $data;
        }
    }
}

