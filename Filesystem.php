<?php 

namespace Score 
{
    use RecursiveIteratorIterator;
    use RecursiveDirectoryIterator;

    class Filesystem extends \Score\Filesystem\Helpers
    {

        const SORT_ASC = SORT_ASC;
        const SORT_DESC = SORT_DESC;

        /**
         * $_allowedTypes - default is a ["*"] which is a wildcard, accepts any file.
         *
         * @var array
         */
        protected $_allowedTypes = ["*"];
        
        /**
         * $_maxFileUploadSize - default is 25 MB
         *
         * @var integer
         */
        protected $_maxFileUploadSize = 25;

        /**
         * $_hashHelper - Interface for storing hash values
         *
         * @var \Score\Filesystem\Interfaces\HashInterface
         */
        protected $_hashHelper;

        public $root;
        public $path;
        public $fullPath;

        /**
         * Make new instance of object with path name
         * 
         * @param string $root - Full Path Ex. '/path/to/assets/media'
         * @param string $path - Relative Path of Root Ex. '/42' 
         */
        public function __construct(string $root, string $path = "/", \Score\Filesystem\Interfaces\HashInterface $hashHelper = null)
        {
            $this->root = $root;
            $this->path = $path;
            $this->fullPath = $root . $path;
            $this->_hashHelper = $hashHelper;
        }

        /**
         * This will set the array of allowed file type(s) in the instance
         * 
         * @param array $types - this is pulled directly from the file itself ['png', 'image/png']
         */
        public function SetAllowedFileTypes(array $types = ["*"]) 
        {
            $this->_allowedTypes = $types;
        }

        /**
         * This will set the max file upload size in the instance
         * 
         * @param int $maxSize - set the max file size in MB
         */
        public function SetMaxFileUploadSize(int $maxSize)
        {
            $this->_maxFileUploadSize = intval($maxSize);
        }

        /**
         * Determine if a file or directory exists.
         *
         * @param string $path
         * @return bool
         */
        public function Exists(?string $path = null): bool
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return file_exists($path);
        }

        /**
         * (ONLY SETUP FOR ONE FILE AT A TIME)
         * 
         * This is a helper to determine if allowed types match the image getting uploaded
         * if allowed types has ["*"] - wildcard, it will come back true every time.
         * 
         * @param array $fileData - this is pulled directly from the file itself ['png', 'image/png']
         * @return boolean 
         */
        protected function _IsAllowedFileType(array $fileData = []): bool
        {
            $boolean = false;

            foreach($this->_allowedTypes as $value) 
            {           
                if (in_array($value, $fileData) || $value == "*") 
                {
                    $boolean = true;
                    break;
                }
            }

            return $boolean;
        }

        /**
         * _registerFile - Finds Slug and returns it if not registers the slug.
         *
         * @param string $path
         * @return void
         */
        protected function _registerFile(string $path) 
        {
            $slug = $this->_hashHelper->findSlugByPath($path);

            if ($slug->Status === \Score\Data\Set::STAT_GOOD) 
            {
                $slug = $slug->Data[0]["slug"];
            } 
            else 
            {
                $this->_hashHelper->addFileHash($path);

                $slug = $this->_hashHelper->findSlugByPath($path);

                if ($slug->Status === \Score\Data\Set::STAT_GOOD) 
                {
                    $slug = $slug->Data[0]["slug"];
                } 
                else 
                {
                    $slug = "N/A";
                }
            }

            return $slug;
        }

        /**
         * Get the MD5 hash of the file at the given path.
         *
         * @param string | $path 
         * @return string
         */
        public function Hash(?string $path = null)
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return md5_file($path);
        }

        /**
         * Write the contents of a file.
         *
         * @param  string  $path
         * @param  string  $contents
         * @param  bool  $lock
         * @return int
         */
        public function Put(?string $path = null, $contents, bool $lock = false)
        {
            if(empty($path)) 
            {
                $path = $this->fullPath;
            }

            return file_put_contents($path, $contents, $lock ? LOCK_EX : 0);
        }

        /**
         * Write the contents of a file, replacing it atomically if it already exists.
         *
         * @param  string  $path
         * @param  string  $content
         */
        public function Replace(?string $path = null, $content)
        {
            if(empty($path)) 
            {
                $path = $this->fullPath;
            }

            // If the path already exists and is a symlink, get the real path...
            clearstatcache(true, $path);
            $path = realpath($path) ? : $path;
            $tempPath = tempnam(dirname($path), basename($path));

            // Fix permissions of tempPath because `tempnam()` creates it with permissions set to 0600...
            chmod($tempPath, 0777 - umask());
            file_put_contents($tempPath, $content);
            rename($tempPath, $path);
        }

        /**
         * Get or set UNIX mode of a file or directory.
         *
         * @param  string  $path
         * @param  int  $mode
         * @return mixed
         */
        public function Chmod(string $path, ?int $mode = null)
        {
            if ($mode) 
            {
                return chmod($path, $mode);
            }

            return substr(sprintf('%o', fileperms($path)), -4);
        }

        /**
         * Delete the file at a given path.
         *
         * @param  string  $paths
         * @param  bool $deleteStoredHash - if turned on will delete hash from database when file is deleted
         * @return bool
         */
        public function Delete(?string $paths = null): bool
        {
            if (empty($paths)) 
            {
                $paths = $this->fullPath;
            }

            $paths = is_array($paths) ? $paths : (!empty(func_get_args()) ? func_get_args() : [$paths]);

            $success = false;

            foreach ($paths as $path) 
            {
                try 
                {
                    if (@unlink($path)) 
                    {
                        $success = true;

                        if ($this->_hashHelper) 
                        {
                            $this->_hashHelper->deleteFileHash($path);
                        }
                    }
                } 
                catch (\ErrorException $e) 
                {
                    $success = false;
                }
            }

            return $success;
        }

        /**
         * Move a file to a new location.
         *
         * @param  string  $target
         * @return bool
         */
        public function Move(string $target): bool
        {
            $path = $this->fullPath;

            return rename($path, $target);
        }

        /**
         * Copy a file to a new location.
         *
         * @param  string  $path
         * @param  string  $target
         * @return bool
         */
        public function Copy(?string $path = null, string $target): bool
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return copy($path, $target);
        }

        /**
         * Extract the file name from a file path.
         *
         * @param string $path
         * @return string
         */
        public function Name(?string $path = null)
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return pathinfo($path, PATHINFO_FILENAME);
        }

        /**
         * Extract the trailing name component from a file path.
         *
         * @param string $path
         * @return mixed
         */
        public function Basename(?string $path = null)
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return pathinfo($path, PATHINFO_BASENAME);
        }

        /**
         * Extract the parent directory from a file path.
         *
         * @return mixed
         */
        public function Dirname(?string $path = null)
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return pathinfo($path, PATHINFO_DIRNAME);
        }

        /**
         * Extract the file extension from a file path.
         *
         * @return string
         */
        public function Extension(?string $path = null)
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return pathinfo($path, PATHINFO_EXTENSION);
        }

        /**
         * Get the file type of a given file.
         *
         * @param $path 
         * @return string
         */
        public function Type(?string $path = null)
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return filetype($path);
        }

        /**
         * Get the mime-type of a given file.
         *
         * @param string $path
         * @return string|false
         */
        public function MimeType(?string $path = null)
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }
            
            return finfo_file(finfo_open(FILEINFO_MIME_TYPE), $path);
        }

        /**
         * Get the file size of a given file.
         *
         * @param string $path
         * @param boolean $format
         * @param boolean $formatWithText
         * @return mixed
         */
        public function Size(?string $path = null, bool $format = false, bool $formatWithText = false)
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return !$format ? filesize($path) : $this->FormatSizeUnits(filesize($path), $formatWithText);
        }

        /**
         * Get the file's last modification time.
         *
         * @param string $path
         * @return int|false
         */
        public function LastModified(?string $path = null)
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return filemtime($path);
        }

        /**
         * Determine if the given path is a directory.
         *
         * @param string $path
         * @return bool
         */
        public function IsDirectory(?string $path = null): bool
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return is_dir($path);
        }

        /**
         * Determine if the given path is readable.
         *
         * @param string $path
         * @return bool
         */
        public function IsReadable(?string $path = null): bool
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return is_readable($path);
        }

        /**
         * Determine if the given path is writable.
         *
         * @param string $path
         * @return bool
         */
        public function IsWritable(?string $path = null): bool
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return is_writable($path);
        }

        /**
         * Determine if the given path is a file.
         *
         * @param string $path
         * @return bool
         */
        public function IsFile(?string $path = null): bool
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return is_file($path);
        }

        /**
         * Determine if the given path is a Symbolic Link.
         *
         * @param string $path
         * @return bool
         */
        public function isSymLink(?string $path = null): bool
        {
            if (empty($path)) 
            {
                $path = $this->fullPath;
            }

            return is_link($path);
        }

        /**
         * Find path names matching a given pattern.
         *
         * @param  string  $pattern
         * @param  int     $flags
         * @return array
         */
        public function glob(?string $pattern = null, int $flags = 0)
        {
            if (empty($pattern)) 
            {
                $pattern = $this->fullPath;
            }

            return glob($pattern, $flags);
        }

        /**
         * Get an array of all files and folders in a directory.
         *
         * @param  string   $col - (name|type|extType)
         * @param  const  $sort SORT_ASC | SORT_DESC
         * @return array
         */
        public function All($sort = self::SORT_ASC): array
        {
            $data = [];
            $directory = rtrim($this->fullPath, "/") . '/*';

            foreach ($this->glob($directory) as $path) 
            {
                if($this->Exists($path))
                {
                    $data[] = $this->_returnDataFrom($path);  
                }
            }

            return array_merge(
                $this->ArrayMultiSort($data, [
                    "type" => $sort,
                    "name" => $sort
                ])
            );
        }

        /**
         * Get all of the directories within a given directory.
         *
         * @param  const  $sort SORT_ASC | SORT_DESC      
         * @return array
         */
        public function Directories($sort = self::SORT_ASC): array
        {
            $data = [];
            $directory = rtrim($this->fullPath, "/") . '/*';

            foreach ($this->glob($directory) as $path) 
            {
                if ($this->Exists($path)) 
                {
                    if ($this->IsDirectory($path)) 
                    {
                        $data[] = $this->_returnDataFrom($path);  
                    }
                }
            }

            return $this->ArrayMultiSort($data, [
                "name" => $sort
            ]);
        }

        /**
         * Get all of the files within a given directory.
         *
         * @param  const    $sort SORT_ASC | SORT_DESC 
         * @return array
         */
        public function Files($sort = self::SORT_ASC): array
        {        
            $data = [];
            $directory = rtrim($this->fullPath, "/") . '/*';

            foreach ($this->glob($directory) as $path) 
            {
                if ($this->Exists($path)) 
                {
                    if ($this->IsFile($path)) 
                    {
                        $data[] = $this->_returnDataFrom($path);                        
                    }
                }
            }

            return $this->ArrayMultiSort($data, [
                "name" => $sort
            ]);
        }

        /**
         * Upload Files to a given directory
         *
         * @param  $_FILES  $files
         * @param  boolean  $overwrite - overwrite the file if exist       
         * @param  array    $options - array of options      
         * @return string \Score\Data\Set
         */
        public function UploadFiles(?array $files, bool $overwrite = false, ...$options)
        {
            $directory = rtrim($this->fullPath, "/") . '/';
            $result = \Score\Data\Set::MakeNoData();

            foreach ($files as $file) 
            {
                if (($opt = \Score\Helper\CallableFunction::getCallback("beforeUpload", $options)) !== false)
                {
                    // $ogFile = $directory . str_replace(' ', '', $file["name"]);
                    // if ($this->Exists($ogFile)) 
                    // {
                    //     $this->Delete($ogFile);
                    // }

                    $file = $opt->call($file);
                }

                /** Make sure their is no spaces in the name  */
                $file["name"] = str_replace(' ', '', $file["name"]);

                $filePath = $directory . $file['name'];
                
                $fileExt = $this->Extension($filePath);
                $fileMimeType = $this->MimeType($file['tmp_name']);
                $allowedFileType = $this->_IsAllowedFileType([
                    $fileExt, 
                    $fileMimeType
                ]);

                if (!$this->Exists($filePath)) 
                {
                    /** If everything went well. It will put file inside folder :) */
                    if ($allowedFileType) 
                    {
                        if (move_uploaded_file($file['tmp_name'], $filePath)) 
                        {
                            $storeHashMessage = "";
                            if($this->_hashHelper) 
                            {
                                $storeHashMessage = " " . $this->_checkForMultiplePaths($filePath);
                            }

                            $result->SetGood(true, "File(s) uploaded successfully!" . $storeHashMessage);
                        } 
                        else 
                        {
                            $result->SetError("File(s) didn't upload successfully!");
                        }
                    }
                    else 
                    {
                        $result->SetError("Media Manager doesn't support the file ext or mime type!");
                    }
                }
                else
                {
                    if ($overwrite) 
                    {
                        $this->Replace($filePath, file_get_contents($file['tmp_name']));

                        $result->SetGood(true, "File(s) successfully replaced!");
                    }
                    else 
                    {
                        $result->SetError("File(s) already exist.");
                    }
                }
            }
            
            return $result->toJSON();
        }

        /**
         * Create a directory.
         *
         * @param  int     $mode
         * @param  bool    $recursive
         * @param  bool    $force
         * @return bool
         */
        public function MakeDirectory(int $mode = 0755, bool $recursive = false, bool $force = false): bool
        {
            $directory = $this->fullPath;

            if ($force) 
            {
                return @mkdir($directory, $mode, $recursive);
            }

            return mkdir($directory, $mode, $recursive);
        }

        /**
         * Create a SymLink.
         *
         * @param  string  $path - path with new folder or file name 
         * @param  int     $target
         * @param  bool    $hardLink -  makes it a hard link.
         * @return bool
         */
        public function MakeSymLink(?string $path = null, ?string $target = null, bool $hardLink = false): bool
        {
            if (!empty($path)) 
            {
                if (empty($target)) 
                {
                    $target = $this->fullPath;
                }
    
                if ($hardLink) 
                {
                    return link($target, $path);
                }
    
                return symlink($target, $path);
            }

            return false;
        }

        /**
         * Move a directory.
         *
         * @param  string  $to
         * @param  bool  $overwrite
         * @return bool
         */
        public function MoveDirectory(string $to, bool $overwrite = false): bool
        {
            $directory = $this->fullPath;

            if ($overwrite && $this->IsDirectory($to) && !$this->DeleteDirectory($to)) 
            {
                return false;
            }

            return @rename($directory, $to) === true;
        }

        /**
         * Rename a directory.
         *
         * @param  string  $to
         * @return bool
         */
        public function RenameDirectory(string $to): bool
        {
            $directory = $this->fullPath;

            return @rename($directory, $to) === true;
        }

        /**
         * Recursively delete a directory.
         *
         * The directory itself may be optionally preserved.
         *
         * @param  string  $directory
         * @param  bool    $preserve
         * @return bool
         */
        public function DeleteDirectory(?string $directory = null, bool $preserve = false): bool
        {
            if (empty($directory)) 
            {
                $directory = $this->fullPath;
            }

            if (!$this->isDirectory($directory)) 
            {
                return false;
            }

            if ($this->isSymLink($directory)) 
            {
                unlink($directory);
                return true;
            }

            $currentDir = new RecursiveDirectoryIterator($directory, RecursiveDirectoryIterator::SKIP_DOTS);
            $items = new RecursiveIteratorIterator($currentDir, RecursiveIteratorIterator::CHILD_FIRST);

            foreach ($items as $item) 
            {
                // If the item is a directory, we can just recurs into the function and
                // delete that sub-directory otherwise we'll just delete the file and
                // keep iterating through each file until the directory is cleaned.
                if ($item->isDir() && !$item->isLink()) 
                {
                    $this->DeleteDirectory($item->getRealPath());
                }
                else 
                {
                    $this->Delete($item->getRealPath());
                }
            }

            if (!$preserve) 
            {
                rmdir($directory);
            }

            return true;
        }

        /**
         * Empty the specified directory of all files and folders.
         *
         * @param  string  $directory
         * @return bool
         */
        public function CleanDirectory(?string $directory = null): bool
        {
            if (empty($directory)) 
            {
                $directory = $this->fullPath;
            }

            return $this->DeleteDirectory($directory, true);
        }

        /** 
         * Returns a string of all the same files in media manager but in different paths 
         * 
         * @param string $filePath
         * @return string
         */
        protected function _checkForMultiplePaths(string $filePath): string
        {
            $paths = "";
            $storeHashMessage = "";

            if ($this->_hashHelper)
            {
                if( $this->_hashHelper->addFileHash($filePath) ) 
                {
                    $res = $this->_hashHelper->isFileHashExist($filePath);

                    if ($res->Status == \Score\DB\ResultSet::STAT_GOOD) 
                    {
                        foreach ($res->Data as $key => $value) 
                        {
                            $relPath = \str_replace($this->root, "", $value["path"]);
                            
                            $paths .= ($key == 0 ? "" : ", ") . "<strong>{$relPath}</strong>";
                        }

                        $storeHashMessage = "But the file already exist in {$paths} already.";
                    }
                }
            }

            return $storeHashMessage;
        }

        /** 
         * This searches for files and folders in the directory of the instance. 
         * 
         * @param string $query - string to search for in the directory
         * @param boolean $caseSensitive - when searching for string has to match exactly how you type it
         * @return array - [0 => "files", 1 => "folders"]
         */
        public function Search(?string $query, bool $caseSensitive = false): array
        {
            $caseCMD = ($caseSensitive ? "-name" : "-iname");

            $out["folder"] = explode("\n", str_replace($this->root, "", trim(shell_exec("find {$this->root} -type d {$caseCMD} '*{$query}*'"), "\n")));
            $out["files"] = explode("\n", str_replace($this->root, "", trim(shell_exec("find {$this->root} -type f {$caseCMD} '*{$query}*'"), "\n")));

            return [
                $out
            ];
        }

        /**
         * Return Basic Data about the File, Image or Directory
         *
         * @param string $path
         * @return array
         */
        protected function _returnDataFrom(string $path): array
        {
            $data = [];

            // 1. Check to see if it's a Directory / Image / File
            if ($this->IsDirectory($path))
            {
                $data[] = [
                    "type" => "folder"
                ];
            }
            else if (is_array(getimagesize($path)))  // Only if image
            {
                list($width, $height) = getimagesize($path);

                $data[] = [
                    "type" => "image",
                    "imageName" => $this->Basename($path),
                    "width" => (!empty($width) ? $width : "N/A"),
                    "height" => (!empty($height) ? $height : "N/A"),
                ];
                
            } 
            else // Default to file if doesn't check out.
            {
                $data[] = [
                    "type" => "file",
                    "extType" => $this->Extension($path),
                    "mimeType" => $this->MimeType($path),
                    "last_modified" => \Score\Time::createFromFormat(\Score\Time::FORMAT_UNIX, $this->LastModified($path))->Display(\Score\Time::FORMAT_DT),
                ];
            }

            // 2. Get Main Data about image and file types
            if (is_array(getimagesize($path)) || $this->IsFile($path)) 
            {
                $data[] = [
                    "size" => $this->Size($path, true, true),
                    "extType" => $this->Extension($path),
                    "mimeType" => $this->MimeType($path),
                    "last_modified" => \Score\Time::createFromFormat(\Score\Time::FORMAT_UNIX, $this->LastModified($path))->Display(\Score\Time::FORMAT_DT),
                ];
            }

            // 3. Register File in the DB if needed.
            if ($this->_hashHelper && $this->IsFile($path)) 
            {
                $slug = $this->_registerFile($path);

                $data[] = [
                    "slug" => $slug,
                ];
            }
            
            // 4. Main Data about the DIR / FILE / IMAGE.
            $data[] = [
                "path" => str_replace($this->root, "", $path),
                "name" => $this->Name($path),
                "isSymbolicLink" => is_link($path),
                "symbolicPath" => (is_link($path) ? str_replace($this->root, "", realpath($path)): null),
            ];

            return array_merge(...$data);
        }
    }
}