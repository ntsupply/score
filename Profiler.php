<?php

namespace Score
{
    use \Score\Profiler\Summary;
    use \Score\Profiler\Snap;
    use \Score\Server;

    class Profiler 
    {
        public $active = false;
        public $name = "default";
        public $key;
        public $signature;
        protected $_profiled;

        /**
         * @var \Score\Profiler\RecorderAdapter[]
         */
        protected $_recorders = [];

        /**
         * @var \Score\Profiler[]
         */
        protected static $_profilers = [];

        public function __construct() 
        {
            $this->signature = \Score\Encryption::CreateGUID("profiler", false);
            $this->_profiled = new Summary("profiled", null, Snap::TYPE_SUMMARY, (Server::IsGET() ? $_GET : $_POST));

            if (defined("PROFILER_START_TIME"))
            {
                $this->_profiled->start = PROFILER_START_TIME;
            }

            if (!\Score\Console::IsConsole())
            {
                if (isset($_SERVER["REQUEST_TIME_FLOAT"]))
                {
                    $this->_profiled->start = $_SERVER["REQUEST_TIME_FLOAT"];
                }
    
                $this->_profiled->url = $_SERVER['REQUEST_URI'];
                $this->_profiled->method = $_SERVER['REQUEST_METHOD'];
                $this->_profiled->agent = $_SERVER['HTTP_USER_AGENT'];
            }
            else
            {
                $this->_profiled->vars = \Score\Console::GetCArgs();
            }
        }

        public function __destruct()
        {
            $this->_profiled->mem_limit = Strings::NumberFromBytes(ini_get("memory_limit"));

            unset($this->_profiled);

            foreach ($this->_recorders as &$recorder)
            {
                $recorder->shutdown($this);
            }
        }

        public function setKey($key)
        {
            $this->key = $key;
        }

        public function addRecorder(\Score\Profiler\RecorderAdapter $recorder)
        {
            $this->_recorders[] = $recorder;
        }

        public function record(Snap $snap)
        {
            foreach ($this->_recorders as &$r)
            {
                $r->record($this, $snap);
            }
        }

        public function registerOnCookie($cookieName = "score_profiler", $methods = ['get', 'post', 'put', 'delete'])
        {
            if (isset($_SERVER['REQUEST_METHOD']))
            {
                $cookieVal = @json_decode(Server::GetVar($cookieName, Server::GETVAR_COOKIE));

                if (!empty($cookieVal) && array_search(strtolower($_SERVER['REQUEST_METHOD']), array_map('strtolower', $methods)) !== false)
                {
                    $this->name = preg_replace("/[^A-Za-z0-9 ]/", '_', $cookieVal);
                    $this->active = true;
                    $this->register($this);
                }
            }
        }

        public function register()
        {
            \Score\Profiler::registerProfiler($this);
        }

        public static function registerProfiler(\Score\Profiler $profiler)
        {
            self::$_profilers[$profiler->signature] = $profiler;
            header("X-Score-Profiler: " . implode(",", array_keys(self::$_profilers)));
        }

        public static function track(Snap $snap)
        {
            foreach (self::$_profilers as &$profiler)
            {
                if ($profiler->active)
                {
                    $profiler->record($snap);
                }
            }
        }
    }
}