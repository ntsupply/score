<?php

namespace Score\Session
{
    class Cookie implements \Iterator
    {
        protected $_name;
        protected $_data = array();
        protected $_encKey;

        protected static $cookies;

        /**
         * 
         * @param type $name
         * @param type $encryptKey
         * @param type $encryptVal
         */
        public function __construct($name, $encryptKey = null)
        {
            $this->_name = $name;
            $this->_encKey = $encryptKey;

            if (empty($_COOKIE[$name]))
            {
                return;
            }

            $data = \json_decode($_COOKIE[$name], true);

            if (json_last_error() == \JSON_ERROR_NONE)
            {
                $this->_data = $data;
            }
        }

        /**
         * Sets a session variable
         * @param string name of variable
         * @param mixed value of variable
         * @return void
         */
        public function __set($name, $value)
        {
            $this->_data[$name] = $value;
        }

        /**
         * Sets a session variable
         * @param string name of variable
         * @param mixed value of variable
         * @return void
         */
        public function set($name, $value)
        {
            $this->__set($name, $value);
        }

        /**
         * Fetches a session variable
         * @param string name of variable
         * @return mixed value of session varaible
         */
        public function __get($name)
        {
            return $this->_data[$name];
        }

        /**
         * Fetches a session variable
         * @param string name of variable
         * @return mixed value of session varaible
         */
        public function get($name)
        {
            return $this->__get($name);
        }

        /**
         * Deletes a session variable
         * @param string name of variable
         * @return boolean
         */
        public function __unset($name)
        {
            if (isset($this->_data[$name]))
            {
                unset($this->_data[$name]);
                return true;
            }

            return false;
        }

        /**
         * Deletes a session variable
         * @param string name of variable
         * @return boolean
         */
        public function del($name)
        {
            return $this->__unset($name);
        }

        /**
         * 
         * @param string $path
         * @param string $domain
         * @param bool $secure
         * @param bool $httponly
         * @return bool
         */
        public function saveOnShutdown($expires = null, $path = "/", $domain = null, $secure = false, $httponly = false)
        {
            if (empty(self::$cookies))
            {
                self::$cookies = array();
                \register_shutdown_function(__CLASS__ . "::Shutdown");
            }

            self::$cookies[$this->_name] = array("c" => $this, "a" => \func_get_args());
        }

        /**
         * 
         * @param string $path
         * @param string $domain
         * @param bool $secure
         * @param bool $httponly
         * @return bool
         */
        public function save($expires = null, $path = "/", $domain = null, $secure = false, $httponly = false)
        {
            if (empty($expires))
            {
                $expires = new \Score\Time("30 day");
            }

            if (!\is_numeric($expires))
            {
                if (\is_string($expires))
                {
                    $expires = new \Score\Time($expires);
                }
                else
                {
                    $expires = 0;
                }
            }
            
            if ($expires instanceof \DateTime)
            {
                $expires = $expires->format('U');;
            }

            if (!\is_numeric($expires))
            {
                $expires = 0;
            }

            $data = json_encode($this->_data);

            if (\setcookie($this->_name, $data, $expires, $path, $domain, $secure, $httponly))
            {
                $_COOKIE[$this->_name] = $data;
                return true;
            }

            return false;
        }

        /**
         * 
         * @return bool
         */
        public function destroy()
        {
            $this->_data = array();
            return \setcookie($this->_name, "", time() - 3600);
        }

        public function current()
        {
            return current($this->_data);
        }

        public function key()
        {
            return key($this->_data);
        }

        public function next()
        {
            return next($this->_data);
        }

        public function rewind()
        {
            reset($this->_data);
        }

        public function valid()
        {
            $key = key($this->_data);
            return ($key !== NULL && $key !== FALSE);
        }

        public static function Shutdown()
        {
            foreach (self::$cookies as &$detail)
            {
                \call_user_func_array(array($detail["c"], "save"), $detail["a"]);
            }
        }
    }
}