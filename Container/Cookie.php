<?php

namespace Score\Container
{
    /**
     * Cookie encapsulating object stored by reference with constraint information
     * @author Bradley Worrell-Smith
     */
    class Cookie
    {
        const NAME_GUID = -1;
        const INSTANCE_SINGLETON = 1;
        const INSTANCE_MULTI = 2;
        const INSTANCE_GLOBAL_SINGLETON = 3;

        /**
         * Exceptions use same number schema as Score\Container
         */
        const EXCEPTION_BADOBJ = "(100505) Invalid Object passed to Container";
        const EXCEPTION_BADNAME = "(100506) Invalid Object Name";

        /**
         * Name of object
         * @var string
         */
        public $name;
        /**
         * Object stored
         * @var object
         */
        public $object;
        /**
         * Instance constraint
         * @var int
         */
        public $typeInstance;
        /**
         * Object type detected by get_class()
         * @var string
         */
        public $typeObject;

        /**
         * Create the Cookie with object information
         * @param type $iObject Instance of Object you want stored
         * @param type $iName Unique name of Object in Container
         * @param type $iInstType Type of Instance to validate
         * @throws \Score\Exception
         */
        public function __construct(&$iObject, $iName = null, $iInstType = self::INSTANCE_MULTI)
        {
            if (!is_object($iObject))
            {
                throw new \Score\Exception(self::EXCEPTION_BADOBJ);
            }

            if ($iName === -1)
            {
                $iName = \Score\Encryption::CreateGUID(get_class($iObject));
            }
            elseif (empty($iName))
            {
                $iName = get_class($iObject);
            }

            if (!is_string($iName))
            {
                throw new \Exception(self::EXCEPTION_BADNAME);
            }

            $this->name = $iName;
            $this->object = &$iObject;
            $this->typeInstance = $iInstType;
            $this->typeObject = get_class($iObject);
        }

        /**
         * Destruct to make sure the object is being removed correctly
         */
        public function __destruct()
        {
            unset($this->object);
        }
    }
}
