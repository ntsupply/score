Score Framework
=========================
**Systematic Code Optimizing Resources Efficiently**

*Everybody likes to Score!*


Description
-------------------------
This framework is designed to make thing easier.  Score
is designed for simplicity and speed.  Primarily designed
with common naming and coding conventions in mind for the
original authors.


Developed By
-------------------------
* Bradley Worrell-Smith
* Ron Rebennack


Requirements
-------------------------
PHP 5.4,5.5,5.6,7.0,7.1


Version Info
-------------------------

1.0.0 Release (2016-10-07)

 * Finalized Score Router
 * Added fixes for php7 compatibility

0.3.4 Beta Stage (2013-10-22)

 * Added Stopwatch
 * Added Session
 * Added Bootstrap for testing
 * Several tweaks and fixes
 * Move more documentation to wiki

0.3.2 Beta Stage (2013-09-25)

 * Added DB DataTable
 * DB Connection Enhancements & Restrictions
 * Misc Fixes

0.3.0 Beta Stage (2013-09-04)

 * DB Connection Enhancements
 * Better logging support
 * Fixed many issues with Loader, Logger, Strings, etc
 * Added Encryption

0.3.0 Beta Stage (2013-09-04)

 * DB Connection Enhancements
 * Better logging support
 * Fixed many issues with Loader, Logger, Strings, etc
 * Added Encryption

0.2.0 Alpha Stage

 * More slapping code in here
 * Opened to Public on Repo


License
-------------------------

**MIT LICENSE**

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


Basic Usage
-------------------------
Add Score to your include path.  Score utilizes PHP 5.3
namespacing.  You can use an alternative apl_autoloader
or the one written in Score.

*Adding Loader*

    :::php
    <?php
    require("Score/Loader.php");

    \Score\Loader::register();

*Using Components like Score\Strings*

    :::php
    use \Score\Strings;
    $assocArray = Strings::Explode("FirstName=John,LastName=Smith", ",", "=");


Documentation
-------------------------
Any written documentation available is posted on the project's wiki page
at https://bitbucket.org/scoreframework/score/wiki/Home.  The code itself
has been commented well for IDE's like Netbeans.  Future documentation is
in the works that will combine the wiki with aditional searching
capabilities.  Until then, best wishes.  It is beta after all.