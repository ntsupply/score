<?php

namespace Score
{
    use Score\Container\Cookie;

    /**
     * Container Class for Instance tracking
     * @author Bradley Worrell-Smith
     */
    class Container
    {
        const EXCEPTION_INSINGLE = "(100501) Singleton Object already in Container";
        const EXCEPTION_INMIXED = "(100502) Illegal use of mixing types of Instances in Container";
        const EXCEPTION_INNAME = "(100503) Object (by name) exists in container.  Please specify a unique name/identifier"; // removed 0.1.1
        const EXCEPTION_NONAME = "(100504) Invalid Name in Container";

        /**
         * Global Instance of Container
         * @var Container
         */
        protected static $_global = null;

        /**
         * Stack of Instances of \Score\Container\Cookie
         * @var array
         */
        protected $_stack = array();

        /**
         * Check Global instance of Container
         * @return void
         */
        public static function _checkGlobals()
        {
            if (!is_null(self::$_global))
            {
                return;
            }

            // set instance to 1 so it doesn't get into infinate loop
            self::$_global = __CLASS__;
            self::$_global = new self::$_global();
        }

        /**
         * Create instance of container and register it with the global contianer
         */
        public function __construct()
        {
            self::_checkGlobals();

            if (is_object(self::$_global))
            {
                self::globalSet(new Cookie($this, __CLASS__, Cookie::INSTANCE_MULTI));
            }
        }

        /**
         * Add Object from Cookie to global container
         * @param \Score\Container\Cookie $Cookie
         * @return object Object from Cookie passed by reference
         * <code>
         *   Container::globalSet(new Container\Cookie($xObject, "My Object"));
         * </code>
         */
        public static function &globalSet(Cookie &$Cookie)
        {
            self::_checkGlobals();
            return self::$_global->set($Cookie);
        }

        /**
         * Get the object from Container.  If not found, it will throw Exception
         * @param string $Name
         * @return object
         */
        public static function &globalGet($Name)
        {
            self::_checkGlobals();
            return self::$_global->get($Name);
        }

        /**
         * Delete an object from the global container
         * @param string $Name
         * @return void
         */
        public static function globalDelete($Name)
        {
            self::_checkGlobals();
            self::$_global->delete($Name);
        }

        /**
         * Check for object in all containers.  Return errors if object
         * constraints are exceeded.
         * @param type $SearchContainer
         * @param type $NewObject
         * @param type $DefaultContainer
         * @return string|boolean Error if found and false no errors found
         */
        protected static function _checkForObject(&$SearchContainer, &$NewObject, &$DefaultContainer)
        {
            try
            {
                $objStack = $SearchContainer->getStack();

                foreach ($objStack as &$objInfo)
                {
                    if ($objInfo->object instanceof self)
                    {
                        $res = self::_checkForObject($objInfo->object, $NewObject, $DefaultContainer);
                        if (!empty($res))
                        {
                            return $res;
                        }

                        continue;
                    }

                    if ($objInfo->typeObject != $NewObject->typeObject)
                    {
                        continue;
                    }

                    if ($objInfo->typeInstance == Cookie::INSTANCE_GLOBAL_SINGLETON)
                    {
                        return self::EXCEPTION_INSINGLE;
                    }

                    if ($SearchContainer == $DefaultContainer)
                    {
                        if ($objInfo->typeInstance == Cookie::INSTANCE_SINGLETON)
                        {
                            return self::EXCEPTION_INSINGLE;
                        }

                        if ($objInfo->typeInstance != $NewObject->typeInstance)
                        {
                            return self::EXCEPTION_INMIXED;
                        }
                    }
                }
            }
            catch (\Exception $err)
            {
                // Return message and let parent pass back to throw \Score\Exception
                return $err->getMessage();
            }

            return false;
        }

        /**
         * Add Object from Cookie to instance of this container
         * @param \Score\Container\Cookie $iCookie Instance of Object encapsluated by Cookie
         * @return object Object from Cookie passed by reference
         * @throws \Score\Exception If object name already exists or doesn't meet constraints
         * <code>
         *   $container->Set(new Container\Cookie($xObject, "My Object"));
         * </code>
         */
        public function &set(Cookie &$Cookie)
        {
            $res = self::_checkForObject(self::$_global, $Cookie, $this);

            if ($res !== false)
            {
                throw new \Score\Exception($res);
            }

            $this->_stack[] = &$Cookie;
            return $Cookie->object;
        }

        /**
         * Quickly set instance by automatically creating Cookie encapsulation
         * @param object $Object Instance of Object you want stored
         * @param string $Name name of Object in Container
         * @param int $iInstType Type of Instance to validate
         * @return object
         */
        public function &QSet(&$Object, $Name = null, $iInstType = Cookie::INSTANCE_MULTI)
        {
            return $this->set(new Cookie($Object, $Name, $iInstType));
        }

        /**
         * Get Object from Container
         * @param type $Name name in container
         * @return object Object passed by reference
         * @throws \Score\Exception
         */
        public function &get($Name)
        {
            $xRes = array();

            foreach ($this->_stack as &$xObject)
            {
                if ($xObject->name == $Name)
                {
                    $xRes[] = &$xObject->object;
                }
            }

            if (sizeof($xRes) == 0)
            {
                throw new \Score\Exception(self::EXCEPTION_NONAME);
            }

            if (sizeof($xRes) > 1)
            {
                return $xRes;
            }

            return $xRes[0];
        }

        /**
         * Get the stack of objects in this Container
         * @return array List of Object in Container
         */
        public function &getStack()
        {
            return $this->_stack;
        }

        /**
         * Get a list of unqiue names in this stack
         * @return array names
         */
        public function listStack()
        {
            $xRes = array();

            foreach ($this->_stack as $xObject)
            {
                $xRes[] = $xObject->name;
            }

            return $xRes;
        }

        /**
         * Delete instance of object and remove from container
         * @param string $Name
         * @return void
         */
        public function delete($Name)
        {
            foreach ($this->_stack as $xKey => &$xObject)
            {
                if ($xObject->name == $Name)
                {
                    unset($this->_stack[$xKey]);
                }
            }
        }

        /**
         * Destruct trap to make sure the objects are removed from stack
         */
        public function __destruct()
        {
            foreach ($this->_stack as $xKey => $xObj)
            {
                unset($this->_stack[$xKey]);
            }
        }
    }

}