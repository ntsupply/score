<?php

namespace Score\Cache
{
    final class Manager
    {
        const LIFETIME_NEVEREXPIRES = -1;

        public static $defaultLifetime = 5;

        protected static $_adapters = array();

        public static function Register(\Score\Cache\Adapter\Base $cacheAdapter)
        {
            $pos = count(self::$_adapters);

            if (!is_null($cacheAdapter->Namespaces))
            {
                $cnt = count($cacheAdapter->Namespaces);

                foreach (self::$_adapters as $num => &$adapter)
                {
                    if (is_null($adapter->Namespaces) || count($adapter->Namespaces) <= $cnt)
                    {
                        $pos = $num;
                    }
                }
            }

            array_splice(self::$_adapters, $pos, 0, array($cacheAdapter));
        }

        public static function get($namespace, $key)
        {
            foreach (self::$_adapters as &$adapter)
            {
                if (empty($adapter->Namespaces) || (!empty($adapter->Namespaces) && array_search($namespace, $adapter->Namespaces) !== false))
                {
                    return $adapter->get($key);
                }
            }

            return false;
        }

        public static function getNewer($namespace, $key, $date)
        {
            foreach (self::$_adapters as &$adapter)
            {
                if (empty($adapter->Namespaces) || (!empty($adapter->Namespaces) && array_search($namespace, $adapter->Namespaces) !== false))
                {
                    return $adapter->getNewer($key, $date);
                }
            }

            return false;
        }

        public static function set($namespace, $key, $data = null, $lifetime = null)
        {
            if (is_null($lifetime))
            {
                $lifetime = self::$defaultLifetime;
            }

            foreach (self::$_adapters as &$adapter)
            {
                if (empty($adapter->Namespaces) || (!empty($adapter->Namespaces) && array_search($namespace, $adapter->Namespaces) !== false))
                {
                    $adapter->set($key, $data, $lifetime);
                    break;
                }
            }
        }

        public static function del($namespace, $key)
        {
            foreach (self::$_adapters as $num => &$adapter)
            {
                if (empty($adapter->Namespaces) || (!empty($adapter->Namespaces) && array_search($namespace, $adapter->Namespaces) !== false))
                {
                    $adapter->del($key);
                }
            }
        }

        public static function flush($namespace, $onlyExpired = false)
        {
            foreach (self::$_adapters as $num => &$adapter)
            {
                if (empty($adapter->Namespaces) || (!empty($adapter->Namespaces) && array_search($namespace, $adapter->Namespaces) !== false))
                {
                    $adapter->flush($onlyExpired);
                }
            }
        }
    }
}