<?php

namespace Score\Cache\Adapter
{
    use \Score\Server;
    use \Score\Time;

    class Local extends \Score\Cache\Adapter\Base
    {
        protected $_dirPath = false;

        public function setCacheDir($dirPath)
        {
            if (Server::SetupFilePath($dirPath, Server::MK_DIR))
            {
                $this->_dirPath = $dirPath;
            }
            else
            {
                \Score\Logger::ByGlobal(__METHOD__, "Unable to create cache file: {$filename}");
            }
        }

        public function deserializeFile($file)
        {
            if (!file_exists($file))
            {
                \Score\Logger::ByGlobal(__METHOD__, "Cache file does not exist: {$filename}");
            }

            try
            {
                $item = unserialize(file_get_contents($file));

                if ($item instanceof \Score\Cache\Item)
                {
                    return $item;
                }
            }
            catch (\Exception $err)
            {
                \Score\Logger::ByGlobal(__METHOD__, "Unable to de-serialize cache file: {$filename}\n" . $err->getMessage());
            }

            return false;
        }

        public function filehash($key)
        {
            return $this->_dirPath . DIRECTORY_SEPARATOR . $this->hash($key) . ".data";
        }

        public function hash($key)
        {
            return md5($key) . "." . abs(crc32($key));
        }

        public function getNewer($key, $date)
        {
            if ($this->_dirPath === false)
            {
                return false;
            }

            $file = $this->filehash($key);

            if (file_exists($file))
            {
                $item = $this->deserializeFile($file);

                if ($item === false)
                {
                    return false;
                }

                if (Time::Compare($item->Created, $date) !== Time::COMPARE_LT)
                {
                    return $this->GetData($item);
                }
            }

            return false;
        }

        public function get($key)
        {
            if ($this->_dirPath === false)
            {
                return false;
            }

            $file = $this->filehash($key);

            if (file_exists($file))
            {
                if (($item = $this->deserializeFile($file)) !== false)
                {
                    return $this->GetData($item);
                }
            }

            return false;
        }

        public function flush($onlyExpired)
        {
            if ($this->_dirPath === false)
            {
                return false;
            }

            foreach (glob($this->_dirPath . DIRECTORY_SEPARATOR . "*.data") as $filename)
            {
                if (!is_writeable($filename))
                {
                    continue;
                }

                $unlink = true;

                if ($onlyExpired)
                {
                    $item = $this->deserializeFile($filename);
                    if ($item && !$item->IsExpired())
                    {
                        $unlink = false;
                    }
                }

                if ($unlink)
                {
                    unlink($filename);
                }
            }
        }

        public function set($key, $data, $lifetime)
        {
            if ($this->_dirPath === false)
            {
                return;
            }

            $file = $this->filehash($key);

            if (!Server::SetupFilePath($file))
            {
                return;
            }

            if (is_null($data))
            {
                unlink($file);
            }
            else
            {
                $item = new \Score\Cache\Item;
                $item->Init($key, $data, $lifetime);

                if (is_writeable($file))
                {
                    file_put_contents($file, serialize($item));
                }
            }
        }

        public function del($key)
        {
            if ($this->_dirPath === false)
            {
                return;
            }

            $file = $this->filehash($key);

            if (file_exists($file) && is_writeable($file))
            {
                unlink($file);
            }
        }
    }
}