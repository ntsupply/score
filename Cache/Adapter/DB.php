<?php

namespace Score\Cache\Adapter
{
    use \Score\Time;

    class DB extends \Score\Cache\Adapter\Base
    {
        protected $_connection = false;
        protected $_table;
        protected $_map = array();
        protected $_dataset = null;

        const MAP_KEY = ":key";
        const MAP_DATA = ":data";
        const MAP_EXPIRES = ":expires";
        const MAP_CREATED = ":created";
        const MAP_DATASET = ":dataset";
        const MAP_PK = null;

        public function &setConnection(\Score\DB\Connection $connection, $tableName)
        {
            $this->_connection = &$connection;
            $this->_table = $tableName;

            foreach (\Score\Helper::GetClassConstants($this, "MAP_") as $constant)
            {
                if (empty($this->_dataSet) && $constant == self::MAP_DATASET)
                {
                    \Score\Logger::ByGlobal(__METHOD__, "Data Mapping requires manual setup");
                    // This require manual setup
                    continue;
                }

                if (empty($constant))
                {
                    \Score\Logger::ByGlobal(__METHOD__, "Data Mapping not supported");
                    // Not supported
                    continue;
                }

                $this->setMapping($constant);
            }

            return $this;
        }

        public function setDataset($columnName, $value)
        {
            $this->_dataset = $value;
            $this->setMapping(self::MAP_DATASET, $columnName);
        }

        public function &setMapping($mapField, $columnName = null)
        {
            $this->_map[$mapField] = (empty($columnName) || !is_string($columnName) ? ltrim($mapField, ':') : $columnName);
            return $this;
        }

        public function getNewer($key, $date)
        {
            $item = $this->_getItem($key);

            if (Time::Compare($item->Created, $date) !== Time::COMPARE_LT)
            {
                return $this->GetData($item);
            }

            return false;
        }

        protected function _getBase($key)
        {
            $base = array($this->_map[self::MAP_KEY] => $key);

            if (!empty($this->_dataset))
            {
                $base[$this->_map[self::MAP_DATASET]] = $this->_dataset;
            }

            return $base;
        }

        protected function _getItem($key)
        {
            $item = new \Score\Cache\Item();
            $fields = $this->_getBase($key);
            $result = $this->_connection->SelectBy($this->_table, $fields, "\\Score\\DB\\ResultSet");

            if ($result->Status != \Score\DB\ResultSet::STAT_GOOD)
            {
                $item->Init($key, null);
            }
            else
            {
                $result = $result->Data[0];
                $item->Data = unserialize($result[$this->_map[self::MAP_DATA]]);
                $item->Key = $key;
                $item->Created = $result[$this->_map[self::MAP_CREATED]];
                $item->Expires = $result[$this->_map[self::MAP_EXPIRES]];
            }

            return $item;
        }

        public function get($key)
        {
            return $this->GetData($this->_getItem($key));
        }

        public function flush($onlyExpired)
        {
            $sql = "DELETE FROM " . $this->_connection->QualifyObject($this->_table);
            $where = array();
            $fields = array();

            if (!empty($this->_dataset))
            {
                $fields[$this->_map[self::MAP_DATASET]] = $this->_dataset;
                $where[] = $this->_connection->QualifyObject($this->_map[self::MAP_DATASET]) . ' = :' . $this->_map[self::MAP_DATASET];
            }

            if ($onlyExpired)
            {
                $fields[$this->_map[self::MAP_EXPIRES]] = \Score\Time::QuickStamp();
                $where[] = $this->_connection->QualifyObject($this->_map[self::MAP_EXPIRES]) . ' < :' . $this->_map[self::MAP_EXPIRES];
            }

            if (!empty($where))
            {
                $sql .= " WHERE " . implode(" AND ", $where);
            }

            $prep = $this->_connection->prepare($sql);
            $prep->execute($fields);
        }

        public function set($key, $data, $lifetime)
        {
            $fields = $this->_getBase($key);
            $new = $fields;

            $sel = $this->_connection->SelectBy($this->_table, $fields, false);

            $item = new \Score\Cache\Item();
            $item->Init($key, $data, $lifetime);

            $new[$this->_map[self::MAP_CREATED]] = $item->Created;
            $new[$this->_map[self::MAP_DATA]] = serialize($item->Data);
            $new[$this->_map[self::MAP_EXPIRES]] = $item->Expires;

            if (!empty($sel))
            {
                $crit = $this->_connection->PrepValues($fields, \Score\DB\Connection::PREPTYPE_WHERE);
                $critWhere = implode(" AND ", $crit);

                $new = array_merge($sel[0], $new);
                $res = $this->_connection->Update($this->_table, $critWhere, $new);
            }
            elseif (!empty($new[$this->_map[self::MAP_KEY]]))
            {
                $result = $this->_connection->Insert($this->_table, $new, "\\Score\\DB\\ResultSet");
            }

            if ($result->Status != \Score\DB\ResultSet::STAT_GOOD)
            {
                // @todo throw exception
            }
        }

        public function del($key)
        {
            $fields = $this->_getBase($key);

            $this->_connection->Delete($this->_table, $fields);
        }
    }
}
