<?php

namespace Score\Cache\Adapter
{
    abstract class Base
    {
        public $Namespaces;

        public function __construct($namespaces = array())
        {
            if (!is_array($namespaces))
            {
                $namespaces = array($namespaces);
            }

            $this->Namespaces = $namespaces;
        }

        public function GetData(\Score\Cache\Item &$item)
        {
            if ($item->IsExpired())
            {
                $this->del($item->Key);
                return false;
            }

            return $item->Data;
        }

        public function flush($onlyExpired)
        {
        }

        public function getNewer($key, $date)
        {
            return false;
        }

        abstract public function get($key);

        abstract public function set($key, $data, $lifetime);

        abstract public function del($key);
    }
}