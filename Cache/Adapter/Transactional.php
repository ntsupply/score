<?php

namespace Score\Cache\Adapter
{
    class Transactional extends \Score\Cache\Adapter\Base
    {
        protected $_datastore = array();

        public function getNewer($key, $date)
        {
            // Not really an issue for Transaction for now
            return $this->get($key);
        }

        public function get($key)
        {
            if (isset($this->_datastore[$key]))
            {
                return $this->GetData($this->_datastore[$key]);
            }

            return false;
        }

        public function set($key, $data, $lifetime)
        {
            if (!isset($this->_datastore[$key]) && empty($data))
            {
                return;
            }

            if (is_null($data))
            {
                unset($this->_datastore[$key]);
            }
            else
            {
                $item = new \Score\Cache\Item();
                $item->Init($key, $data, $lifetime);

                $this->_datastore[$key] = $item;
            }
        }

        public function del($key)
        {
            if (isset($this->_datastore[$key]))
            {
                unset($this->_datastore[$key]);
            }
        }

        public function flush($onlyExpired)
        {
            if ($onlyExpired)
            {
                $this->_datastore = array();
                return;
            }

            foreach ($this->_datastore as $key => $item)
            {
                if ($item->IsExpired())
                {
                    unset($this->_datastore[$key]);
                }
            }
        }
    }
}