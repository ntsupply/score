<?php

namespace Score\Cache
{
    use \Score\Cache\Manager;

    class Worker
    {
        protected $_lifetime;
        protected $_namespace;

        public function __construct($namespace)
        {
            $this->_namespace = $namespace;
        }

        public function set($key, $data, $lifetime = null)
        {
            if (is_null($lifetime))
            {
                $lifetime = $this->_lifetime;
            }

            Manager::set($this->_namespace, $key, $data, $lifetime);
        }

        public function getNewer($key, $date)
        {
            return Manager::getNewer($this->_namespace, $key, $date);
        }

        public function get($key)
        {
            return Manager::get($this->_namespace, $key);
        }

        public function del($key)
        {
            return Manager::del($this->_namespace, $key);
        }

        public function Lifetime($minutes)
        {
            $this->_lifetime = $minutes;
        }
    }
}