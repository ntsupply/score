<?php

namespace Score\Cache
{
    use \Score\Time;

    class Item
    {
        use \Score\Helper\ScrapePopulate;

        public $Key;
        public $Data;
        public $Created;
        public $Expires;
        public $Lifetime;

        public function Init($key, $data, $lifetime = null)
        {
            if (is_null($lifetime))
            {
                $lifetime = \Score\Cache\Manager::$defaultLifetime;
            }

            $dt = new Time();
            $dt->setFormat(Time::FORMAT_DTS);

            $this->Created = $dt->Display();
            $this->Lifetime = $lifetime;

            $dt->add(new \DateInterval(\Score\Strings\Format::Write("PT{0}M", $lifetime)));
            $this->Expires = $dt->Display();

            $this->Key = $key;
            $this->Data = $data;
        }

        public function Populate(array $properties)
        {
            $this->_populate($properties);
        }

        public function IsExpired()
        {
            if (empty($this->Expires) || Time::Compare($this->Expires) === \Score\Time::COMPARE_LT)
            {
                return true;
            }

            return false;
        }
    }
}