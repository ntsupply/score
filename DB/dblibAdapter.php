<?php

namespace Score\DB
{
    use \Score\Strings\Format;

    class dblibAdapter extends \Score\DB\defaultAdapter
    {

        public $objectQualifierLeft = "["; // Microsoft Special
        public $objectQualifierRight = "]"; // Microsoft Special

        public function __construct(\PDO &$PDO, $DSN)
        {
            parent::__construct($PDO, $DSN);

            // 2008 and below only
            if (version_compare($this->getServerVersion(), '11.0', '<'))
            {
                $this->PDO()->setAttribute(\PDO::ATTR_EMULATE_PREPARES, 1);
            }
        }

        /**
         * Get the Meta Table with Columns from a specific table
         * @param string $tableName Table to get meta information from
         * @param string $schema    Set or auto detect Schema
         * @param string $catalog   Set or auto detect Cataglog
         * @return MetaTable
         */
        public function getMetaTable($tableName, $schema = null, $catalog = null)
        {
            $tableInfo = new MetaTable();
            $tableInfo->schema = $schema;
            $tableInfo->catalog = $catalog;
            $tableInfo->tableName = $tableName;

            if (empty($tableInfo->schema) && preg_match('/(dbname|database)=(.*?)(;|\z)/imx', $this->_dsn, $regs))
            {
                $tableInfo->catalog = $regs[2];
            }

            if (strpos($tableName, ".") !== false)
            {
                list($tableInfo->schema, $tableInfo->tableName) = explode(".", $tableName);
            }
            else
            {
                $tableInfo->schema = 'dbo';
                $tableInfo->tableName = $tableName;
            }

            $this->PopulateMetaColumns($tableInfo);

            return $tableInfo;
        }

        public function Insert($tableName, array $values, $useResultSet = null)
        {
            $fields = $this->QualifyObject(array_keys($values));

            $this->LastErrorSet(null);
            $this->PrepValues($values);

            $sql = sprintf("INSERT INTO %s (%s) VALUES (%s)", $this->QualifyObject($tableName), implode(",", $fields), implode(",", array_keys($values)));
            $res = $this->PrepResultSet($useResultSet);
            $res->Prepare($this->PDO()->prepare($sql));

            if ($res->Prepare()->execute($values))
            {
                $result = $this->PDO()->query("SELECT CAST(COALESCE(SCOPE_IDENTITY(), @@IDENTITY) AS int)");
                $result->execute();
                $result = $result->fetch();

                $res->Data = $result[0];
                $res->Status = ResultSet::STAT_GOOD;
                $res->Prepare()->returnValue = $res->Data;
            }
            else
            {
                $this->LastErrorSet($res->Prepare()->lastError);

                $res->Status = ResultSet::STAT_ERROR;
                $res->Message = $this->LastErrorMessage();
                $res->Prepare()->returnValue = false;
            }

            if ($res->UseSimpleResult === true)
            {
                return $res->Data;
            }

            return $res;
        }

        /**
         * Check for the existence of a record
         * @param string $query
         * @param array $values  Optional Associative Array of name and values to replace in SQL.
         * @param mixed $useResultSet Optional set the use of a specific resultset
         * @return mixed
         */
        public function GetFirstRow($query, array $values = array(), $useResultSet = null)
        {
            $limit = 1;
            $query = str_ireplace("SELECT ", "SELECT TOP(1) ", $query, $limit);

            $this->LastErrorSet(null);
            $this->PrepValues($values);

            $res = $this->PrepResultSet($useResultSet);
            $res->Prepare($this->PDO()->prepare($query));

            if ($res->Prepare()->execute($values))
            {
                $res->Data = $res->Prepare()->fetch(\PDO::FETCH_ASSOC);

                if (empty($res->Data))
                {
                    $res->Status = ResultSet::STAT_NODATA;
                }
                else
                {
                    $res->Status = ResultSet::STAT_GOOD;
                }
            }
            else
            {
                $this->LastErrorSet($res->Prepare()->lastError);

                $res->Status = ResultSet::STAT_ERROR;
                $res->Message = $this->LastErrorMessage();
            }

            if ($res->UseSimpleResult === true)
            {
                return $res->Data;
            }

            return $res;
        }

        /**
         * Fetch the first column
         * @param string $query
         * @return mixed
         */
        public function GetFirstColumn($query, array $values = array())
        {
            $limit = 1;
            $query = str_ireplace("SELECT ", "SELECT TOP(1) ", $query, $limit);

            $this->PrepValues($values);

            $set = false;
            $res = $this->PrepResultSet($set);
            $res->Prepare($this->PDO()->prepare($query));

            if ($res->Prepare()->execute($values))
            {
                return $res->Prepare()->fetchColumn();
            }
            else
            {
                $this->LastErrorSet($res->Prepare()->lastError);
                return null;
            }
        }

        public function getServerVersion()
        {
            return $this->PDO()->query("SELECT SERVERPROPERTY('productversion')")->fetchColumn();
        }

        /**
         * Check for the existence of a record
         * @param string $tableName
         * @param array $values  Optional Associative Array of name and values to replace in SQL.
         * @return boolean
         * @example CanFind("MyTable", array("id" => 123))
         */
        public function CanFind($tableName, array $values = array())
        {
            $this->LastErrorSet(null);

            if (empty($values))
            {
                $select = Format::Write("SELECT COUNT(*) FROM `{0}`", $tableName);
            }
            else
            {
                $crit = $this->PrepValues($values, Connection::PREPTYPE_WHERE);
                $crit = implode(" AND ", $crit);

                $select = Format::Write("SELECT 1 FROM {0} WHERE {1}", $tableName, $crit);
            }

            $prep = $this->PDO()->prepare($select);

            if ($prep->execute($values))
            {
                return $prep->fetchColumn() == 1;
            }

            return false;
        }

    }

}
