<?php

namespace Score\DB
{

    use \Score\DB\Connection;

    class Prepare extends \PDOStatement
    {

        protected $_db;
        public $lastExecuted;
        public $lastError;
        public $returnValue;
        public $startTime;
        public $stopTime;

        protected function __construct(Connection &$db)
        {
            $this->_db = & $db;
        }

        public function execute($input_parameters = null)
        {
            if ($this->_db instanceof Connection)
            {
                if ($this->_db->getAttribute(Connection::ATTR_QUERY_STOPWATCH) === true)
                {
                    $time = explode(".", microtime(true));
                    $this->startTime = $time;
                }
            }

            $this->lastError = null;
            $this->lastExecuted = $input_parameters;

            try
            {
                $this->returnValue = parent::execute($input_parameters);
            }
            catch (\Exception $err)
            {
                \Score\Logger::ByGlobal(__METHOD__, $err->getMessage() . "\n\tQuery: {$this->queryString}\n\tParams: " . \Score\Logger::PrintVar($input_parameters));
                $this->return = false;
                $this->lastError = $err->getMessage();
            }

            if ($this->_db instanceof Connection)
            {
                if ($this->_db->getAttribute(Connection::ATTR_QUERY_STOPWATCH) === true)
                {
                    $time = explode(".", microtime(true));
                    $this->stopTime = $time;
                }
            }

            return $this->returnValue;
        }

    }

}
