<?php

namespace Score\DB
{
    /**
     * SQLite adapter
     * @author Bradley Worrell-Smith
     */
    class sqliteAdapter extends \Score\DB\defaultAdapter
    {
        public $blockAttr = array(\PDO::ATTR_AUTOCOMMIT);
        public $objectQualifierLeft = '"';
        public $objectQualifierRight = '"';

        /**
         * Get Schema/DB Name
         * @return string
         */
        protected function _getSchemaName()
        {
            $schema = null;

            try
            {
                $prep = $this->PDO()->prepare("PRAGMA database_list");
                $prep->execute();
                $row = $prep->fetch();

                if ($row)
                {
                    $schema = $row["name"];
                }
            }
            catch (\PDOException $err)
            {
            }

            return $schema;
        }

        /**
         * Get a list of tables
         * @return bool|array
         */
        public function getTableList()
        {
            $tableList = array();

            try
            {
                $prep = $this->PDO()->prepare("SELECT * FROM sqlite_master WHERE type='table' ORDER BY name");
                $prep->execute();
                $tables = $prep->fetchAll();

                if ($tables)
                {
                    foreach ($tables as $table)
                    {
                        $tableList[] = $table["tbl_name"];
                    }
                }
            }
            catch (\PDOException $err)
            {
                return false;
            }

            return $tableList;
        }

        /**
         * Get the Meta Table with Columns from a specific table
         * @param string $tableName Table to get meta information from
         * @param string $schema    Set or auto detect Schema
         * @param string $catalog   Set or auto detect Cataglog
         * @return MetaTable
         */
        public function getMetaTable($tableName, $schema = null, $catalog = null)
        {
            $tableInfo = new MetaTable();
            $tableInfo->schema = $schema;
            $tableInfo->catalog = $catalog;
            $tableInfo->tableName = $tableName;

            if (empty($tableInfo->schema))
            {
                $tableInfo->schema = $this->_getSchemaName;
            }

            $this->PopulateMetaColumns($tableInfo);

            return $tableInfo;
        }

        /**
         * Used to populate table info with columns
         * @param MetaTable $tableInfo MetaTable Object to populate
         */
        public function PopulateMetaColumns(MetaTable &$tableInfo)
        {
            $tableInfo->delColumn();

            try
            {
                $prep = $this->PDO()->prepare("PRAGMA table_info($tableName)");
                $prep->execute();
                $columns = $prep->fetchAll();

                if ($columns)
                {
                    foreach ($columns as $num => $field)
                    {
                        $hdCol = new MetaColumn();
                        $hdCol->dataType = $field["type"];
                        $hdCol->name = $field["name"];
                        $hdCol->order = $num;
                        $hdCol->keyType = ($field["pk"] ? "PRI" : "");
                        $hdCol->defSet = true;
                        $hdCol->defValue = $field["dflt_value"];
                        $hdCol->isNullable = ($field["notnull"] === 0 ? true : false);
                        $hdCol->isNumeric = ($field["type"] === "REAL" || $field["type"] === "NUMERIC" ? true : false);
                        $hdCol->length = 255;

                        $tableInfo->addColumn($hdCol);
                    }
                }
            }
            catch (\PDOException $err)
            {
                $tableInfo->error = $err->getMessage();
            }

            return $tableInfo;
        }
    }
}
