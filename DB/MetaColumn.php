<?php

namespace Score\DB
{
    /**
     * MetaColumn Information
     * @author Bradley Worrell-Smith
     */
    class MetaColumn
    {
        public $dataType;
        public $name;
        public $order = 0;
        public $length;
        public $precision = 0;
        public $defSet = false;
        public $defValue;
        public $keyType = "";
        public $extra = "";
        public $isNumeric = false;
        public $isNullable = false;
    }
}
