<?php

namespace Score\DB
{
    use Score\DB\ResultSet;

    class ResultSetHelper extends \Score\DB\ResultSet
    {
        public function __construct($iData = null, $iStatus = null, $iMessage = null)
        {
            if ( empty($iStatus) )
            {
                $iStatus = ResultSet::STAT_GOOD;
            }

            $this->Data = $iData;
            $this->Status = $iStatus;
            $this->Message = $iMessage;
        }

        public static function MakeGood($iData = null, $iMessage = null)
        {
            return new ResultSetHelper($iData, ResultSet::STAT_GOOD, $iMessage);
        }

        public static function MakeError($iMessage, $iData = null)
        {
            return new ResultSetHelper($iData, ResultSet::STAT_ERROR, $iMessage);
        }

        public static function MakeWarning($iMessage, $iData = null)
        {
            return new ResultSetHelper($iData, ResultSet::STAT_WARNING, $iMessage);
        }

        public static function MakeNoData($iMessage = null, $iData = null)
        {
            return new ResultSetHelper($iData, ResultSet::STAT_NODATA, $iMessage);
        }

        public static function MakeInvalid($iMessage = null, $iData = null)
        {
            return new ResultSetHelper($iData, ResultSet::STAT_INVALID, $iMessage);
        }
    }
}