<?php

namespace Score\DB\Model
{
    use \Score\Logger;

    class Binding
    {
        /**
         * @var \Score\DB\Connection
         */
        public $dbconn;
        public $table;
        public $fields;
        public $key;
        public $select;
        public $populated = false;
        public $cacheKey;

        protected $_relations = [];
        protected $_filters = [
            self::FILTER_MODE_POPULATE => [],
            self::FILTER_MODE_SAVE => [],
        ];

        const FILTER_MODE_POPULATE = 1;
        const FILTER_MODE_SAVE = 2;

        /**
         * Undocumented function
         *
         * @param string $table
         * @param array $fields
         * @param string $key
         * @return \Score\DB\Model\Binding
         */
        public function &Map($table, $fields = null, $key = "id")
        {
            $this->table = $table;
            $this->fields = $fields;
            $this->key = $key;
            return $this;
        }

        public function &addRelation(\Score\DB\Model\Relation $relation)
        {
            $this->_relations[$relation->Slug()] = $relation;
            return $this;
        }

        /**
         * Undocumented function
         *
         * @param string $field
         * @param callback $callback
         * @param int $mode
         * @return \Score\DB\Model\Binding
         */
        public function &addFilter($field, $callback, $mode = self::FILTER_MODE_POPULATE)
        {
            $this->_filters[$mode][] = ["f" => strtolower($field), "c" => $callback];
            return $this;
        }

        /**
         * @param string $field
         * @return \Score\DB\Model\Binding
         */
        public function &setJsonFilter($field, $assoc = false)
        {
            $this->addFilter($field, function (&$val, &$data) use ($assoc) {
                if (!empty($val) && is_string($val))
                {
                    $val = json_decode($val, $assoc);
                }
            }, self::FILTER_MODE_POPULATE);

            $this->addFilter($field, function (&$data) use ($assoc) {
                $data = (!empty($data) ? \Score\Strings::EncodeJsonUTF8($data) : ($assoc ? '[]' : null)); // don't forget to quote the array.. lazy
            }, self::FILTER_MODE_SAVE);

            return $this;
        }

        /**
         * Undocumented function
         *
         * @param array $data
         * @param int $mode
         * @return array
         */
        public function applyFilters(array $data, $mode)
        {
            if (!empty($this->_filters[$mode]))
            {
                // Process this in order of the filter added, that way if a susequent filter can rely on a prior filter
                foreach ($this->_filters[$mode] as &$filter)
                {
                    foreach ($data as $fieldName => &$fieldValue)
                    {
                        $fieldName = strtolower($fieldName);
    
                        if ($filter["f"] == $fieldName)
                        {
                            try
                            {
                                $fn = $filter["c"];
                                $fn($fieldValue, $data);
                            }
                            catch (\Exception $ex)
                            {
                                // Do nothing and continue
                            }
                        }
                    }
                }
            }

            return $data;
        }

        public function Valid()
        {
            if (empty($this->table))
            {
                Logger::WriteStack(__METHOD__, "Invalid Binding on Model " . $this->table);
                return false;
            }

            if (empty($this->dbconn))
            {
                Logger::WriteStack(__METHOD__, "Invalid Connection on Model " . $this->table);
                return false;
            }

            return true;
        }
    }
}