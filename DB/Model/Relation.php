<?php

namespace Score\DB\Model
{
    class Relation
    {
        public $className;
        public $classField;
        public $sourceField;

        public function __construct($className, $classField, $sourceField = "id")
        {
            $this->className = $className;
            $this->classField = $classField;
            $this->sourceField = $sourceField;
        }

        public function Slug()
        {
            return implode("-", array($this->className, $this->classField, $this->sourceField));
        }
    }
}