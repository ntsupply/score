<?php

namespace Score\DB\Model
{
    class Generator
    {
        protected $_db;
        protected $_ns = "app";
        protected $_nd = "\t";

        public function __construct(\Score\DB\Connection $conn)
        {
            $this->_db = $conn;
        }

        public function setNamespace($ns)
        {
            $this->_ns = trim($ns, '\/');

        }

        public function setIndent($spacesOrTabs)
        {
            $this->_nd = $spacesOrTabs;
        }

        public function renderTable($table, $dir = null)
        {
            $meta = $this->_db->getMetaTable($table);

            $namespace = $this->_ns;
            $classPath = explode(" ", ucwords(str_replace('_', ' ', $meta->tableName)));

            $class = array_pop($classPath);

            if (sizeof($classPath) > 0)
            {
                $namespace .= '\\' . implode('\\', $classPath);

                if (!empty($dir))
                {
                    $dir = \Score\Server::SetupFilePath($dir . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $classPath));
                }
            }

            $out ="<?php\n\n" .
                "namespace {$namespace}\n{\n" .
                "\tclass {$class} extends \\Score\\DB\\Model\n\t{\n";

            foreach ($meta as $column)
            {
                $out .= \Score\Strings::Write("\t\tpublic {0}{1};\n", '$', $column->name);
            }

            $out .= "\n\t\tpublic function __construct(\$from = null)\n" .
                    "\t\t{\n" .
                    "\t\t\t\$this->Binding()->Map(\"{$meta->tableName}\");\n" .
                    "\t\t\t\$this->Populate(\$from);\n" .
                    "\t\t}\n" .
                    "\t}\n}\n";

            $code = str_replace("\t", $this->_nd, $out);

            if ($dir)
            {
                \file_put_contents($dir . DIRECTORY_SEPARATOR . $class . '.php', $code);
            }
            else
            {
                return $code;
            }
        }
    }
}