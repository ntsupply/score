<?php

namespace Score\DB
{

    use \Score\DB\Model\Binding;
    use \Score\Logger;
    use \Score\Cache\Worker as CacheWorker;

    class Model
    {
        private $__score_model_binding;

        use \Score\Helper\ScrapePopulate;
        const CACHE_OBJECT_MODEL = "OBJECT_MODELS";

        /**
         * Sleep called when serializing
         * @return void
         */
        public function __sleep()
        {
            $vars = array_keys(get_object_vars($this));

            unset($vars["__score_model_binding"]);

            return $vars; // This needs to be here or it won't serialze the object
        }

        /**
         * Binding info
         * @return \Score\DB\Model\Binding
         */
        public function &Binding(\Score\DB\Connection &$dbConn = null)
        {
            if (!$this->__score_model_binding instanceof Binding)
            {
                $this->__score_model_binding = new Binding();
            }

            if ($dbConn instanceof \Score\DB\Connection)
            {
                $this->__score_model_binding->dbconn = &$dbConn;
            }

            return $this->__score_model_binding;
        }

        /**
         * Cache Worker
         * @staticvar \Score\Cache\Worker $cache
         * @return \Score\Cache\Worker
         */
        public static function &Cache()
        {
            static $cache = null;

            if (!$cache instanceof CacheWorker)
            {
                $cache = new CacheWorker(self::CACHE_OBJECT_MODEL);
                $cache->Lifetime(5);
            }

            return $cache;
        }

        /**
         * Undocumented function
         *
         * @param \Score\Data\Set $result
         * @param boolean $doit
         * @param boolean $singleton
         * @return void
         */
        public static function Factory(\Score\Data\Set &$result, $doit = false, $singleton = false)
        {
            $class = get_called_class();

            if ($doit === true)
            {
                if ($result->Status == \Score\Data\Set::STAT_GOOD)
                {
                    foreach ($result->Data as &$row)
                    {
                        $row = new $class($row);

                        if ($singleton)
                        {
                            return $row;
                        }
                    }

                    return $result->Data;
                }
                elseif ($singleton)
                {
                    return new $class();
                }
            }
        }

        /**
         * Undocumented function
         *
         * @param mixed $from
         * @return void
         */
        public function Populate($from = null)
        {
            if (empty($from))
            {
                return false;
            }

            if (is_array($from))
            {
                $pop = $this->Binding()->applyFilters($from, \Score\DB\Model\Binding::FILTER_MODE_POPULATE);
                return $this->_populate($pop);
            }

            $key = $this->Binding()->key;

            return $this->PopulateBy(array($key => $from));
        }

        public function PopulateBy(array $critera)
        {
            if (empty($this->Binding()->table))
            {
                return false;
            }

            $select = $this->Binding()->select;
            $this->Binding()->cacheKey = implode("|", array(__CLASS__, $this->Binding()->table, serialize($critera)));

            $cacheData = self::Cache()->get($this->Binding()->cacheKey);

            if (!empty($cacheData))
            {
                $this->Binding()->populated = true;
                $pop = $this->Binding()->applyFilters($cacheData, \Score\DB\Model\Binding::FILTER_MODE_POPULATE);
                return $this->_populate($pop);
            }

            if (empty($select))
            {
                $filteredCritera = array();

                foreach ($critera as $key => $value)
                {
                    if (!is_null($key))
                    {
                        $filteredCritera[$key] = $value;
                    }
                }

                if (empty($filteredCritera))
                {
                    $res = ResultSet::MakeInvalid("Missing criteria for Model");
                }
                else
                {
                    $res = $this->Binding()->dbconn->SelectBy($this->Binding()->table, $filteredCritera);
                }
            }
            else
            {
                $crit = implode(" AND ", $this->Binding()->dbconn->PrepValues($critera, \Score\DB\Connection::PREPTYPE_WHERE, $this->Binding()->table));
                $select = "{$select} WHERE {$crit}";

                $res = $this->Binding()->dbconn->Select($select, $critera);
            }

            if ($res->Status == ResultSet::STAT_ERROR)
            {
                Logger::WriteStack(__METHOD__, "Failed to get record for " . get_class($this) . "\n" . $select . "\n" . $res->Message);
            }
            elseif ($res->Status != ResultSet::STAT_GOOD)
            {
                Logger::WriteStack(__METHOD__, "Failed to get record for " . get_class($this) . "\n" . $select, Logger::TYPE_VERBOSE);
            }
            elseif ($res->Status == ResultSet::STAT_GOOD)
            {
                self::Cache()->set($this->Binding()->cacheKey, $res->Data[0]);
                $pop = $this->Binding()->applyFilters($res->Data[0], \Score\DB\Model\Binding::FILTER_MODE_POPULATE);
                return $this->_populate($pop);
            }

            return false;
        }

        public function Delete()
        {
            $key = $this->Binding()->key;

            if (empty($this->Binding()->table))
            {
                return ResultSet::MakeInvalid("Optional feature not implimented");
            }

            $res = $this->Binding()->dbconn->Delete($this->Binding()->table, array($key => $this->$key));
            self::Cache()->del($this->Binding()->cacheKey);

            if ($res->Status == ResultSet::STAT_GOOD)
            {
                $this->$key = null;
            }

            return $res;
        }

        public function Save()
        {
            $key = $this->Binding()->key;

            if (!$this->Binding()->Valid())
            {
                return ResultSet::MakeInvalid("Optional feature not implimented");
            }

            $res = null;

            $meta = $this->Binding()->dbconn->getMetaTable($this->Binding()->table);
            
            if (empty($this->Binding()->fields))
            {
                $this->Binding()->fields = $meta->getColumnList();
            }

            if (empty($this->Binding()->fields))
            {
                return ResultSet::MakeInvalid("Unable to get Meta Information for " .  $this->Binding()->table);
            }

            $data = $this->_scrape($this->Binding()->fields);

            // Fix up/check data that it's suitable for table
            foreach ($data as $dataCol => &$dataValue)
            {
                $dataType = gettype($this->$dataCol);
                switch ($dataType)
                {
                    case "boolean":
                        $metaCol = $meta->getColumn($dataCol);
                        switch ($metaCol->dataType)
                        {
                            case 'int':
                            case 'tinyint':
                                $dataValue = intval($dataValue);
                                break;
                        }
                        break;
                    case "string":
                        $metaCol = $meta->getColumn($dataCol);
                        if (is_null($dataValue) && !$metaCol->isNullable)
                        {
                            $dataValue = '';
                        }
                        break;
                }
            }

            $val = $this->$key;
            self::Cache()->del($this->Binding()->cacheKey);

            $data = $this->Binding()->applyFilters($data, \Score\DB\Model\Binding::FILTER_MODE_SAVE);

            if (empty($val) || (!empty($val) && !$this->Binding()->dbconn->CanFind($this->Binding()->table, array($key => $val))))
            {
                if (empty($val))
                {
                    unset($data[$key]);
                }

                $res = $this->Binding()->dbconn->Insert($this->Binding()->table, $data);

                if ($res->Status == ResultSet::STAT_GOOD)
                {
                    if (empty($val))
                    {
                        $this->$key = $res->Data;
                    }
                }
                else
                {
                    Logger::WriteStack(__METHOD__, "Failed to insert record for " . get_class($this) . "\n" . $res->Prepare()->lastError);
                }
            }
            else
            {
                unset($data[$key]);
                $res = $this->Binding()->dbconn->Update($this->Binding()->table, "$key = " . $this->Binding()->dbconn->quote($val), $data);

                if ($res->Status != ResultSet::STAT_GOOD)
                {
                    Logger::WriteStack(__METHOD__, "Failed to update record for " . get_class($this) . "\n" . $res->Prepare()->lastError);
                }
            }

            return $res;
        }
    }
}
