<?php

namespace Score\DB
{
    use \Score\DB\Connection;
    use \Score\DB\ResultSet;

    /**
     * DataTable storage and update object
     * @author Bradley Worrell-Smith
     */
    class DataTable extends \Score\Data\Table
    {
        const EXCEPTION_MISMATCH = "(100803) Mismatched Data between headers and values";

        protected $_dbconn;
        protected $_tableInfo;
        protected $_tableName;
        protected $_prep;

        public $Status = "";
        public $Message;

        const COMMIT_UPDATE = 1;
        const COMMIT_INSERT = 2;
        const COMMIT_DELETE = 4;

        /**
         * Create a new Data Table
         * @param \Score\DB\Connection $iDB Connection Object
         * @param string $iTable Table Name
         * @return void
         */
        public function __construct(Connection $iDB = null, $iTable = null)
        {
            $this->_dbconn = $iDB;

            if (!empty($iTable) && empty($this->_tableName))
                $this->_tableName = $iTable;

            if (!empty($this->_tableName))
                $this->SetTable($this->_tableName);

            return $this;
        }

        /**
         * Table name
         * @param sting $iTable
         * @return \Score\DB\DataTable
         */
        public function &SetTable($iTable)
        {
            $this->delIndex();
            $this->Status = "";
            $this->Message = null;
            $this->_data = array();
            $this->_headers = array();
            $this->_tableInfo = $this->_dbconn->getMetaTable($iTable);
            $this->_tableName = $iTable;

            foreach ($this->_tableInfo as $metadata)
            {
                $this->_headers[] = $metadata->name;
            }

            $keyCol = $this->_keyCol();

            if (!empty($keyCol))
            {
                $this->addIndex($keyCol->name);
            }

            return $this;
        }

        /**
         * Set the connection
         * @param \Score\DB\Connection $iDBConn
         * @return \Score\DB\DataTable
         */
        public function &SetConnection(Connection &$iDBConn)
        {
            $this->_dbconn =& $iDBConn;
            return $this;
        }

        /**
         * Default blank array with keys from header
         * @return array
         */
        public function getNewRow()
        {
            $new = parent::getNewRow();

            foreach($this->_tableInfo as $metadata)
            {
                if (!$metadata->defSet)
                    continue;

                if (array_search($metadata->dataType, array('int', 'tinyint', 'smallint', 'mediumint', 'bigint', 'decimal', 'float', 'double', 'real', 'char', 'varchar', 'tinytext')))
                    $new[$metadata->name] = $metadata->defValue;

                if ($metadata->defValue == "CURRENT_TIMESTAMP")
                    $new[$metadata->name] = \Score\Time::QuickStamp();

                if ($metadata->isNullable === false && is_null($metadata->defValue))
                    $new[$metadata->name] = "";
            }

            return $new;
        }

        /**
         * Get the key column from a table
         * @return \Score\DB\MetaColumn
         */
        protected function _keyCol()
        {
            $keyCol = null;

            foreach($this->_tableInfo as $metadata)
            {
                if ($metadata->keyType == "PRI")
                {
                    $keyCol = $metadata;
                    $this->addIndex($keyCol->name);
                    break;
                }
            }

            return $keyCol;
        }

        /**
         * Select Yadda
         * @param string $iWhere
         * @param array $iValues
         * @return \Score\DB\DataTable
         */
        public function &Select($iWhere = null, array $iValues = array())
        {
            $sql = "SELECT * FROM " . $this->_dbconn->Adapter()->QualifyObject($this->_tableName);
            $this->_dbconn->PrepValues($iValues);

            if (!empty($iWhere))
            {
                $sql = "$sql WHERE $iWhere";
            }

            $ret = null;
            $prep = $this->_dbconn->prepare($sql);

            try
            {
                $prep->execute($iValues);
                $this->_data = $prep->fetchAll(\PDO::FETCH_NUM);

                if (empty($this->_data))
                {
                    $this->Status = ResultSet::STAT_NODATA;
                }
                else
                {
                    $this->Status = ResultSet::STAT_GOOD;
                    $this->updateIndex();
                }
            }
            catch ( \Exception $err )
            {
                $ret = false;
                $this->Message = $err->getMessage();
            }

            if (!$ret)
            {
                $this->Status = ResultSet::STAT_ERROR;
            }

            $this->_prep = get_object_vars($prep);
            return $this;
        }

        /**
         * Insert Yadda
         * @param bool $iAutoSeq  Auto Increment on key sequences
         * @return \Score\DB\DataTable
         * @throws \Score\Exception
         */
        public function Insert($iAutoSeq = true)
        {
            $keyCol = $this->_keyCol();
            $hdr = array();
            $ref = array();

            foreach ($this->_headers as $name)
            {
                if ($iAutoSeq && $keyCol->name == $name)
                {
                    continue;
                }

                $hdr[] = $this->_dbconn->Adapter()->QualifyObject($name);
                $ref[] = ":$name";
            }

            $sql = sprintf("INSERT INTO %s (%s) VALUES (%s)", $this->_dbconn->Adapter()->QualifyObject($this->_tableName), implode(",", $hdr), implode(",", $ref));
            $prep = $this->_dbconn->prepare($sql);

            foreach ($this->_data as &$row)
            {
                if (sizeof($this->_headers) != count($row))
                {
                    throw new \Score\Exception(self::EXCEPTION_MISMATCH);
                }

                $new = $row;
                if ($iAutoSeq)
                {
                    if (!empty($row[$keyCol->order]))
                    {
                        // Lets skip this because it exists and should not be Inserted
                        continue;
                    }

                    unset($new[$keyCol->order]);
                }

                $new = array_combine($ref, $new);
                $prep->execute($new);
                $newID = $this->_dbconn->lastInsertId();

                if ($iAutoSeq && is_numeric($newID))
                {
                    $row[$keyCol->order] = (int)$newID;
                }
            }

            $this->_prep = get_object_vars($prep);
            return $this;
        }

        /**
         * Update the records
         * @param string $iWhere Optional Where to override
         * @param array $iValues Values to replace in where
         * @return \Score\DB\DataTable
         * @throws \Score\Exception
         */
        public function &Update($iWhere = "", array $iValues = array())
        {
            $keyCol = $this->_keyCol();

            $ref = array();
            $hdr = array();
            $assigned = array();

            foreach ($this->_headers as $name)
            {
                $hdr[] = ":$name";
                if (empty($iWhere) && !empty($keyCol) && $keyCol->name == $name)
                {
                    continue;
                }

                $ref[] = ":$name";
                $assigned[] = $this->_dbconn->Adapter()->QualifyObject($name) . " = :$name";
            }

            if (empty($iWhere) && !empty($keyCol))
            {
                $iWhere = $this->_dbconn->Adapter()->QualifyObject($keyCol->name) . " = :" . $keyCol->name;
            }

            $sql = sprintf("UPDATE %s SET %s", $this->_dbconn->Adapter()->QualifyObject($this->_tableName), implode(",", $assigned));
            if (!empty($iWhere))
            {
                $sql .= " WHERE $iWhere";
            }

            try
            {
                $prep = $this->_dbconn->prepare($sql);

                for ($rn = 0; $rn < count($this->_data); $rn++)
                {
                    $row = $this->_data[$rn];

                    if (sizeof($this->_headers) != sizeof($row))
                    {
                        throw new \Score\Exception(self::EXCEPTION_MISMATCH);
                    }

                    $new = array_combine($hdr, $row);
                    $new = array_merge($new, $iValues);

                    $prep->execute($new);
                }

                $this->Status = ResultSet::STAT_GOOD;
                $this->Message = null;
            }
            catch (\Exception $err)
            {
                $this->Message = $err->getMessage();
                $this->Status = ResultSet::STAT_ERROR;
            }

            $this->_prep = get_object_vars($prep);
            return $this;
        }

        /**
         * 
         * @param int $mode Bitwise Commit Mode 
         */
        public function Commit($mode = 3)
        {
            if ((self::COMMIT_INSERT & $mode) == $mode)
            {
                foreach ($this->_status["new"] as $nothing => $offset)
                {

                }
            }

            if ((self::COMMIT_UPDATE & $mode) == $mode)
            {
                foreach ($this->_status["set"] as $nothing => $offset)
                {

                }
            }

            if ((self::COMMIT_DELETE & $mode) == $mode)
            {
                foreach ($this->_status["unset"] as $nothing => $unset)
                {
                    $unset["offset"];
                    $unset["data"];
                }
            }
        }

        /**
         * Get the last executed information
         * @return array
         */
        public function getLastExecuted()
        {
            return $this->_prep;
        }
    }
}
