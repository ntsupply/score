<?php

namespace Score\DB
{
    /**
     * ResultSet of data
     * @author Ron Rebennack
     * @author Bradley Worrell-Smith
     */
    class ResultSet extends \Score\Data\Set
    {
        /**
         * Prepared Object
         * @var \Score\DB\Prepare
         */
        protected $_prepare;

        /**
         * Prepare Object
         * @param \Score\DB\Prepare $iPrep
         * @return \Score\DB\Prepare
         */
        public function &Prepare(\Score\DB\Prepare $iPrep = null)
        {
            if (isset($iPrep))
            {
                $this->_prepare = $iPrep;
            }

            return $this->_prepare;
        }
    }
}
