<?php

namespace Score\DB
{
    /**
     * Windows based adapter.  Kinda like dblibAdapter, but better supported
     */
    class sqlsrvAdapter extends \Score\DB\defaultAdapter
    {
        public $objectQualifierLeft = "["; // Microsoft Special
        public $objectQualifierRight = "]"; // Microsoft Special

        public function __construct(\PDO &$PDO, $DSN)
        {
            parent::__construct($PDO, $DSN);
            $this->PDO()->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
        }

        /**
         * Get the Meta Table with Columns from a specific table
         * @param string $tableName Table to get meta information from
         * @param string $schema    Set or auto detect Schema
         * @param string $catalog   Set or auto detect Cataglog
         * @return MetaTable
         */
        public function getMetaTable($tableName, $schema = null, $catalog = null)
        {
            $tableInfo = new MetaTable();
            $tableInfo->schema = $schema;
            $tableInfo->catalog = $catalog;
            $tableInfo->tableName = $tableName;

            if (empty($tableInfo->catalog) && preg_match('/(dbname|database)=(.*?)(;|\z)/imx', $this->_dsn, $regs))
            {
                $tableInfo->catalog = $regs[2];
            }

            if (strpos($tableName, ".") !== false)
            {
                list($tableInfo->schema, $tableInfo->tableName) = explode(".", $tableName);
            }
            else
            {
                $tableInfo->schema = 'dbo';
                $tableInfo->tableName = $tableName;
            }

            $this->PopulateMetaColumns($tableInfo);

            return $tableInfo;
        }
    }
}