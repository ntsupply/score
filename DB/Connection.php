<?php

namespace Score\DB
{
    use \Score\Loader;

    /**
     * Database connection Class
     * @author Bradley Worrell-Smith
     */
    class Connection extends \PDO
    {
        const RESULTSET_NONE = false;
        const RESULTSET_DEFAULT = "\\Score\\DB\\ResultSet";

        const ATTR_RESULTSET_HANDLER = -1;
        const ATTR_QUERY_STOPWATCH = -2;

        const EXCEPTION_BADRESULTNAME = "(100801) Bad ResultSet Name or type";
        const EXCEPTION_BADRESULTOBJ = "(100802) Invalid ResultSet Object. Must be instance of Score\DB\ResultSet";

        const PREPTYPE_WHERE = 1;
        const PREPTYPE_ASSIGN = 2;

        /**
         * Holder for adapter
         * @var Score\DB\defaultAdapter;
         */
        protected $_adapter;
        /**
         * ResultSet Handler
         * false = Lazy style Insert, Select, or Update
         * @var mixed ResultSet Handler
         */
        protected $_resultsetHandler = false;
        /**
         * Globally blocked attributes
         * @var array
         */
        protected $_blockAttr = array(\PDO::ATTR_PERSISTENT);
        /**
         * Store if to Track Query times in Prepare
         * @var mixed
         */
        protected $_queryStopwatch;

        /**
         * PDO Database Communications Device
         * @param string $iDSN  DSN connection string
         * @param string $iUser User
         * @param string $iPass Password
         * @param array $iOpt   Optional PDO connection options
         */
        public function __construct($iDSN, $iUser = null, $iPass = null, array $iOpt = array())
        {
            // Don't pass attribute options to constructor because the blockAttr can stop it
            parent::__construct($iDSN, $iUser, $iPass, array());

            $adapter = "\\Score\\DB\\" . $this->getAttribute(\PDO::ATTR_DRIVER_NAME) . "Adapter";

            if (Loader::Load($adapter, true))
            {
                $this->_adapter = new $adapter($this, $iDSN);
            }
            else
            {
                $this->_adapter = new \Score\DB\defaultAdapter($this, $iDSN);
            }

            // Default these for now
            $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->setAttribute(\PDO::ATTR_STATEMENT_CLASS, array("\Score\DB\Prepare", array(&$this)));

            foreach ($iOpt as $attr => $val)
            {
                $this->setAttribute($attr, $val);
            }
        }

        /**
         * See PDO Documentation for setAttribute.  This one scrubs attributes reserved by adapter or by this class
         * @param int $attribute
         * @param int $value
         * @return mixed
         */
        public function setAttribute($attribute, $value)
        {
            if ($attribute == self::ATTR_RESULTSET_HANDLER)
            {
                if (!is_null($value) && !Loader::Load($value, true))
                {
                    throw new \Score\Exception(self::EXCEPTION_BADRESULTNAME);
                }

                $this->_resultsetHandler = $value;
                return;
            }

            if ($attribute == self::ATTR_QUERY_STOPWATCH)
            {
                $this->_queryStopwatch = ($value ? $value : false);
                return;
            }

            if (!empty($this->Adapter()->blockAttr) && array_search($attribute, $this->Adapter()->blockAttr) !== false && array_search($attribute, $this->_blockAttr) !== false)
            {
                return;
            }

            @parent::setAttribute($attribute, $value);
        }

        /**
         * See PDO Documentation
         * @param int $attribute
         * @return mixed
         */
        public function getAttribute($attribute)
        {
            if ($attribute == self::ATTR_RESULTSET_HANDLER)
            {
                return $this->_resultsetHandler;
            }

            if ($attribute == self::ATTR_QUERY_STOPWATCH)
            {
                return $this->_queryStopwatch;
            }

            return parent::getAttribute($attribute);
        }

        /**
         * Adapter
         * @return \Score\DB\defaultAdapter
         */
        public function &Adapter()
        {
            return $this->_adapter;
        }

        /**
         * Add qualifiers to table/field objects to keep keywords out of SQL
         * @param string $name Name of the Table or Field object
         * @param string $prefixObject Name of Table or Schema Object to prefix before a field
         * @return string
         */
        public function QualifyObject($name, $prefixObject = null)
        {
            return $this->Adapter()->QualifyObject($name, $prefixObject);
        }

        /**
         * Prepare a ResultSet object for returning
         * @param mixed $fallback  Fallback Resultset name.
         * @return \Score\DB\ResultSet
         * @throws \Score\Exception
         */
        public function &PrepResultSet(&$fallback = null)
        {
            return $this->Adapter()->PrepResultSet($fallback);
        }

        /**
         * Prep Values from Associative array for PDO Query/Execute string
         * @param array $iValues Associative Array of names with values
         * @param int $prepType Where or assignment
         * @param string $table Table to qualify on field names
         * @retrun array Array of assigned. Ex: array(`MyField` = :MyField)
         */
        public function PrepValues(array &$assocArray, $prepType = self::PREPTYPE_ASSIGN, $table = null)
        {
            return $this->Adapter()->PrepValues($assocArray, $prepType, $table);
        }

        /**
         * Get Schema/DB Name
         * @return string
         */
        public function getSchemaName()
        {
            return $this->Adapter()->getSchemaName();
        }

        /**
         * Get a list of tables
         * @return bool|array
         */
        public function getTableList()
        {
            return $this->Adapter()->getTableList();
        }

        /**
         * Get the Meta Table with Columns from a specific table
         * @param string $tableName
         * @return \Score\DB\MetaTable
         */
        public function getMetaTable($tableName)
        {
            return $this->Adapter()->getMetaTable($tableName);
        }

        /**
         * Truncate table
         * @param string $tableName
         */
        public function TableTruncate($tableName)
        {
            return $this->Adapter()->TableTruncate($tableName);
        }

        /**
         * Check for the existence of a record
         * @param string $tableName
         * @param array $iValues  Optional Associative Array of name and values to replace in SQL.
         * @return boolean
         * @example CanFind("MyTable", array("id" => 123))
         */
        public function CanFind($tableName, array $iValues = array())
        {
            return $this->Adapter()->CanFind($tableName, $iValues);
        }

        /**
         * Base on query given vs just a table like CanFind does
         * @param string $query
         * @param array $values  Optional Associative Array of name and values to replace in SQL.
         * @return boolean
         * @example CanFindQuery("SELECT * FROM MyTable WHERE id = :id", array("id" => 123))
         */
        public function CanFindQuery($query, array $values = array())
        {
            return $this->Adapter()->CanFindQuery($query, $values);
        }

        /**
         * Quick Select from specific table
         * @param type $tableName
         * @return array
         */
        public function getTableData($tableName)
        {
            return $this->SelectBy($tableName, array(), false);
        }

        /**
         * Query with select statment
         * @param string $query
         * @param array $values  Optional Associative Array of values
         * @param array|\Score\DB\ResultSet $useResultSet Optional set the use of a specific resultset
         * @return array|\Score\DB\ResultSet
         */
        public function Select($query, array $values = array(), $useResultSet = null)
        {
            return $this->Adapter()->Select($query, $values, $useResultSet);
        }

        /**
         * Select all fields in a table by passing array of critera
         * @param string $tableName
         * @param array $critera
         * @param mixed $useResultSet
         * @return array|\Score\DB\ResultSet \Score\DB\ResultSet or array
         */
        public function SelectBy($tableName, array $critera = array(), $useResultSet = null)
        {
            return $this->Adapter()->SelectBy($tableName, $critera, $useResultSet);
        }

        /**
         * Delete data from table
         * @param string $tableName
         * @param array $critValues  Associative Array of values of criteria
         * @param mixed $useResultSet Optional set the use of a specific resultset
         * @return array|\Score\DB\ResultSet \Score\DB\ResultSet or false === failed to delete.
         * @example $db->Delete("products", array("id" => 13));
         */
        public function Delete($tableName, array $critValues, $useResultSet = null)
        {
            return $this->Adapter()->Delete($tableName, $critValues, $useResultSet);
        }

        /**
         * Insert Data into Table
         * @param string $tableName
         * @param array $values Assoc array of fields/values
         * @param mixed $useResultSet Optional set the use of a specific resultset
         * @return array|\Score\DB\ResultSet \Score\DB\ResultSet or false === failed to insert.  Otherwise newly inserted record id
         */
        public function Insert($tableName, array $values, $useResultSet = null)
        {
            return $this->Adapter()->Insert($tableName, $values, $useResultSet);
        }

        /**
         * Update Table
         * @param string $tableName
         * @param string $where
         * @param array $iValues
         * @param mixed $useResultSet Optional set the use of a specific resultset
         * @return array|\Score\DB\ResultSet \Score\DB\ResultSet or true/false for if it updated
         */
        public function Update($tableName, $where, array $iValues, $useResultSet = null)
        {
            return $this->Adapter()->Update($tableName, $where, $iValues, $useResultSet);
        }

        /**
         * Check for the existence of a record
         * @param string $query
         * @param array $values  Optional Associative Array of name and values to replace in SQL.
         * @param mixed $useResultSet Optional set the use of a specific resultset
         * @return array|\Score\DB\ResultSet
         */
        public function GetFirstRow($query, array $values = array(), $useResultSet = null)
        {
            return $this->Adapter()->GetFirstRow($query, $values, $useResultSet);
        }

        /**
         * Check for the existence of a record
         * @param string $tableName
         * @param array $critera  Optional Associative Array of name and values to replace in SQL.
         * @param mixed $useResultSet Optional set the use of a specific resultset
         * @return array|\Score\DB\ResultSet
         */
        public function GetFirstRowBy($tableName, array $critera = array(), $useResultSet = null)
        {
            return $this->Adapter()->GetFirstRowBy($tableName, $critera, $useResultSet);
        }

        /**
         * Fetch the first column
         * @param string $query
         * @return string
         */
        public function GetFirstColumn($query, array $values = array())
        {
            return $this->Adapter()->GetFirstColumn($query, $values);
        }

        /**
         * Set Last Error
         * @param mixed $value
         */
        public function LastErrorSet($value = null)
        {
            return $this->Adapter()->LastErrorSet($value);
        }

        /**
         * Returns last error Message
         * @return string
         */
        public function LastErrorMessage()
        {
            return $this->Adapter()->LastErrorMessage();
        }

        /*
         * Returns if the database connection is still valid
         * @return boolean
         */
        public function isConnected()
        {
            return $this->Adapter()->isConnected();
        }
    }
}
