<?php

namespace Score\DB
{
    use Score\DB\ResultSet;
    use Score\DB\ResultSetHelper;
    use Score\Strings\Format;

    class MySQLHelper
    {
        public $DBConnection;

        private $SelfConnected;

        function __construct($host = null, $databaseName = null, $username = null, $password = null)
        {
            if ( func_num_args() == 0 )
            {
                return;
            }

            $this->DBConnection = mysql_connect($host, $username, $password);
            $this->SelfConnected = true;

            if ( !mysql_select_db($databaseName, $this->DBConnection) )
            {
                mysql_close($this->DBConnection);

                $this->DBConnection = null;
            }
        }

        function __destruct()
        {
            if ( $this->SelfConnected )
            {
                mysql_close($this->DBConnection);
            }
        }

        public function Execute($query)
        {
            if ( $this->DBConnection == null )
            {
                return ResultSetHelper::MakeError("Database connection has not been established");
            }

            $retRes = new ResultSet();

            try
            {
                $mehQuery = mysql_query($query);
            }
            catch ( Exception $xErr )
            {
                $retRes->Status = ResultSet::STAT_ERROR;
                $retRes->Message = "Exception: " . $xErr->getMessage();
            }

            if ( $mehQuery === false )
            {
                $retRes->Status = ResultSet::STAT_ERROR;
                $retRes->Message = sprintf("%s (%s)", mysql_error(), mysql_errno());
            }
            else
            {
                $retRes->Status = ResultSet::STAT_GOOD;
            }

            return $retRes;
        }

        public function QuickSelect($tableName)
        {
            return $this->Select(sprintf("SELECT * FROM %s", $tableName));
        }

        public function Select($iQuery)
        {
            if ( $this->DBConnection == null )
            {
                return ResultSetHelper::MakeError("Database connection has not been established");
            }

            $retRes = new ResultSet();

            try
            {
                $mehQuery = mysql_query($iQuery);

                if ( $mehQuery === false )
                {
                    $retRes->Status = ResultSet::STAT_ERROR;
                    $retRes->Message = sprintf("%s (%s)", mysql_error(), mysql_errno());

                    return $retRes;
                }
            }
            catch ( Exception $xErr )
            {
                $retRes->Status = ResultSet::STAT_ERROR;
                $retRes->Message = "Exception: " . $xErr->getMessage();

                return $retRes;
            }

            try
            {
                $dataArray = array();

                while ( $xRow = mysql_fetch_array($mehQuery, MYSQL_ASSOC) )
                {
                    $dataArray[] = $xRow;
                }

                $retRes->Data = $dataArray;
                $retRes->Status = ResultSet::STAT_GOOD;
            }
            catch ( Exception $xErr )
            {
                $retRes->Status = ResultSet::STAT_ERROR;
                $retRes->Message = "Exception: " . $xErr->getMessage();
            }

            return $retRes;
        }

        public function FindFirst($iQuery)
        {
            if ( $this->DBConnection == null )
            {
                return null;
            }

            $firstRow = self::GetFirstRow($iQuery);

            return $firstRow->Data;
        }

        public function GetFirstRow($iQuery)
        {
            if ( $this->DBConnection == null )
            {
                return null;
            }

            $retRes = new ResultSet();
            $mehQuery = mysql_query($iQuery . " LIMIT 1");

            if ( !is_resource($mehQuery) )
            {
                $retRes->Status = ResultSet::STAT_ERROR;
                $retRes->Message = sprintf("%s (%s)", mysql_error(), mysql_errno());

                return $retRes;
            }

            $xData = mysql_fetch_assoc($mehQuery);

            if ( empty($xData) )
            {
                $retRes->Status = ResultSet::STAT_NODATA;
            }
            else
            {
                $retRes->Data = $xData;
                $retRes->Status = ResultSet::STAT_GOOD;
            }

            return $retRes;
        }

        public function GetFirstColumn($iQuery)
        {
            if ( $this->DBConnection == null )
            {
                return null;
            }

            $mehQuery = mysql_query($iQuery . " LIMIT 1");
            if ( $mehQuery === false )
            {
                return null;
            }

            $dataArray = mysql_fetch_row($mehQuery);

            return $dataArray[0];
        }

        public function CanFind($iQuery)
        {
            if ( $this->DBConnection == null )
            {
                return false;
            }

            $mehQuery = mysql_query(sprintf("SELECT EXISTS(%s) AS Dude", $iQuery));
            if ( $mehQuery === false )
            {
                return false;
            }

            $dataArray = mysql_fetch_row($mehQuery);

            return ($dataArray[0] == 1);
        }

        public function Insert($tableName, array $dataArray)
        {
            if ( $this->DBConnection == null )
            {
                return ResultSetHelper::MakeError("Database connection has not been established");
            }

            $cols = array_keys($dataArray);
            $vals = array();

            foreach ( $cols as $name )
            {
                $vals[] = sprintf("'%s'", mysql_real_escape_string($dataArray[$name]));
            }

            $sql = sprintf("INSERT INTO `%s` (%s) VALUES (%s)", $tableName, implode(",", $cols), implode(",", $vals));

            $retRes = $this->Execute($sql);

            if ( $retRes->Status == ResultSet::STAT_GOOD )
            {
                $retRes->Data = $this->NewID();
            }

            return $retRes;
        }

        public function Update($tableName, $condition, array $dataArray)
        {
            if ( $this->DBConnection == null )
            {
                return ResultSetHelper::MakeError("Database connection has not been established");
            }

            $vals = array();

            foreach ( $dataArray as $colName => $colVal )
            {
                if ( $colVal === null )
                {
                    $vals[] = sprintf("`%s` = NULL", $colName);
                }
                else
                {
                    if ( $colVal === false || $colVal === true )
                    {
                        $vals[] = sprintf("`%s` = %s", $colName, $colVal ? 1 : 0);
                    }
                    else
                    {
                        $vals[] = sprintf("`%s` = '%s'", $colName, mysql_real_escape_string($colVal));
                    }
                }
            }

            $sql = sprintf("UPDATE `%s` SET %s", $tableName, implode(",", $vals));

            if ( !empty($condition) )
            {
                $sql .= " WHERE " . $condition;
            }

            return $this->Execute($sql);
        }

        public function NewID()
        {
            if ( $this->DBConnection == null )
            {
                return ResultSetHelper::MakeError("Database connection has not been established");
            }

            return self::GetFirstColumn("SELECT LAST_INSERT_ID()");
        }

        public function CleanArray($tableName, &$dataArray)
        {
            if ( $this->DBConnection == null )
            {
                return ResultSetHelper::MakeError("Database connection has not been established");
            }

            $retRes = $this->Select(Format::Write("SHOW COLUMNS FROM `{0}`", $tableName));
            if ( $retRes->Status != ResultSet::STAT_GOOD )
            {
                return $retRes;
            }

            $colList = array();
            $fieldList = array_keys($dataArray);

            foreach ( $retRes->Data as $dbCol )
            {
                $colList[] = strtolower($dbCol["Field"]);
            }

            foreach ( $fieldList as $field )
            {
                if ( !in_array(strtolower($field), $colList) )
                {
                    unset($dataArray[$field]);
                }
            }

            ResultSetHelper::MakeGood();
        }
    }
}