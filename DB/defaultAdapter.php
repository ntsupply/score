<?php

namespace Score\DB
{
    use \Score\DB\MetaTable;
    use \Score\DB\MetaColumn;
    use \Score\Container\Cookie;
    use \Score\DB\ResultSet;
    use \Score\Strings\Format;
    use \Score\Loader;
    use \Score\DB\Connection;
    use \Score\Profiler\Snap;

    /**
     * Default Adapter for database stuff.  Note that this will atempt to use INFORMATION_SCHEMA for extra detail.
     * @author Bradley Worrell-Smith
     * @link http://en.wikipedia.org/wiki/Information_schema
     */
    class defaultAdapter
    {

        protected $_pdo;
        protected $_dsn;

        /**
         * Last Exception message reported from the PDOStatement
         * @var string
         */
        protected $_lastError;
        public $objectQualifierLeft = "'"; // Standard SQL
        public $objectQualifierRight = "'"; // Standard SQL
        public $blockAttr = array();

        /**
         * Constructor
         * @param \PDO $iPDO Backreference to PDO Connection
         */
        public function __construct(\PDO &$PDO, $DSN)
        {
            // Setup for DI to prevent circular references
            $cookie = new Cookie($PDO, Cookie::NAME_GUID, Cookie::INSTANCE_MULTI);
            \Score\Container::globalSet($cookie);
            $this->_pdo = $cookie->name;

            $this->_dsn = $DSN;
        }

        /**
         * Get the PDO object from the Container
         * @return \PDO
         */
        public function &PDO()
        {
            return \Score\Container::globalGet($this->_pdo);
        }

        /**
         * Add qualifiers to table/field objects to keep keywords out of SQL
         * @param string $name Name of the Table or Field object
         * @param string $prefixObject Name of Table or Schema Object to prefix before a field
         * @return string|array
         */
        public function QualifyObject($name, $prefixObject = null)
        {
            if (is_array($name))
            {
                foreach ($name as &$item)
                {
                    $item = $this->objectQualifierLeft . $item . $this->objectQualifierRight;

                    if (!empty($prefixObject))
                    {
                        $item = $this->objectQualifierLeft . $prefixObject . $this->objectQualifierRight . '.' . $item;
                    }
                }

                return $name;
            }

            $name = $this->objectQualifierLeft . $name . $this->objectQualifierRight;

            if (!empty($prefixObject))
            {
                $name = $this->objectQualifierLeft . $prefixObject . $this->objectQualifierRight . '.' . $name;
            }

            return $name;
        }

        /**
         * Truncate table
         * @param string $tableName
         */
        public function TableTruncate($tableName)
        {
            $this->PDO()->exec("TRUNCATE TABLE " . $this->QualifyObject($tableName));
        }

        /**
         * Get a list of tables
         * @return bool|array
         */
        public function getTableList()
        {
            $tableList = array();

            try
            {
                $prep = $this->PDO()->prepare("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ?");
                $prep->execute(array($this->_schemaName));
                $tables = $prep->fetchAll();

                if ($tables)
                {
                    foreach ($tables as $table)
                    {
                        $tableList[] = $table["table_name"];
                    }
                }
            }
            catch (\PDOException $err)
            {
                return false;
            }

            return $tableList;
        }

        /**
         * Get the Meta Table with Columns from a specific table
         * @param string $tableName Table to get meta information from
         * @param string $schema    Set or auto detect Schema
         * @param string $catalog   Set or auto detect Cataglog
         * @return MetaTable
         */
        public function getMetaTable($tableName, $schema = null, $catalog = null)
        {
            $tableInfo = new MetaTable();
            $tableInfo->schema = $schema;
            $tableInfo->catalog = $catalog;
            $tableInfo->tableName = $tableName;

            if (empty($tableInfo->schema) && preg_match('/(dbname|database)=(.*?)(;|\z)/imx', $this->_dsn, $regs))
            {
                $tableInfo->schema = $regs[2];
            }

            $this->PopulateMetaColumns($tableInfo);

            return $tableInfo;
        }

        /**
         * Used to populate table info with columns
         * @param MetaTable $tableInfo MetaTable Object to populate
         */
        public function PopulateMetaColumns(MetaTable &$tableInfo)
        {
            $tableInfo->delColumn();

            $prep = $this->PDO()->prepare("SELECT *
                                            FROM INFORMATION_SCHEMA.COLUMNS
                                           WHERE (:cat IS NULL OR TABLE_CATALOG = :cat) AND
                                                 TABLE_SCHEMA = :sch AND
                                                 TABLE_NAME = :tbl", array(\PDO::ATTR_EMULATE_PREPARES => 1));
            $prep->execute(array(":cat" => $tableInfo->catalog, ":sch" => $tableInfo->schema, ":tbl" => $tableInfo->tableName));
            $columns = $prep->fetchAll();

            if ($columns)
            {
                foreach ($columns as $num => $field)
                {
                    $hdCol = new MetaColumn();
                    $hdCol->extra = isset($field["EXTRA"]) ? $field["EXTRA"] : "";
                    $hdCol->dataType = $field["DATA_TYPE"];
                    $hdCol->keyType = isset($field["COLUMN_KEY"]) ? $field["COLUMN_KEY"] : "";
                    $hdCol->name = $field["COLUMN_NAME"];
                    $hdCol->order = $num;
                    $hdCol->defSet = true;
                    $hdCol->defValue = $field["COLUMN_DEFAULT"];
                    $hdCol->isNullable = ($field["IS_NULLABLE"] === "YES" ? true : false);

                    if (is_null($field["NUMERIC_SCALE"]))
                    {
                        $hdCol->length = $field["CHARACTER_MAXIMUM_LENGTH"];
                    }
                    else
                    {
                        $hdCol->isNumeric = true;
                        $hdCol->precision = $field["NUMERIC_PRECISION"];
                        $hdCol->length = $field["NUMERIC_SCALE"];
                    }

                    $tableInfo->addColumn($hdCol);
                }
            }
        }

        /**
         * Prepare a ResultSet object for returning
         * @param mixed $fallback  Fallback Resultset name.
         * @return \Score\DB\ResultSet
         * @throws \Score\Exception
         */
        public function &PrepResultSet(&$fallback = null)
        {
            $new = null;
            $simple = true;
            $name = Connection::RESULTSET_DEFAULT;
            $handler = $this->PDO()->getAttribute(Connection::ATTR_RESULTSET_HANDLER);

            if (!empty($handler))
            {
                $name = $handler;
                $simple = false;
            }

            if ($fallback === false)
            {
                $simple = true;
            }

            if (is_string($fallback) && $fallback != $name)
            {
                if (!Loader::Load($fallback, true))
                {
                    throw new \Score\Exception(Connection::EXCEPTION_BADRESULTNAME);
                }

                $name = $fallback;
                $simple = false;
            }

            if (is_object($fallback))
            {
                $new = & $fallback;
                $new->UseSimpleResult = false;
            }
            else
            {
                $new = new $name();
                $new->UseSimpleResult = $simple;
            }

            if (!$new instanceof ResultSet)
            {
                throw new \Score\Exception(Connection::EXCEPTION_BADRESULTOBJ);
            }

            return $new;
        }

        /**
         * Prep Values from Associative array for PDO Query/Execute string
         * @param array $assocArray Associative Array of names with values
         * @param int $prepType Where or assignment
         * @param string $table Table to qualify on field names
         * @retrun array Array of strings that with operators. Ex: array("`foo` = :foo", "`bar` = :bar")
         */
        public function PrepValues(array &$assocArray, $prepType = Connection::PREPTYPE_ASSIGN, $table = null)
        {
            $return = array();

            if ($assocArray)
            {
                $cols = array_keys($assocArray);
                $new = array();

                foreach ($assocArray as $name => $value)
                {
                    $name = trim($name, ":");
                    $remove = false;

                    switch ($prepType)
                    {
                        case Connection::PREPTYPE_WHERE:
                            if (is_null($value) || (is_array($value) && empty($value)))
                            {
                                $return[] = $this->QualifyObject($name, $table) . ' IS NULL';
                                $remove = true;
                                break;
                            }
                            elseif (is_array($value))
                            {
                                $inc = [];

                                foreach ($value as $k => $v)
                                {
                                    $key = ":{$name}__{$k}";
                                    $inc[] = $key;
                                    $new[$key] = $v;
                                }

                                $return[] = $this->QualifyObject($name, $table) . ' IN (' . implode(",", $inc) . ')';
                                $remove = true;

                                break;
                            }

                        // Fall through to assignment because it is the same statement as a where
                        case Connection::PREPTYPE_ASSIGN:
                            $return[] = $this->QualifyObject($name, $table) . ' = :' . $name;
                            break;
                    }

                    if (!$remove)
                    {
                        $new[':' . $name] = $value;
                    }
                }

                $assocArray = $new;
            }

            return $return;
        }

        /**
         * Check for the existence of a record
         * @param string $tableName
         * @param array $values  Optional Associative Array of name and values to replace in SQL.
         * @return boolean
         * @example CanFind("MyTable", array("id" => 123))
         */
        public function CanFind($tableName, array $values = array())
        {
            $this->LastErrorSet(null);

            if (empty($values))
            {
                $select = Format::Write("SELECT EXISTS({0}) AS Dude", $tableName);
            }
            else
            {
                $crit = $this->PrepValues($values, Connection::PREPTYPE_WHERE);
                $crit = implode(" AND ", $crit);

                $select = Format::Write("SELECT EXISTS(SELECT * FROM `{0}` WHERE {1}) AS Dude", $tableName, $crit);
            }

            $prep = $this->PDO()->prepare($select);

            if ($prep->execute($values))
            {
                return $prep->fetchColumn() == 1;
            }

            return false;
        }

        /**
         * Base on query given vs just a table like CanFind does
         * @param string $query
         * @param array $values  Optional Associative Array of name and values to replace in SQL.
         * @return boolean
         * @example CanFindQuery("SELECT * FROM MyTable WHERE id = :id", array("id" => 123))
         */
        public function CanFindQuery($query, array $values = array())
        {
            $snap = Snap::q(__METHOD__);
            $this->LastErrorSet(null);
            $this->PrepValues($values);

            $select = Format::Write("SELECT EXISTS({0}) AS Dude", $query);
            $snap->annotate($query, $values);
            $prep = $this->PDO()->prepare($select);

            if ($prep->execute($values))
            {
                return $prep->fetchColumn() == 1;
            }

            return false;
        }

        /**
         * Query with select statment
         * @param string $query
         * @param array $values  Optional Associative Array of values
         * @param mixed $useResultSet Optional set the use of a specific resultset
         * @return array
         */
        public function Select($query, array $values = array(), $useResultSet = null)
        {
            $snap = Snap::q(__METHOD__);
            $this->LastErrorSet(null);
            $this->PrepValues($values);

            $res = $this->PrepResultSet($useResultSet);
            $res->Prepare($this->PDO()->prepare($query));
            $snap->annotate($query, $values);

            if ($res->Prepare()->execute($values))
            {
                $snap->annotate("Executed");
                $res->Data = $res->Prepare()->fetchAll(\PDO::FETCH_ASSOC);
                $snap->annotate("Fetched");

                if (empty($res->Data))
                {
                    $res->Status = ResultSet::STAT_NODATA;
                }
                else
                {
                    $res->Status = ResultSet::STAT_GOOD;
                }
            }
            else
            {
                $snap->annotate($res->Prepare()->lastError);
                $this->LastErrorSet($res->Prepare()->lastError);

                $res->Data = array();
                $res->Status = ResultSet::STAT_ERROR;
                $res->Message = $this->LastErrorMessage();
            }


            if ($res->UseSimpleResult === true)
            {
                return $res->Data;
            }

            return $res;
        }

        /**
         * Select all fields in a table by passing array of critera
         * @param string|array $tableName
         * @param array $critera
         * @param mixed $useResultSet
         * @return mixed ResultSet or array
         */
        public function SelectBy($tableName, array $critera = array(), $useResultSet = null)
        {
            $sortBy = "";

            if (!is_array($tableName))
            {
                $tableName = [$tableName];
            }

            foreach ($tableName as $i => $sb)
            {
                if (empty($i))
                {
                    $tn = $sb;
                }
                else
                {
                    $sortBy .= ($i === 1 ? " ORDER BY " : ",") . $this->QualifyObject(trim($sb,"!")) . (\Score\Strings::Begins($sb, "!") ? " DESC" : '');
                }
            }

            $snap = Snap::q(__METHOD__);
            $this->LastErrorSet(null);
            $crit = $this->PrepValues($critera, Connection::PREPTYPE_WHERE);

            if (!empty($critera))
            {
                $crit = implode(" AND ", $crit);
            }
            else
            {
                $crit = "1 = 1";
                $critera = array();
            }

            $query = "SELECT * FROM " . $this->QualifyObject($tn) . " WHERE {$crit} {$sortBy}";

            $res = $this->PrepResultSet($useResultSet);
            $res->Prepare($this->PDO()->prepare($query));
            $snap->annotate($query, $critera);

            if ($res->Prepare()->execute($critera))
            {
                $snap->annotate("Executed");
                $res->Data = $res->Prepare()->fetchAll(\PDO::FETCH_ASSOC);
                $snap->annotate("Fetched");

                if (empty($res->Data))
                {
                    $res->Status = ResultSet::STAT_NODATA;
                }
                else
                {
                    $res->Status = ResultSet::STAT_GOOD;
                }
            }
            else
            {
                $snap->annotate("Error: ".$res->Prepare()->lastError);
                $this->LastErrorSet($res->Prepare()->lastError);

                $res->Data = array();
                $res->Status = ResultSet::STAT_ERROR;
                $res->Message = $this->LastErrorMessage();
            }

            if ($res->UseSimpleResult === true)
            {
                return $res->Data;
            }

            return $res;
        }

        /**
         * Delete data from table
         * @param string $tableName
         * @param array $critValues  Associative Array of values of criteria
         * @param mixed $useResultSet Optional set the use of a specific resultset
         * @example $db->Delete("products", array("id" => 13));
         */
        public function Delete($tableName, array $critValues, $useResultSet = null)
        {
            $snap = Snap::q(__METHOD__);
            $this->LastErrorSet(null);

            $crit = $this->PrepValues($critValues, Connection::PREPTYPE_WHERE);
            $crit = implode(" AND ", $crit);
            $query = "DELETE FROM " . $this->QualifyObject($tableName) . " WHERE $crit";

            $res = $this->PrepResultSet($useResultSet);
            $res->Prepare($this->PDO()->prepare($query));
            $snap->annotate($query, $critValues);

            if ($res->Prepare()->execute($critValues))
            {
                $snap->annotate("Executed");
                $res->Data = $res->Prepare()->rowCount();
                $res->Status = ResultSet::STAT_GOOD;
                $res->Prepare()->returnValue = $res->Data;
            }
            else
            {
                $snap->annotate("Error: " . $res->Prepare()->lastError);
                $this->LastErrorSet($res->Prepare()->lastError);

                $res->Status = ResultSet::STAT_ERROR;
                $res->Message = $this->LastErrorMessage();
            }

            if ($res->UseSimpleResult === true)
            {
                return $res->Prepare()->returnValue;
            }

            return $res;
        }

        /**
         * Insert Data into Table
         * @param string $tableName
         * @param array $values Assoc array of fields/values
         * @param mixed $useResultSet Optional set the use of a specific resultset
         * @return mixed ResultSet or false === failed to insert.  Otherwise newly inserted record id
         */
        public function Insert($tableName, array $values, $useResultSet = null)
        {
            $snap = Snap::q(__METHOD__);
            $fields = $this->QualifyObject(array_keys($values));

            $this->LastErrorSet(null);
            $this->PrepValues($values);

            $sql = sprintf("INSERT INTO %s (%s) VALUES (%s)", $this->QualifyObject($tableName), implode(",", $fields), implode(",", array_keys($values)));
            $res = $this->PrepResultSet($useResultSet);
            $res->Prepare($this->PDO()->prepare($sql));

            $snap->annotate($sql, $values);

            if ($res->Prepare()->execute($values))
            {
                $snap->annotate("Executed");
                $res->Data = $this->PDO()->lastInsertId($tableName);
                $res->Status = ResultSet::STAT_GOOD;
                $res->Prepare()->returnValue = $res->Data;
            }
            else
            {
                $snap->annotate("Error: " . $res->Prepare()->lastError);
                $this->LastErrorSet($res->Prepare()->lastError);

                $res->Status = ResultSet::STAT_ERROR;
                $res->Message = $this->LastErrorMessage();
                $res->Prepare()->returnValue = false;
            }

            if ($res->UseSimpleResult === true)
            {
                return $res->Prepare()->returnValue;
            }

            return $res;
        }

        /**
         * Update Table
         * @param string $tableName
         * @param string $where
         * @param array $values
         * @param mixed $useResultSet Optional set the use of a specific resultset
         * @return mixed ResultSet or true/false for if it updated
         */
        public function Update($tableName, $where, array $values, $useResultSet = null)
        {
            $snap = Snap::q(__METHOD__);

            $this->LastErrorSet(null);
            $set = $this->PrepValues($values);
            $sql = "UPDATE " . $this->QualifyObject($tableName) . " SET " . implode(", ", $set) . " WHERE $where";

            $res = $this->PrepResultSet($useResultSet);
            $res->Prepare($this->PDO()->prepare($sql));

            $snap->annotate($sql, $values);

            if ($res->Prepare()->execute($values))
            {
                $snap->annotate("Executed");
                $res->Data = $res->Prepare()->rowCount();
                $res->Status = ResultSet::STAT_GOOD;
            }
            else
            {
                $snap->annotate("Error: " . $res->Prepare()->lastError);

                $this->LastErrorSet($res->Prepare()->lastError);

                $res->Status = ResultSet::STAT_ERROR;
                $res->Message = $this->LastErrorMessage();
            }

            if ($res->UseSimpleResult === true)
            {
                return $res->Prepare()->returnValue;
            }

            return $res;
        }

        /**
         * Check for the existence of a record
         * @param string $query
         * @param array $values  Optional Associative Array of name and values to replace in SQL.
         * @param mixed $useResultSet Optional set the use of a specific resultset
         * @return mixed
         */
        public function GetFirstRow($query, array $values = array(), $useResultSet = null)
        {
            $snap = Snap::q(__METHOD__);
            $query = $query . " LIMIT 1";

            $this->LastErrorSet(null);
            $this->PrepValues($values);

            $res = $this->PrepResultSet($useResultSet);
            $res->Prepare($this->PDO()->prepare($query));

            $snap->annotate($query, $values);

            if ($res->Prepare()->execute($values))
            {
                $snap->annotate("Executed");
                $res->Data = $res->Prepare()->fetch(\PDO::FETCH_ASSOC);

                if (empty($res->Data))
                {
                    $res->Status = ResultSet::STAT_NODATA;
                }
                else
                {
                    $res->Status = ResultSet::STAT_GOOD;
                }
            }
            else
            {
                $snap->annotate("Error: " . $res->Prepare()->lastError);
                $this->LastErrorSet($res->Prepare()->lastError);

                $res->Status = ResultSet::STAT_ERROR;
                $res->Message = $this->LastErrorMessage();
            }

            if ($res->UseSimpleResult === true)
            {
                return $res->Data;
            }

            return $res;
        }


        /**
         * Check for the existence of a record
         * @param string $tableName
         * @param array $critera  Optional Associative Array of name and values to replace in SQL.
         * @param mixed $useResultSet Optional set the use of a specific resultset
         * @return mixed
         */
        public function GetFirstRowBy($tableName, array $critera = array(), $useResultSet = null)
        {
            $snap = Snap::q(__METHOD__);

            $sortBy = "";

            if (!is_array($tableName))
            {
                $tableName = [$tableName];
            }

            foreach ($tableName as $i => $sb)
            {
                if (empty($i))
                {
                    $tn = $sb;
                }
                else
                {
                    $sortBy .= ($i === 1 ? " ORDER BY " : ",") . $this->QualifyObject(trim($sb,"!")) . (\Score\Strings::Begins($sb, "!") ? " DESC" : '');
                }
            }

            $snap = Snap::q(__METHOD__);
            $this->LastErrorSet(null);
            $crit = $this->PrepValues($critera, Connection::PREPTYPE_WHERE);

            if (!empty($critera))
            {
                $crit = implode(" AND ", $crit);
            }
            else
            {
                $crit = "1 = 1";
                $critera = array();
            }

            $query = "SELECT * FROM " . $this->QualifyObject($tn) . " WHERE {$crit} {$sortBy} LIMIT 1";

            $res = $this->PrepResultSet($useResultSet);
            $res->Prepare($this->PDO()->prepare($query));

            $snap->annotate($query, $critera);

            if ($res->Prepare()->execute($critera))
            {
                $snap->annotate("Executed");
                $res->Data = $res->Prepare()->fetch(\PDO::FETCH_ASSOC);

                if (empty($res->Data))
                {
                    $res->Status = ResultSet::STAT_NODATA;
                }
                else
                {
                    $res->Status = ResultSet::STAT_GOOD;
                }
            }
            else
            {
                $snap->annotate("Error: " . $res->Prepare()->lastError);
                $this->LastErrorSet($res->Prepare()->lastError);

                $res->Status = ResultSet::STAT_ERROR;
                $res->Message = $this->LastErrorMessage();
            }

            if ($res->UseSimpleResult === true)
            {
                return $res->Data;
            }

            return $res;
        }
        
        /**
         * Fetch the first column
         * @param string $query
         * @return mixed
         */
        public function GetFirstColumn($query, array $values = array())
        {
            $snap = Snap::q(__METHOD__);

            $query = $query . " LIMIT 1";
            $this->PrepValues($values);

            $set = false;
            $res = $this->PrepResultSet($set);
            $res->Prepare($this->PDO()->prepare($query));

            $snap->annotate($query, $values);

            if ($res->Prepare()->execute($values))
            {
                $snap->annotate("Executed");

                return $res->Prepare()->fetchColumn();
            }
            else
            {
                $snap->annotate("Error: " . $res->Prepare()->lastError);

                $this->LastErrorSet($res->Prepare()->lastError);
                return null;
            }
        }

        /**
         * Set Last Error
         * @param mixed $value
         */
        public function LastErrorSet($value = null)
        {
            $this->_lastError = $value;
        }

        /**
         * Returns last error Message
         * @return string
         */
        public function LastErrorMessage()
        {
            if (!empty($this->_lastError))
            {
                return $this->_lastError;
            }

            $gotsError = $this->PDO()->errorInfo();

            if (empty($gotsError))
            {
                return;
            }

            return $gotsError[2];
        }

        /*
         * Returns if the database connection is still valid
         * @return boolean
         */
        public function isConnected()
        {
            try
            {
                $this->PDO()->query("SELECT 1+1");
                return true;
            }
            catch (\PDOException $err)
            {
                return false;
            }
        }

    }

}
