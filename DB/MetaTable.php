<?php

namespace Score\DB
{
    use \Score\Strings;
    use \Score\DB\MetaColumn;

    /**
     * MetaTable information object
     * @author Bradley Worrell-Smith
     */
    class MetaTable implements \Iterator
    {
        public $numColumns = 0;
        public $schema;
        public $catalog;
        public $tableName;
        public $error;

        protected $_cols = array();
        protected $_pos;

        /**
         * Add MetaColumn to MetaTable
         * @param \Score\DB\MetaColumn $iColumn
         */
        public function addColumn(MetaColumn $iColumn)
        {
            $this->_cols[] = $iColumn;

            $this->numColumns = count($this->_cols);
        }

        public function delColumn($col = null)
        {
            if (is_null($col))
            {
                $this->_cols = array();
            }
            elseif (is_string($col))
            {
                $col = array_search($col, $this->getColumnList());
            }

            if (is_int($col) && array_key_exists($col, $this->_cols))
            {
                array_splice($this->_cols, $col, 1);
            }
        }

        /**
         * Get MetaColumn Information
         * @param mixed $iCol Column name or number
         * @return \Score\DB\MetaColumn
         */
        public function getColumn($iCol)
        {
            if (is_int($iCol) && array_key_exists($iCol, $this->_cols))
            {
                return $this->_cols[$iCol];
            }

            if (is_string($iCol))
            {
                foreach ($this->_cols as $num => $col)
                {
                    if (Strings::Compare($col->name, $iCol) === 0)
                    {
                        return $this->_cols[$num];
                    }
                }
            }

            return false;
        }

        /**
         * Get a simple columns list as an array
         * @return array
         */
        public function getColumnList()
        {
            $list = array();

            foreach ($this->_cols as $num => $col)
            {
                $list[] = $col->name;
            }

            return $list;
        }


        /**
         * Iterator rewind
         */
        public function rewind()
        {
            $this->_pos = 0;
        }

        /**
         * Iterator Current
         * @return \Score\DB\MetaColumn
         */
        public function current()
        {
            return $this->_cols[$this->_pos];
        }

        /**
         * Iterator Position #
         * @return int
         */
        public function key()
        {
            return $this->_pos;
        }

        /**
         * Iterator Next
         */
        public function next()
        {
            ++$this->_pos;
        }

        /**
         * Iterator Valid
         * @return bool
         */
        public function valid()
        {
            return isset($this->_cols[$this->_pos]);
        }
    }
}
