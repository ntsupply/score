<?php

namespace Score\Time
{
    /**
     * Stopwatch utility to time performance
     * @author Bradley Worrell-Smith
     * @todo Optional: possible auto-logging option
     * @todo Optional: new class extends Stopwatch for tracking cpu/mem
     */
    class Stopwatch
    {
        protected $_times = array();
        protected $_htime;
        protected $_enabled;

        /**
         * Constructor
         */
        public function __construct()
        {
            $this->Enable(true);
        }

        /**
         * Enable/Disable the stopwatch
         * @param bool $Enabled
         */
        public function Enable($Enabled = true)
        {
            $this->Stop();
            $this->_enabled = $Enabled;
        }

        /**
         * Start the stopwatch
         * @param string $tag Tag Name
         * @return \Score\Time\Stopwatch
         */
        public function &Start($tag = null)
        {
            if (empty($tag))
            {
                $this->Stop(); // Make sure and stop just in case

                $this->_htime = microtime(true);
            }
            else
            {
                $this->_times[] = array("start" => microtime(true), "stop" => null, "tag" => $tag);
            }

            return $this;
        }

        /**
         * Stop the stopwatch
         * @param string $tag Tag Name
         * @return \Score\Time\Stopwatch
         */
        public function &Stop($tag = null)
        {
            if (!$this->_enabled)
            {
                $this->_htime = null;
                return $this;
            }

            $pos = count($this->_times);
            $stop = microtime(true);
            $data = array();

            if (!empty($tag))
            {
                for ($cnt = $pos - 1; $cnt >= 0; $cnt--)
                {
                    if ($this->_times[$cnt]["tag"] == $tag && is_null($this->_times[$cnt]["stop"]))
                    {
                        $pos = $cnt;
                        $data = $this->_times[$pos];
                        break;
                    }
                }
            }

            if (empty($data)) // So if tagged, but not found open from above
            {
                if (empty($this->_htime))
                {
                    $this->_htime = null;
                    return $this;
                }

                $data["start"] = $this->_htime;
                $data["stop"] = $stop;
                $data["tag"] = $tag;
            }

            $data["latency"] = $stop - $data["start"];
            $data["readable"] = round($data["latency"], 5);

            $this->_times[] = $data;
            $this->_htime = null;
            return $this;
        }

        /**
         * Get the log of times
         * @return array
         */
        public function getLog()
        {
            return $this->_times;
        }

        /**
         * Get the last timeing made in seconds
         * @param int $precision How many decimal points to round to
         * @return float
         */
        public function First($precision = null)
        {
            $list = array_reverse($this->_times);
            return $this->_echo(end($list), $precision);
        }

        /**
         * Get the last timeing made in seconds
         * @param int $precision How many decimal points to round to
         * @return float
         */
        public function Last($precision = null)
        {
            return $this->_echo(end($this->_times), $precision);
        }

        /**
         * Get the best timing
         * @return array
         */
        public function Best()
        {
            $list = $this->_times;
            \Score\Arrays::Sort($list, "latency", \SORT_DESC, true);

            return end($list);
        }

        /**
         * Get the best time in seconds
         * @param int $precision How many decimal points to round to
         * @return float
         */
        public function BestTime($precision = null)
        {
            return $this->_echo($this->Best(), $precision);
        }

        /**
         * Get the worst timeing
         * @return array
         */
        public function Worst()
        {
            $list = $this->_times;
            \Score\Arrays::Sort($list, "latency", \SORT_ASC, true);

            return end($list);
        }

        /**
         * Get the worst timeing in seconds
         * @param int $precision How many decimal points to round to
         * @return float
         */
        public function WorstTime($precision = null)
        {
            return $this->_echo($this->Worst(), $precision);
        }

        /**
         * Simple function to get the latency from the array
         * @param array $entry
         * @param int $precision How many decimal points to round to
         * @return float
         */
        public function _echo($entry, $precision)
        {
            if (is_null($entry))
            {
                return null;
            }

            if (is_null($precision))
            {
                return $entry["latency"];
            }

            return round($entry["latency"], $precision);
        }

        /**
         * Return the last entry
         * @return string
         */
        public function __toString()
        {
            return (string)$this->Last();
        }
    }
}