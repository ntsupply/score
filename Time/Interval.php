<?php

namespace Score\Time
{

    class Interval extends \DateInterval
    {
        const SHOW_FULL = false;
        const SHOW_SHORT = true;

        /**
         * Format as defined by \DateInterval but with aggregate options
         * @param string $format
         * 
         * @todo Need to fix the way it calc the d,m etc
         * 
         * @return string
         */
        public function format($format)
        {
            //years
            $y = $this->y;
            //months
            $am = ($y * 12) + $this->y;
            //days
            $ad = ($y * 365) + ($this->m * 30) + $this->d;
            //hour
            $ah = ($ad * 24) + $this->h;
            //min
            $ai = ($ah * 60) + $this->i;
            //sec
            $as = ($ai * 60) + $this->s;

            $format = str_replace("%am", $am, $format);
            $format = str_replace("%ah", $ah, $format);
            $format = str_replace("%ai", $ai, $format);
            $format = str_replace("%as", $as, $format);
            $format = str_replace("%a", $ad, $format);

            return parent::format($format);
        }

        /**
         * Write out an interval
         * @param mixed $show
         * @param string $separator
         * @return string
         */
        function writeTime($show = 2, $separator = null)
        {
            $format = array();
            if ($show === self::SHOW_SHORT)
            {
                if (is_null($separator))
                {
                    $separator = ",";
                }

                if ($this->y !== 0)
                {
                    $format[] = '%y yr' . (($this->y > 1) ? 's' : '');
                }
                if ($this->m !== 0)
                {
                    $format[] = '%m mo' . (($this->m > 1) ? 's' : '');
                }
                if ($this->d !== 0)
                {
                    $format[] = '%d d';
                }
                if ($this->h !== 0)
                {
                    $format[] = '%h h';
                }
                if ($this->i !== 0)
                {
                    $format[] = '%i m';
                }
                if ($this->s !== 0)
                {
                    if (!count($format))
                    {
                        return '<1m';
                    }
                    else
                    {
                        $format[] = '%s s';
                    }
                }
            }
            else
            {
                if (is_null($separator))
                {
                    $separator = " and";
                }

                if ($this->y !== 0)
                {
                    $format[] = '%y year' . (($this->y > 1) ? 's' : '');
                }
                if ($this->m !== 0)
                {
                    $format[] = '%m month' . (($this->m > 1) ? 's' : '');
                }
                if ($this->d !== 0)
                {
                    $format[] = '%d day' . (($this->d > 1) ? 's' : '');
                }
                if ($this->h !== 0)
                {
                    $format[] = '%h hour' . (($this->h > 1) ? 's' : '');
                }
                if ($this->i !== 0)
                {
                    $format[] = '%i minute' . (($this->i > 1) ? 's' : '');
                }
                if ($this->s !== 0)
                {
                    if (!count($format))
                    {
                        return 'less than a minute ago';
                    }
                    else
                    {
                        $format[] = '%s second' . ($this->s > 1) ? 's' : '';
                    }
                }
            }


            if ($show !== self::SHOW_FULL && \is_int($show))
            {
                $format = array_slice($format, 0, $show);
            }

            $separator = rtrim($separator) . ' ';
            
            return $this->format(implode($separator, $format));
        }

    }

}