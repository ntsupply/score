<?php

namespace Score
{
    use \Score\Helper;
    use \Score\Console\Command;
    use \Score\Strings\Format;
    use \Score\Container\Cookie;

    /**
     * Console and CLI methods
     * @author Brad Smith
     */
    class Console
    {
        /**
         * Associative array of Command line arguments
         * @var array
         */
        protected static $_CArgs = null;

        protected $_stdin;
        protected $_stdout;
        protected $_fgColors = array(
			'black' => '0;30',
			'dark_gray' => '1;30',
			'blue' => '0;34',
			'light_blue' => '1;34',
			'green' => '0;32',
			'light_green' => '1;32',
			'cyan' => '0;36',
			'light_cyan' => '1;36',
			'red' => '0;31',
			'light_red' => '1;31',
			'purple' => '0;35',
			'light_purple' => '1;35',
			'brown' => '0;33',
			'yellow' => '1;33',
			'light_gray' => '0;37',
			'white' => '1;37');
		protected $_bgColors = array(
			'black' => '40',
			'red' => '41',
			'green' => '42',
			'yellow' => '43',
			'blue' => '44',
			'magenta' => '45',
			'cyan' => '46',
			'light_gray' => '47');
        protected $_commands = array();
        protected $_guid;

        const EXCEPTION_NOTCLI = "(100150) Console App bust be run from Console Shell";

        /**
         * Build your own console application
         * @return void
         * @throws \Score\Exception
         */
        public function __construct()
        {
            if (!self::IsConsole())
            {
                throw new \Score\Exception("\?");
            }

            $cookie = new Cookie($this, Cookie::NAME_GUID, Cookie::INSTANCE_MULTI);
            \Score\Container::globalSet($cookie);
            $this->_guid = $cookie->name;

            set_time_limit(0);
            $this->_stdin = fopen('php://stdin', 'r');
            $this->_stdout = fopen('php://stdout', 'r');

            $this->addCommand(new Command($this->_guid, "exit", function () { return Command::EXIT_HALT; }));
            $this->addCommand(new \Score\Console\HelpCommand($this->_guid));
        }

        /**
         * Lets cleanup some things
         */
        public function __destruct()
        {
            \Score\Container::globalDelete($this->_guid);
        }

		/**
         * Returns ANSI colored string
         * @param type $string
         * @param type $fgColor
         * @param type $bgColor
         * @return string
         */
		public function Colorize($string, $fgColor = null, $bgColor = null)
        {
            if (\Score\Server::IsWindows())
            {
                return $string;
            }

			$colorized = "";
            $fgColor = strtolower($fgColor);
            $bgColor = strtolower($bgColor);

			// Check if given foreground color found
			if (isset($this->_fgColors[$fgColor]))
            {
				$colorized .= "\033[" . $this->_fgColors[$fgColor] . "m";
			}

			// Check if given background color found
			if (isset($this->_bgColors[$bgColor]))
            {
				$colorized .= "\033[" . $this->_bgColors[$bgColor] . "m";
			}

			// Add string and end coloring
			$colorized .=  $string . "\033[0m";

			return $colorized;
		}

		/**
         * Returns all foreground color names
         * @return array
         */
		public function getFGColors()
        {
			return array_keys($this->_fgColors);
		}

        /**
         * Similar to Format::Write, but echo's and append line feed to end.
         * @param string $string   String with formatting
         * @param string $replace1 Replacement value for {0}.
         * @return Formatted string value
         */
        public static function WriteLine($string, $replace1 = null)
        {
            echo(Format::Write("$string\n", $replace1));
        }

		/**
         * Returns all background color names
         * @return array
         */
		public function getBGColors()
        {
			return array_keys($this->_bgColors);
		}

        /**
         * Add the command to the Shell
         * @param \Score\Console\Command $cmd
         */
        public function addCommand(Command $cmd)
        {
            $this->_commands[$cmd->name] = $cmd;
        }

        /**
         * Array of command objects
         * @return array
         */
        public function Commands()
        {
            return $this->_commands;
        }

        /**
         * Main Shell entry to prompt for commands
         */
        public function Prompt()
        {
            $lastResult = 0;

            while ($lastResult != Command::EXIT_HALT)
            {
                $cmd = null;
                $wack = null;

                echo($this->Colorize("# ", "light_blue"));
                $line = fgets($this->_stdin, 2048);
                $line = trim($line);

                preg_match_all('/\'(?:\\\\.|[^\\\\\'])*\'|"(?:\\\\.|[^\\\\"])*"|\S+/simx', $line, $wack);

                if (count($wack) == 0)
                {
                    continue;
                }

                // Should be first entry
                $args = $wack[0];
                $cmd = array_shift($args);

                if (!array_key_exists($cmd, $this->_commands))
                {
                    echo("-> Unknown Command!\n");
                    continue;
                }

                $lastResult = $this->_commands[$cmd]->Execute($args);
            }
        }

        /**
         * Is this session a Console/CLI session
         * @return bool
         */
        public static function IsConsole()
        {
            $sapi_type = php_sapi_name();
            return (substr($sapi_type, 0, 3) == 'cli' ? true : false);
        }

        /**
         * Parse Command Line Arguments
         * @return void
         */
        protected static function _ParseCArgs()
        {
            if (!is_null(self::$_CArgs))
            {
                return;
            }

            self::$_CArgs = array();

            if (!self::IsConsole())
            {
                return;
            }

            foreach ($_SERVER["argv"] as $xKey => $xValue)
            {
                if ($xKey == 0 || $xValue == "--")
                {
                    continue;
                }

                $xData = null;

                if (preg_match('/(?P<dash>--|-)(?P<name>.*?)[=:](?P<quote>[\'"])(?P<value>.*?)(?P=quote)/i', $xValue, $xData))
                {
                    Helper::StackValue(self::$_CArgs, $xData['value'], $xData['name']);
                }
                elseif (preg_match('/(?P<dash>--|-)(?P<name>.*?)[=:](?P<value>.*?)\z/i', $xValue, $xData))
                {
                    Helper::StackValue(self::$_CArgs, $xData['value'], $xData['name']);
                }
                elseif (preg_match('/(?P<dash>--|-)(?P<name>\w*)/i', $xValue, $xData))
                {
                    Helper::StackValue(self::$_CArgs, true, $xData['name']);
                }
            }
        }

        /**
         * Get a specific item from a command line or array list
         * @param string $Who Name of the Command line arguement
         * @return mixed
         */
        public static function GetCArgs($Who = null, $default = null)
        {
            self::_ParseCArgs();

            if (is_null($Who))
            {
                return self::$_CArgs;
            }
            elseif (array_key_exists($Who, self::$_CArgs))
            {
                return self::$_CArgs[$Who];
            }

            return $default;
        }

    }
}
