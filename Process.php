<?php

namespace Score
{
    use \Score\Server;
    use \Score\Time;

    /**
     * Process class used to execute, control, and query the thread
     * @author Bradley Worrell-Smith
     */
    class Process
    {
        const EXCEPTION_NOCMD = "(100801) Command/Executable not specified";
        const EXCEPTION_RUNFAIL = "(100802) Unable to run specified command";
        const EXCEPTION_BADCMD = "(100803) Bad or misformatted command";
        const EXCEPTION_RUNNOT = "(100804) Process not running.  Use Run()";
        const EXCEPTION_NOTPID = "(100805) Does not appear to be a valid PID #";

        const SIGTERM = 15;
        const SIGKILL = 9;

        protected $_resource;
        protected $_pid;
        protected $_name;
        protected $_command;
        protected $_running;
        protected $_params = array();
        protected $_sudoWrap = false;
        protected $_sudoUser = null;
        protected $_sudoPfile = null;
        protected $_winWrap = false;
        protected $_pipeList;
        protected $_pipes;
        protected $_waitDelay = 10000;
        protected $_startTime;
        protected $_startDir;
        protected $_bufferTO = 10000;
        protected $_bufferSize = false;
        protected $_bufferData;
        protected $_bufferErr;
        protected $_exitCode;
        protected $_windows;

        /**
         * Set the command with optional parameters and process name
         * @param string       $iCommand Command to execute
         * @param string|array $iParams  Optional arguments to pass
         * @param string       $iName    Name of process to tag in Taskmanager. Only works if using setWinWrap()
         * @return \Score\Process
         * @throws \Score\Exception
         */
        public function &setCommand($iCommand, $iParams = array(), $iName = "MyProcess")
        {
            if (!is_string($iCommand))
                throw new \Score\Exception(self::EXCEPTION_BADCMD);

            $this->_name = $iName;
            $this->_command = $iCommand;
            $this->addParam($iParams);

            if (empty($this->_pipeList))
                $this->_pipeList = array(0 => array("pipe", "r+"), // stdin is a pipe that the child will read from
                                         1 => array("pipe", "a"), // stdout is a pipe that the child will write to
                                         2 => array("pipe", "r"));
            return $this;
        }

        /**
         * Set buffer size.  This does not determin the size that will return from getBuffer() but rather how
         * much it reads from STDIN at a time.
         * @param int|bool $iBytes Buffer size to read from process pipe STDIN
         * @return \Score\Process This object used for chaining
         */
        public function &setBufferSize($iBytes = 2048)
        {
            if (!$iBytes)
                $this->_bufferSize = false;
            else
                $this->_bufferSize = (is_numeric ($iBytes) ? $iBytes : 2048);

            return $this;
        }

        /**
         * Set the Buffer timeout in Microseconds. 1000000 = 1 second, 10000 = 1 millisecond
         * @param int|float|long $iMircoSeconds
         * @return \Process This object used for chaining
         */
        public function &setBufferTimeout($iMicroSeconds = 10000)
        {
            $this->_bufferTO = $iMicroSeconds;
            return $this;
        }

        /**
         * Set if to wrap this process behind Sudo.  This might break pipe information.
         * @param bool $iUseSudo Use Sudo to run as another user?
         * @param string $iSudoUser Optional
         * @param string $iSudoPasswordFile Optional password file to direct to sudo
         * @return \Score\Process This object used for chaining
         */
        public function &setSudoWrap($iUseSudo, $iSudoUser = null, $iSudoPasswordFile = null)
        {
            $this->_sudoWrap = $iUseSudo;
            $this->_sudoUser = $iSudoUser;
            $this->_sudoPfile = $iSudoPasswordFile;
            return $this;
        }

        /**
         * Set if to wrap the process behind Window's start command for its own background session.
         * @param bool $iUseStart Use start or not
         * @return \Score\Process This object used for chaining
         */
        public function &setWinWrap($iUseStart = true)
        {
            $this->_winWrap = $iUseStart;
            return $this;
        }

        /**
         * Set the Wait Delay option in Microseconds. 1000000 = 1 second, 10000 = 1 millisecond
         * @param int|float|long $iMircoSeconds
         * @return \Score\Process This object used for chaining
         */
        public function &setWaitDelay($iMircoSeconds)
        {
            $this->_waitDelay = $iMircoSeconds;
            return $this;
        }

        /**
         * Set the starting dir
         * @param string $iPath
         */
        public function &setStartDir($iPath)
        {
            $this->_startDir = $iPath;
            return $this;
        }

        /**
         * Add a parameter to arguments passed to process
         * @param string|array $iParam Paraments to append as arguments. In the order as added
         * @return \Score\Process This object used for chaining
         */
        public function &addParam($iParam)
        {
            if (is_null($iParam) ||$iParam == "")
                return $this;

            if (!is_array($iParam))
                $iParam = array($iParam);

            $this->_params = array_merge($this->_params, $iParam);
            return $this;
        }

        /**
         * Get the full exec path with all the parameters that will be executed.
         * @throws \Score\Exception
         */
        public function getFullExec()
        {
            if (empty($this->_command))
                throw new \Score\Exception(self::EXCEPTION_NOCMD);

            $fullExec = Server::FixDirFile($this->_command, false, (strpos($this->_command, " ") ? true : false));
            $this->_windows = Server::IsWindows();

            if (!empty($this->_params))
                $fullExec .= " " . implode(" ", $this->_params);

            if ($this->_windows && $this->_winWrap)
                $fullExec = sprintf("start \"%s\" %s", $this->_name, $fullExec);

            if (!$this->_windows && $this->_sudoWrap)
            {
                if ($this->_sudoUser && $this->_sudoPfile)
                    $fullExec = sprintf("sudo -u %s -S %s < %s", $this->_sudoUser, $fullExec, $this->_sudoPfile);
                else
                    $fullExec = sprintf("sudo %s", $fullExec);
            }

            if (empty($this->_startDir) || !file_exists($this->_startDir))
            {
                $this->_startDir = dirname(trim($this->_command, '"'));
            }

            return $fullExec;
        }

        /**
         * Run's the command by compiling the command with arguemnts and wrappers.
         * @param bool $iWaitFinish Wait for the process to finish before returning?
         * @param bool $iNoErr      Surpress Exception and return bool?
         * @return boolean Did the operation sucessfully
         */
        public function Run($iWaitFinish = true, $iNoErr = false)
        {
            $fullexec = $this->getFullExec();

            \Score\Logger::ByGlobal(__METHOD__, "Running: (" . $fullexec . ") in (" . var_export($this->_startDir, true) . ")");
            $this->_startTime = new Time();
            $this->_resource = proc_open($fullexec, $this->_pipeList, $this->_pipes, $this->_startDir, null, array('bypass_shell' => true));

            if (!is_resource($this->_resource))
            {
                if ($iNoErr)
                    throw new \Score\Exception(self::EXCEPTION_RUNFAIL);

                return false;
            }

            $procInfo = proc_get_status($this->_resource);

            $this->_pid = $procInfo["pid"];
            $this->_running = true;
            stream_set_blocking($this->_pipes[0], 0);
            stream_set_blocking($this->_pipes[2], 0);

            if (($err = stream_get_contents($this->_pipes[2])))
            {
                $this->_bufferErr = $err;
            }

            if ($iWaitFinish)
            {
                while ($this->Wait($this->_waitDelay))
                    $this->getBuffer(false);
            }
        }

        /**
         * Flush the buffer data.
         * @return \Score\Process This object used for chaining
         */
        public function &Flush()
        {
            $this->_bufferData = "";
            return $this;
        }

        /**
         * Send data to STDOUT to the process
         * @param string $iData
         * @return \Score\Process This object used for chaining
         */
        public function &Send($iData)
        {
            if ($this->isRunning())
            {
                fwrite($this->_pipes[0], $iData);

                if (substr($iData, -1) != "\n")
                {
                    fwrite($this->_pipes[0], "\n");
                }

                fflush($this->_pipes[0]);
            }

            return $this;
        }

        /**
         * Refreshes buffer from STDIN and returns the data
         * @param bool $iFlush Automatically flush the data from the buffer? Default = true
         * @return string Buffer data
         */
        public function getBuffer($iFlush = true)
        {
            $data = null;

            if ($this->_bufferSize && is_resource($this->_resource))
            {
                if ($this->_windows)
                {
                    if (is_resource($this->_pipes[1]) && ftell($this->_pipes[1] === 0))
                    {
                        $data = fgets($this->_pipes[1], $this->_bufferSize);
                        if ($data !== false)
                            $this->_bufferData .= $data;
                    }
                }
                else
                {
                    $except = null;
                    $reader = array($this->_pipes[1], $this->_pipes[2]);
                    $writer = array($this->_pipes[0]);

                    if (stream_select($reader, $writer, $except, null, $this->_bufferTO) > 0)
                    {
                        $meta = stream_get_meta_data($this->_pipes[0]);

                        if ($meta['unread_bytes'] > 0)
                        {
                            $data = fgets($this->_pipes[1], $this->_bufferSize);

                            if ($data !== false)
                                $this->_bufferData .= $data;
                        }

                        // @todo log STDERR data somewhere
                    }
                }
            }

            $data = $this->_bufferData;

            if ($iFlush)
                $this->Flush();

            return $data;
        }

        /**
         * Error stream
         * @return string
         */
        public function getError()
        {
            return $this->_bufferErr;
        }

        /**
         * Get the time of when the process was started
         * @return Score\Time Time stamp object of when the process was started
         * @throws \Score\Exception
         */
        public function getStartTime()
        {
            if (empty($this->_startTime))
                throw new \Score\Exception(self::EXCEPTION_RUNNOT);

            return $this->_startTime;
        }

        /**
         * Get the exit code from the process.
         * @return mixed Exit code
         * @throws \Score\Exception
         */
        public function getExitCode()
        {
            if (empty($this->_startTime))
                throw new \Score\Exception(self::EXCEPTION_RUNNOT);

            return $this->_exitCode;
        }

        /**
         * Return the proc status array if avaialble.
         * @return array|bool Status information
         */
        public function getStatus()
        {
            if (!is_resource($this->_resource))
            {
                return proc_get_status($this->_resource);
            }

            return null;
        }

        /**
         * Close Streams
         */
        public function _closePipes()
        {
            if (!is_resource($this->_resource))
            {
                return;
            }

            foreach ($this->_pipes as &$pipe)
            {
                if (is_resource($pipe))
                    fclose($pipe);
            }
        }

        /**
         * Kill the process
         * @return int Singal Termination
         * @throws \Score\Exception
         */
        public function Kill($iSignal = self::SIGTERM)
        {
            if (empty($this->_startTime))
                throw new \Score\Exception(self::EXCEPTION_RUNNOT);

            $this->_closePipes();
            $res = proc_terminate($this->_resource, $iSignal);
            proc_close($this->_resource);
            $this->_resource = null;
            $this->_startTime = null;

            if (!$res)
            {
                $res = self::PidTerminate($this->_pid, ($iSignal == self::SIGKILL));
            }

            return $res;
        }

        /**
         * Destruct to close out anything that needs cleanup
         */
        public function __destruct()
        {
            $this->_closePipes();
        }

        /**
         * Wait for the process to complete by so many microseconds while readin buffer data and process status.
         * Use in conjuction with while() statement.
         * <code>
         *   while($proc->Wait())
         *   {
         *      echo($proc->getBuffer());
         *   }
         * </code>
         * @param type $iMicroSeconds
         * @return bool If process is still running?
         */
        public function Wait($iMicroSeconds = 10000)
        {
            $xActive = true;
            $this->getBuffer(false);

            try
            {
                $procInfo = proc_get_status($this->_resource);

                if (!$procInfo["running"])
                {
                    proc_close($this->_resource);
                    $this->_exitCode = $procInfo["exitcode"];

                    $xActive = false;
                }
            }
            catch (\Exception $err)
            {
                // Something bad happened, but because this is usually a loop, lets just return false to exit the loop
                $xActive = false;
            }

            if (!$xActive)
                $this->getBuffer(false);
            elseif (!empty($iMicroSeconds))
                usleep($iMicroSeconds);

            return $xActive;
        }

        /**
         * Is the process still running?
         * @return bool
         */
        public function isRunning()
        {
            if (empty($this->_startTime))
                return false;

            $procInfo = proc_get_status($this->_resource);
            return $procInfo["running"];
        }

        /**
         * Get the Process ID from when it was executed
         * @return int
         */
        public function getPID()
        {
            return $this->_pid;
        }

        /**
         * Get the Resource to use with other php functions
         * @return mixed
         */
        public function &getResource()
        {
            return $this->_resource;
        }

        /**
         * This is a replacement of exec() function which quickly runs synchronously.
         * It returns the object used to execute if you need to use it for any output.
         * @param string       $iCommand Command to execute
         * @param string|array $iParams  Optional arguments to pass
         * @param string       $iName    Name of process to tag in Taskmanager. Only works if using setWinWrap()
         * @return \Score\Process
         */
        public static function &Execute($iCommand, $iParams = array(), $iName = "MyProcess")
        {
            $proc = new self();
            $proc->setCommand($iCommand, $iParams, $iName)->Run();

            return $proc;
        }

        /**
         * Is process running based on Process ID?
         * @param int $iPID Integer of process running
         * @return boolean
         */
        public static function PidRunning($iPID)
        {
            if (!is_numeric($iPID))
            {
                throw new \Score\Exception(self::EXCEPTION_NOTPID);
            }

            if (Server::IsWindows())
            {
                $xWMI = new COM('winmgmts://');
                $xProcs = $xWMI->ExecQuery("SELECT CommandLine, ProcessId FROM Win32_Process WHERE ProcessId = $iPID");

                if ( count($xProcs) == 0 )
                {
                    return false;
                }
            }
            else
            {
                $xRet = null;
                $out = array();
                exec("ps eh -p " . $iPID, $out, $xRet);

                return ($xRet == 0);
            }

            return true;
        }

        /**
         * Terminate a proccess by ID
         * @param type $iPID
         * @param type $iForceKill
         * @return boolean
         */
        public static function PidTerminate($iPID, $iForceKill = false)
        {
            if (!self::PidRunning($iPID))
            {
                return false;
            }

            if (Server::IsWindows())
            {
                $xWMI = new COM('winmgmts:{impersonationLevel=impersonate}!//./root/CIMV2');
                $xProcs = $xWMI->ExecQuery("SELECT CommandLine, ProcessId FROM Win32_Process WHERE ProcessId = $iPID");

                foreach($xProcs as $process)
                {
                    if ($iForceKill)
                    {
                        self::Execute("taskkill", "/F /PID $iPID /T");
                    }
                    else
                    {
                        $res = $process->Terminate();
                    }
                }
            }
            else
            {
                $xRet = null;
                passthru("kill " . ($iForceKill ? "-9 " : "") . $iPID, $xRet);

                return !(empty($xRet));
            }
        }
    }
}
