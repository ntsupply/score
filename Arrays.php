<?php

namespace Score
{
    use Score\Strings;

    /**
     * Use this Class for handy functions to manipulate Arrays
     * @author Ron Rebennack
     * @author Bradley Worrell-Smith
     */
    class Arrays
    {
        const SEARCH_VALUES = 0;
        const SEARCH_KEYS = 1;
        const SEARCH_EITHER = 2;
        const SEARCH_BOTH = 3;

        const ZIP_MAX = "max";
        const ZIP_MIN = "min";

        const EXCEPTION_UNKNOWN_BOUNDRY = "(101200) Unknown Boundry in Array Zip";
        const EXCEPTION_ARRAY_OR_OBJECT = "(101201) Stack must be array or object";
        const EXCEPTION_INVALID_COLUMN  = "(101202) Invalid column";

        /**
         * More better replacment for array_key_exists
         * @param array $iArray
         * @param string $iKeyName
         * @param bool $iCaseSensitive
         * @return bool
         */
        public static function KeyExists(array $iArray, $iKeyName, $iCaseSensitive = true)
        {
            if ($iCaseSensitive)
            {
                $hdArray = $iArray;
                $hdKeyName = $iKeyName;
            }
            else
            {
                $hdArray = array_change_key_case($iArray, CASE_UPPER);
                $hdKeyName = strtoupper($iKeyName);
            }

            return array_key_exists($hdKeyName, $hdArray);
        }

        /**
         * Is the given array an Associative Array?
         * @param array $iArray
         * @return bool
         */
        public static function IsAssociative($iArray)
        {
            return !(array_keys($iArray) === range(0, count($iArray) - 1));
        }

        /**
         * Filter an array for only specific keys
         * @param object|array $stack Full array to be filtered
         * @param array $keyList      List of keys that are allowed
         * @param string $prefix      Add prefix to keys
         * @return array
         * @throws \Score\Exception
         */
        public static function FilterKeys($stack, array $keyList, $prefix = null)
        {
            $new = array();

            if (is_object($stack))
            {
                $stack = get_object_vars($stack);
            }

            if (!is_array($stack))
            {
                throw new \Score\Exception(self::EXCEPTION_ARRAY_OR_OBJECT);
            }

            foreach ($stack as $key => &$val)
            {
                if (array_search($key, $keyList) !== false)
                {
                    $new[(!empty($prefix) ? $prefix : "") . $key] = $val;
                }
            }

            return $new;
        }

        /**
         * Search a given array for specific values or keys
         * @param array $iArray          Array to search
         * @param mixed $iSearchFor      What to search for
         * @param int   $iSearchType     Type of search. Use Arrays::SEARCH_ enumerators
         * @param bool  $iCaseSensitive
         * @return mixed  If any are found, it will return the found array elements
         */
        public static function Search($iArray, $iSearchFor, $iSearchType = self::SEARCH_VALUES, $iCaseSensitive = false)
        {
            $oArray = array();

            foreach ($iArray as $aKey => $aValue)
            {
                if (is_array($aValue))
                {
                    $xDrill = self::Search($aValue, $iSearchFor, $iSearchType, $iCaseSensitive);

                    if (count($xDrill) > 0)
                    {
                        $oArray = array_merge($oArray, $xDrill);
                    }

                    continue;
                }

                if (is_array($iSearchFor))
                {
                    $xFoundMatch = false;

                    foreach ($iSearchFor as $sKey => $sValue)
                    {
                        $xFoundKey = false;
                        $xFoundVal = false;

                        if ((self::IsAssociative($iSearchFor) && Strings::Compare($sKey, $aKey, $iCaseSensitive) == 0) || !self::IsAssociative($iSearchFor))
                        {
                            $xFoundKey = true;
                        }

                        if (Strings::Compare($sValue, $aValue, $iCaseSensitive) == 0)
                        {
                            $xFoundVal = true;
                        }

                        if (($iSearchType == self::SEARCH_EITHER && ($xFoundKey || $xFoundVal)) ||
                            ($iSearchType == self::SEARCH_BOTH && ($xFoundKey && $xFoundVal)) ||
                            ($iSearchType == self::SEARCH_KEYS && $xFoundKey) ||
                            ($iSearchType == self::SEARCH_VALUES && $xFoundVal))
                        {
                            $xFoundMatch = true;
                        }
                    }

                    if ($xFoundMatch)
                    {
                        array_push($oArray, array($aKey => $aValue));
                    }
                }
                else
                {
                    $xFoundKey = (Strings::Compare($aKey, $iSearchFor, $iCaseSensitive) == 0);
                    $xFoundVal = (Strings::Compare($aValue, $iSearchFor, $iCaseSensitive) == 0);

                    if (($iSearchType == self::SEARCH_EITHER && ($xFoundKey || $xFoundVal)) ||
                        ($iSearchType == self::SEARCH_BOTH && ($xFoundKey && $xFoundVal)) ||
                        ($iSearchType == self::SEARCH_KEYS && $xFoundKey) ||
                        ($iSearchType == self::SEARCH_VALUES && $xFoundVal))
                    {
                        array_push($oArray, array($aKey => $aValue));
                    }
                }
            }

            return $oArray;
        }

        /**
         * This will sort a collection based on the collection(array()) values
         *
         * @param array   $array  The array to sort
         * @param mixed   $column The column to sort by
         * @param integer $order  The order to sort the array (SORT_ASC, SORT_DESC)
         */
        public static function Sort(&$array, $column = 0, $order = SORT_ASC, $asNumeric = false)
        {
            if (empty($array))
            {
                return;
            }

            usort($array, function($a, $b) use ($column, $asNumeric, $order) {

                $cmp = 0;

                if ($asNumeric)
                {
                    $cmp = ($a[$column] == $b[$column] ? 0 : ($a[$column] > $b[$column] ? 1 : -1));
                }
                else
                {
                    $cmp = strnatcasecmp($a[$column], $b[$column]);
                }

                if ($order == SORT_DESC)
                {
                    $cmp = $cmp * gmp_neg($cmp);
                }

                return $cmp;
            });
        }

        /**
         * Take the data and convert to XML
         * @param array $iA_Data         Multi demensional array of values
         * @param string $iRootNodeName  Root Name of the XML
         * @param SimpleXMLElement $iXML Existing XML Object
         * @return string
         */
        public static function ToXml($iDataArray, $iRootNodeName = "Data", \SimpleXMLElement &$iXML = null)
        {
            if (ini_get("zend.ze1_compatibility_mode") == 1)      // turn off compatibility mode as simple xml throws a wobbly if you don't.
            {
                ini_set("zend.ze1_compatibility_mode", 0);
            }

            if (is_null($iXML))
            {
                $iXML = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$iRootNodeName />");
            }

            foreach ($iDataArray as $xKey => $iValue)
            {
                if (is_numeric($xKey))
                {
                    $xKey = "N" . $xKey;
                }
                else
                {
                    $xKey = preg_replace('/\s/', '_', preg_replace("/[^a-z\w:\-\s]/i", "", $xKey));
                }

                if (is_array($iValue) || is_object($iValue))
                {
                    $xNode = $iXML->addChild($xKey);

                    if (is_object($iValue))
                    {
                        foreach (get_object_vars($iValue) as $xKey => $xVal)
                        {
                            if (is_array($xVal))
                            {
                                self::ToXml($xVal, $iRootNodeName, $xNode);
                            }
                            else
                            {
                                $xNode->addAttribute($xKey, $xVal);
                            }
                        }
                    }
                    else
                    {
                        self::ToXml($iValue, $iRootNodeName, $xNode);
                    }
                }
                else
                {
                    $encoded = htmlentities(preg_replace_callback('/[^\x00-\x7F]/', function ($matches)
                    {
                        return '"&#"' . ord($matches[0]) . '";"';
                    }, $iValue));

                    $iXML->addChild($xKey, $encoded);
                }
            }

            return $iXML->asXML();
        }

        /**
         * Takes an object of type 'stdClass' and recursively cast it to an array
         * @param object
         * @return array
         */
        public static function ObjectToArray($object)
        {
            if (empty($object))
            {
                return $object;
            }

            if (is_object($object))
            {
                $object = get_object_vars($object);
            }

            if (is_array($object))
            {
                foreach ($object as &$dude)
                {
                    $dude = self::ObjectToArray($dude);
                }
            }

            return $object;
        }

        /**
         * Make an array using the key as from the value of the column specified
         * @param array $array
         * @param string $columnName
         * @return array
         * @throws \Score\Exception
         */
        public static function CreateKeysFromColumns($array, $columnName)
        {
            if (empty($array))
            {
                return $array;
            }

            $keys = array();

            foreach ($array as &$values)
            {
                if (!array_key_exists($columnName, $values))
                {
                    throw new \Score\Exception(self::EXCEPTION_INVALID_COLUMN);
                }

                $keys[] = $values[$columnName];
            }

            return array_combine($keys, array_values($array));
        }

        /**
         * Zip Multiple arrays to a boudry
         * @param mixed $boundry Boundry can be size of array or boundry
         * @return array
         */
        public static function Zip($boundry)
        {
            $ret = array();
            $args = func_get_args();

            if (is_int($boundry))
            {
                $count = $boundry;
            }
            elseif (strtolower($boundry) == self::ZIP_MAX)
            {
                $count = max(array_map('count', $args));
            }
            elseif (strtolower($boundry) == self::ZIP_MAX)
            {
                $count = min(array_map('count', $args));
            }
            else
            {
                throw new \Score\Exception(self::EXCEPTION_UNKNOWN_BOUNDRY);
            }

            for ($cnt = 0; $cnt < $count; $cnt++)
            {
                for ($lp = 1; $lp < count($args); $lp++)
                {
                    $val = (isset($args[$lp][$cnt])) ? $args[$lp][$cnt] : null;
                    $ret[$cnt][$lp] = $val;
                }
            }

            return $ret;
        }

        public static function Assert(&$array, $key, $value, $defaultValue = null)
        {
            if (is_null($value))
            {
                $value = $defaultValue;
            }

            if (!is_null($value))
            {
                $array[$key] = $value;
            }

            return $value;
        }
    }
}
