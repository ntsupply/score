<?php

namespace Score\Api
{
    class Role
    {
        const DEFAULT_ERROR_NO = 403;

        public $name;
        public $errNo;
        public $callback;
        protected $_crc;

        public function __construct($name, $callback = null, $errNo = self::DEFAULT_ERROR_NO)
        {
            $this->name = $name;
            $this->callback = $callback;
            $this->errNo = $errNo;
        }

        public function meets(\Score\Api\Controller $controller)
        {
            $out = false;

            $this->_crc++;

            if ($this->_crc === 1 && !empty($this->name))
            {
                if (empty($this->callback))
                {
                    $out = true;
                }
                elseif (is_callable($this->callback))
                {
                    $out = call_user_func($this->callback, $controller);
                }
            }

            $this->_crc--;

            return $out;
        }
    }
}