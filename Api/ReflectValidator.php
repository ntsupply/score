<?php 

namespace Score\Api
{
    /**
     * <code>
     *     @require fieldName,fieldInt[is_int],fieldStr[min_char=1,max_char=10]
     *     @optional fieldFloat[is_float],test[@custom=1]
     * </code>
     * <code>
     *     public function v_custom(\Score\Api\ValidatorField &$object)
     *     {
     *         if ($object->valueFromRequest != 1)
     *         {
     *             $object->validator->SetError("Oh noes!");
     *             return;
     *         }
     *         $object->valueFromRequest = "Yay";
     *     }
     * </code>
     */
    class ReflectValidator extends \Score\Data\Set
    {
        const MAIN_REGEX = "/\[(.*?)\]/";

        protected $_fields = [];
        protected $_controller;
        protected $_dataFromRequest;
        protected $_requiredData;

        /**
         * Creates new instance for Reflect Validate
         * 
         * @param string $requiredFields - Data coming back from block comments ex."username[is_string|min_char=8],password"
         * @param array $dataFromRequest
         */
        public function __construct(string $requiredFields, \Score\Api\Controller &$controller)
        {
            $this->_requiredData = preg_replace( "/\s/", '', $requiredFields);
            $this->_controller = $controller;
        }

        /**
         * Parent controller
         *
         * @return \Score\Api\Controller
         */
        public function &Controller()
        {
            return $this->_controller;
        }

        /**
         * Validate fields require or optional
         *
         * @param boolean $optional
         * @return array
         */
        public function validateFields($optional = false)
        {
            $this->SetGood();

            if (preg_match_all('/((?P<field>.*?)(?P<attr>\[.*?\])?)(?:,|\z)/', trim($this->_requiredData, ", \n\r\t\0"), $fieldMatches, PREG_SET_ORDER))
            {
                foreach ($fieldMatches as $fieldInfo) 
                {
                    // because of the regex, there will be an empty one
                    if (empty($fieldInfo[0]))
                    {
                        continue;
                    }

                    $data = new \Score\Api\ValidatorField($this, $fieldInfo["field"], trim($fieldInfo["attr"], '[]'));

                    $this->_fields[$data->fieldName] = $data->validateFunctions;

                    if (is_null($data->valueFromRequest))
                    {
                        if ($optional)
                        {
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }

                    foreach ($data->validateFunctions as $vodo)
                    {
                        if (preg_match('/\A(.*?)(=.*)?\z/', $vodo, $matches))
                        {
                            $function = $matches[1];

                            if ($function[0] == '@')
                            {
                                $callable = [$this->_controller, "v_" . trim($function, '@')];
                            }
                            else
                            {
                                $callable = [$this, "v_{$function}"];
                            }

                            $params = explode("|", ltrim($matches[2], '='));
                            array_unshift($params, $data);

                            call_user_func_array($callable, $params);
                        }

                        if ($this->Status !== self::STAT_GOOD)
                        {
                            break;
                        }
                    }

                    $this->Controller()->setData($data->fieldName, $data->valueFromRequest);
                }
            }

            return $this->_fields;
        }

        /**
         * See if the value passed is a string
         * 
         * @param \Score\Api\ValidatorField $value
         */
        public function v_is_string(&$object)
        {
            if(\is_string( $object->valueFromRequest ) === false) {
                $this->SetError("{$object->fieldName}: is not a string");
                return;
            }
        }

        /**
         * See if the value passed is a int
         * 
         * @param \Score\Api\ValidatorField $object
         */
        public function v_is_int(&$object)
        {
            $value = filter_var($object->valueFromRequest, FILTER_VALIDATE_INT);

            if ($value === false) {
                $this->SetError("{$object->fieldName}: is not an int with value of '{$object->valueFromRequest}'");
                return;
            }

            $object->valueFromRequest = $value;
        }

        /**
         * See if the value passed is a float
         * 
         * @param \Score\Api\ValidatorField $object
         */
        public function v_is_float(&$object)
        {
            $value = filter_var($object->valueFromRequest, FILTER_VALIDATE_FLOAT);

            if ($value === false) {
                $this->SetError("{$object->fieldName}: is not a float with value of '{$object->valueFromRequest}'");
                return;
            }

            $this->Controller()->setData($object->fieldName, $value);
        }

        /**
         * See if the value passed is the min length wanted from string coming back
         * 
         * @param \Score\Api\ValidatorField $object
         */
        public function v_min_char(&$object, $minVal)
        {
            if (intval(strlen( $object->valueFromRequest )) < intval( $minVal )) {
                $this->SetError("{$object->fieldName}: '{$object->valueFromRequest}' is a length of " . strlen($object->valueFromRequest) . " and minimum characters is {$minVal}.");
            }
        }

        /**
         * See if the value passed is the max length wanted from string coming back
         * 
         * @param \Score\Api\ValidatorField $object
         */
        public function v_max_char(&$object, $maxVal)
        {
            if (intval(strlen( $object->valueFromRequest )) > intval( $maxVal )) {
                $this->SetError("{$object->fieldName}: '{$object->valueFromRequest}' is a length of " . strlen($object->valueFromRequest) . " and the maximum characters is {$maxVal}.");
            }
        }

        /**
         * See if the value passed is the min value.
         * 
         * @param \Score\Api\ValidatorField $object
         */
        public function v_min_val(\Score\Api\ValidatorField &$object, $minVal)
        {
            if ($object->valueFromRequest < $minVal) {
                $this->SetError("{$object->fieldName}: '{$object->valueFromRequest}' is less than the minimum of {$minVal}.");
            }
        }

        /**
         * See if the value passed is the max value wanted.
         * 
         * @param \Score\Api\ValidatorField $object
         */
        public function v_max_val(\Score\Api\ValidatorField &$object, $maxVal)
        {
            if ($object->valueFromRequest > $maxVal) {
                $this->SetError("{$object->fieldName}: '{$object->valueFromRequest}' is greater than the maximum of {$maxVal}.");
            }
        }

        /**
         * See if the value passed is valid email.
         * 
         * @param \Score\Api\ValidatorField $object
         */
        public function v_is_email(\Score\Api\ValidatorField &$object)
        {
            $value = filter_var($object->valueFromRequest, \FILTER_VALIDATE_EMAIL);

            if ($value === false) {
                $this->SetError("{$object->fieldName}: '{$object->valueFromRequest}' is not a valid email.");
                return;
            }

            $object->valueFromRequest = trim($value);
        }

        /**
         * See if the value passed is a valid date field.
         * 
         * @param \Score\Api\ValidatorField $object
         */
        public function v_is_date(\Score\Api\ValidatorField &$object, $format = null)
        {
            $ok = false;

            if (!empty($object->valueFromRequest))
            {
                $constants = \Score\Helper::GetClassConstants('\Score\Time');

                if (empty($format) || !array_key_exists($format, $constants))
                {
                    $format = 'FORMAT_OD';
                }

                try
                {
                    $displayFormat = $constants[$format];
                    $date = new \Score\Time($object->valueFromRequest);
                    $object->valueFromRequest = $date->Display($displayFormat);
                    
                    $ok = true;
                }
                catch (\Throwable $err)
                {
                }
            }

            if ($ok === false) {
                $this->SetError("{$object->fieldName}: '{$object->valueFromRequest}' is not a valid date format of {$displayFormat}.");
                return;
            }
        }

        /**
         * See if the value passed is equal to these fields 
         * Example: fieldName[enum=info|error] - only accept info and error
         * 
         * @param \Score\Api\ValidatorField $object
         */
        public function v_enum(\Score\Api\ValidatorField &$object, $choices = null)
        {
            if (is_null($choices))
            {
                return;
            }

            $args = func_get_args();
            array_shift($args);
            $enums = array_map('strtolower', $args);
            $list = implode(",", $enums);

            $ok = false;

            if (!is_null($object->valueFromRequest) && array_search(strtolower($object->valueFromRequest), $enums) !== false)
            {
                $ok = true;
            }

            if ($ok === false) {
                $this->SetError("{$object->fieldName}: '{$object->valueFromRequest}' is not a valid entry of ({$list})");
                return;
            }
        }
    }
}


