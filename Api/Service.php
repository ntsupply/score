<?php

namespace Score\Api
{
    use \Score\Container\Cookie;
    use \Score\Api\Role;
    use \Score\Router\Route;
    
    class Service
    {
        const ROLE_FORBIDDEN = null;
        const ROLE_PUBLIC = "public";

        const RESPONSE_AUTO = null;
        const RESPONSE_RAW = 1;
        const RESPONSE_RAW_ENCODE = 2;
        const RESPONSE_DATA_SET = 3;

        protected $_svr;
        protected $_dir;
        protected $_ns;
        protected $_cw;
        /**
         * @var \Score\Api\Role[]
         */
        protected $_roles = [];

        public function __construct($namespace, $controllersDir = null)
        {
            $this->_dir = $controllersDir;
            $this->_ns = $namespace;

            $cookie = new Cookie($this, Cookie::NAME_GUID, Cookie::INSTANCE_MULTI);
            \Score\Container::globalSet($cookie);

            $this->_svr = $cookie->name;

            $this->addRole(new Role(Service::ROLE_FORBIDDEN));
            $this->addRole(new Role(Service::ROLE_PUBLIC));
        }

        public function addRole(Role $role)
        {
            $this->_roles[$role->name] = $role;
        }

        /**
         * 
         * @param string $roleName 
         * @return \Score\Api\Role 
         */
        public function getRole($roleName)
        {
            if (!array_key_exists($roleName, $this->_roles))
            {
                $roleName = null;
            }

            return $this->_roles[$roleName];
        }

        /**
         * @namespace string
         */
        public function setupCache($namespace = __CLASS__)
        {
            $this->_cw = new \Score\Cache\Worker($namespace);
        }

        /**
         * @return \Score\Cache\Worker
         */
        public function cacheWorker()
        {
            return $this->_cw;
        }

        public function setupRoutes(\Score\Router &$router, $prefix = null, $idSyntax = "\d*")
        {
            $base = (empty($prefix) ? '' : "/{$prefix}");
            $endpoint = __CLASS__ . "::handler";

            $prms = [
                "service" => $this->_svr
            ];

            $opts = [
                Route::OPT_TRAILING_SLASH => Route::TRAILING_SLASH_UNUSED,
                Route::OPT_PARAMS_STORE => Route::PARAMS_OBJECT,
                Route::OPT_OPTIONS_REQ => Route::OPTIONS_REQ_AUTO
            ];

            $router->add("{$base}/{p=any;n=controller}/{p={$idSyntax};n=id}", $endpoint, $prms, $opts);
            $router->add("{$base}/{p=any;n=controller}/{p={$idSyntax};n=id}/{p=any;n=action}", $endpoint, $prms, $opts);
            $router->add("{$base}/{p=any;n=controller}/{p={$idSyntax};n=id}/{p=any;n=action}/{p=any;n=verb}", $endpoint, $prms, $opts);
            $router->add("{$base}/{p=any;n=controller}/{p=any;n=action}", $endpoint, $prms, $opts);
            $router->add("{$base}/{p=any;n=controller}/{p=any;n=action}/{p={$idSyntax};n=id}", $endpoint, $prms, $opts);
            $router->add("{$base}/{p=any;n=controller}/{p=any;n=action}/{p={$idSyntax};n=id}/{p=any;n=verb}", $endpoint, $prms, $opts);
            $router->add("{$base}/{p=any;n=controller}/{p=any;n=action}/{p=any;n=verb}", $endpoint, $prms, $opts);
            $router->add("{$base}/{p=any;n=controller}/", $endpoint, $prms, $opts);
        }

        public function endpoint($controller)
        {
            $cleaned =  preg_replace("/[^a-z0-9]/", "", strtolower($controller));

            $classPath = "{$this->_ns}\\{$cleaned}";

            if ((empty($this->_dir) && !class_exists($classPath)) || (!empty($this->_dir) && !file_exists("{$this->_dir}/{$cleaned}.php")))
            {
                if (!class_exists($classPath))
                {
                    $this->returnError("Invalid Endpoint", $cleaned);
                }
            }

            new $classPath();
        }

        public static function handler()
        {
            $cookie = \Score\Router\Params::get("service");
            $service = \Score\Container::globalGet($cookie);
            $controller = \Score\Router\Params::get("controller");

            if ($service instanceof \Score\Api\Service && !empty($controller))
            {
                $service->endpoint($controller);
            }

            throw new \Exception("Invalid Api Service");
        }

        public function returnGood($data = null, $message)
        {
            $this->handleResponse(\Score\Data\Set::MakeGood($data, $message));
            exit();
        }

        public function returnError($message, $data = null, $httpCode = 404)
        {
            $this->handleResponse(\Score\Data\Set::MakeError($message, $data), $httpCode);
            exit();
        }

        public function returnInvalidRole($invalidRole)
        {
            $role = $this->getRole($invalidRole);

            $this->returnError("Insufficient privileges!", $this->_role, $role->errNo);
        }

        public function prepareReponse($data, $responseType = self::RESPONSE_AUTO)
        {
            if ($responseType == self::RESPONSE_AUTO)
            {
                if ($data instanceof \Score\Data\Set)
                {
                    $responseType = self::RESPONSE_DATA_SET;
                }
                elseif (!empty($data))
                {
                    $responseType = self::RESPONSE_RAW_ENCODE;
                }
            }

            switch ($responseType)
            {
                case self::RESPONSE_DATA_SET:
                    $out = $data->toJSON(\JSON_UNESCAPED_UNICODE | \JSON_NUMERIC_CHECK);
                    break;
                case self::RESPONSE_RAW_ENCODE:
                    $out = \Score\Strings::EncodeJsonUTF8($data);
                    break;
                case self::RESPONSE_RAW:
                    $out = $data;
                    break;
                default:
                    $out = null;
            }

            return $out;
        }

        public function handleResponse($data, $httpCode = 200, $exit = true, $responseType = self::RESPONSE_AUTO)
        {
            static $responded = false;

            if ($responded === false)
            {
                $out = $this->prepareReponse($data, $responseType);

                if (\Score\Server::GetVar("callback"))
                {
                    $out = \Score\Server::GetVar("callback") . '(' . $out .  ')';
                }

                ignore_user_abort(true);
                ob_end_clean();
                ob_start();

                echo($out);

                if (empty($httpCode) || $httpCode != 200)
                {
                    http_response_code($httpCode);
                }

                header("Content-Encoding: none");
                header("Content-Type: application/json");
                header("Connection: Close");
                header("Content-Length: " . ob_get_length());

                ob_end_flush();
                ob_flush();
                flush();

                if ($data instanceof \Score\Data\Set && $data->SessionCloseCallback)
                {
                    session_write_close();
                    call_user_func($data->SessionCloseCallback);
                }

                $responded = true;
            }

            if ($exit)
            {
                exit();
            }
        }
    }
}