<?php

namespace Score\Api
{
    class ValidatorField
    {
        public $fieldName;
        public $validateFunctions;
        public $valueFromRequest;
        public $validator;

        public function __construct(\Score\Api\ReflectValidator &$validator, $field, $attr)
        {
            $this->validator = $validator;
            $this->fieldName = $field;
            $this->validateFunctions = (empty($attr) ? [true] : explode(',', $attr));
            $this->valueFromRequest = $this->validator->Controller()->getData($field);
        }
    }
}