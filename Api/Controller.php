<?php

namespace Score\Api
{
    use \Score\Server;
    use \Score\Api\Service;
    use Score\Exception;

    class Controller
    {
        protected $_role = Service::ROLE_FORBIDDEN;
        protected $_controller;
        protected $_action;
        protected $_verb;
        protected $_callback;
        protected $_result;
        protected $_require;
        protected $_requireId;
        protected $_optional;
        protected $_dispatch;
        protected $_app;
        protected $_datastore;
        protected $_id;
        protected $_headers;
        protected $_cache;
        protected $_cacheKey;

        public function __construct()
        {
            $this->_controller = \Score\Router\Params::get("controller");
            $this->_action = \Score\Router\Params::get("action");
            $this->_verb = \Score\Router\Params::get("verb");
            $this->_callback = Server::GetVar("callback");
            $this->_id = \Score\Router\Params::get("id");

            if ($this->_action)
            {
                $call = '_' . trim($this->_action . "_" . $this->_verb, '_');
            }
            
            if (empty($call))
            {
                $call = "_index";
            }
            
            $try = [strtolower($this->requestType()) . $call, $call];

            foreach ($try as $c)
            {
                if (is_callable(array($this, $c)))
                {
                    $this->_dispatch = $c;
                    break;
                }
            }

            if (empty($this->_dispatch))
            {
                $this->Service()->returnError("Invalid endpoint!", [
                    "method" => $this->requestType(),
                    "module" => array_pop(explode('\\', get_called_class())),
                    "call" => $call
                ]);
            }

            $this->reflect($this->_dispatch);

            if (!$this->meetsRole($this->_role))
            {
                $this->Service()->returnInvalidRole($this->_role);
            }

            $meetsRequired = $this->meetsRequired();
            
            if ($meetsRequired->Status !== \Score\Data\Set::STAT_GOOD)
            {
                \Score\Logger::ByGlobal(__CLASS__, $meetsRequired, \Score\Logger::TYPE_ERROR);
                $this->Service()->handleResponse($meetsRequired, 500);
            }

            $meetsOptional = $this->meetsOptional();
            
            if ($meetsOptional->Status !== \Score\Data\Set::STAT_GOOD)
            {
                \Score\Logger::ByGlobal(__CLASS__, $meetsOptional, \Score\Logger::TYPE_ERROR);
                $this->Service()->handleResponse($meetsOptional, 500);
            }

            $cached = $this->getCached();

            if ($cached === false)
            {
                $this->_result = call_user_func(array($this, $this->_dispatch), $this->_id);
                $this->setCached($this->_result);
            }
            else
            {
                $this->_result = $cached;
            }

            $this->Service()->handleResponse($this->_result);
        }

        /**
         * @return \Score\Api\Service
         * @throws Exception 
         */
        public function Service()
        {
            $cookie = \Score\Router\Params::get("service");
            return \Score\Container::globalGet($cookie);
        }

        public function reflect($call)
        {
            $map = [
                "@role" => "_role",
                "@require" => "_require",
                "@required" => "_require",
                "@optional" => "_optional",
                "@cache" => "_cache"
            ];

            $reflector = new \ReflectionClass(get_called_class());
            $meth = $reflector->getMethod($call);

            $comments = str_replace("\r", "\n", $meth->getDocComment());

            foreach ($meth->getParameters() as $param)
            {
                if (!$param->isOptional())
                {
                    $this->_requireId = true;
                    break;
                }
            }

            if (preg_match_all('%(@(?:[a-zA-Z0-9]+ *[a-zA-Z0-9, ()_].*|[a-zA-Z0-9]+))$%m', $comments, $matches, PREG_PATTERN_ORDER))
            {
                foreach ($matches[0] as $comment)
                {
                    $comment = trim($comment);
                    $sp = strpos($comment, " ");
                    $key = substr($comment, 0, ($sp ? $sp : strlen($comment)));

                    if (\array_key_exists($key, $map))
                    {
                        $this->{$map[$key]} = ($sp ? trim(substr($comment, $sp)) : '');
                    }
                }
            }
        }

        public function getCacheKey()
        {
            if (empty($this->_cacheKey))
            {
                $salt = var_export($this->_datastore, true);
                $md5 = md5($salt);
                $crc = crc32($salt);
    
                $this->_cacheKey = $this->_controller . ':' . $this->_dispatch . ":{$md5}:{$crc}";
            }

            return $this->_cacheKey;
        }

        public function getCached()
        {
            $cw = $this->Service()->cacheWorker();
            $ch = \Score\Strings::Explode(str_replace(' ', '', $this->getHeader("cache_control", 'only-if-cached')), ',', '=');
            $result = false;

            if ($cw && !is_null($this->_cache) && empty($ch["no-cache"]))
            {
                $result = $cw->get($this->getCacheKey());
            }

            return $result ?: false;
        }

        public function setCached($data)
        {
            $cw = $this->Service()->cacheWorker();
            $ch = \Score\Strings::Explode(str_replace(' ', '', $this->getHeader("cache_control", 'only-if-cached')), ',', '=');

            if ($cw && !is_null($this->_cache) && empty($ch['no-store']))
            {
                $this->_cache = intval($this->_cache) ?: 60;
                $cw->set($this->getCacheKey(), $data, $this->_cache);
            }
        }

        public function meetsRole($query)
        {
            $role = $this->Service()->getRole($query);
            return $role->meets($this);
        }

        public function meetsRequired()
        {
            if ($this->_requireId == true && is_null($this->_id))
            {
                return \Score\Data\Set::MakeError("Malformed or missing required fields in request!");
            }

            if (!empty($this->_require))
            {
                $commentValidator = new \Score\Api\ReflectValidator($this->_require, $this);
                $fields = $commentValidator->validateFields();

                if( $commentValidator->Status !== \Score\Data\Set::STAT_GOOD )
                {
                    return $commentValidator;
                }

                $req = array_keys($fields);
                $has = array_keys($this->getData());

                if (sizeof(array_intersect($has, $req)) != sizeof($req))
                {
                    $commentValidator->SetError("Malformed or missing required fields in request!", $fields);
                    return $commentValidator;
                }
            }

            return \Score\Data\Set::MakeGood();
        }

        public function meetsOptional()
        {
            if (!empty($this->_optional))
            {
                $commentValidator = new \Score\Api\ReflectValidator($this->_optional, $this);
                $commentValidator->validateFields(true);

                if( $commentValidator->Status !== \Score\Data\Set::STAT_GOOD )
                {
                    return $commentValidator;
                }
            }

            return \Score\Data\Set::MakeGood();
        }

        public function getHeaders($prefix = 'HTTP_')
        {
            if (empty($this->_headers))
            {
                $this->_headers = array();
                foreach ($_SERVER as $name => $value)
                {
                    if (substr($name, 0, 5) == $prefix)
                    {
                        $this->_headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                    }
                }
            }
            return $this->_headers;
        }

        public function getHeader($name, $default = null)
        {
            $this->getHeaders();

            if (isset($this->_headers[$name]))
            {
                return $this->_headers[$name];
            }

            return $default;
        }

        public function setData($name, $value)
        {
            if (empty($this->_datastore))
            {
                $this->getData();
            }

            $this->_datastore[$name] = $value;
        }

        public function getData($name = null, $default = null)
        {
            if (empty($this->_datastore))
            {
                $data1 = $_GET;

                if (\Score\Strings::Begins($_SERVER["CONTENT_TYPE"], 'application/json'))
                {
                    $rawJSON = file_get_contents('php://input');
                    $data2 = (!empty($rawJSON) ? json_decode($rawJSON, true) : []);
                }
                else
                {
                    $data2 = $_POST;
                }

                $this->_datastore = array_merge($data1, $data2);
            }

            if (!is_null($name)) 
            {
                if ($this->dataAvailable($name))
                {
                    return $this->_datastore[$name];
                }

                return $default;
            }
            return $this->_datastore;
        }

        public function dataAvailable($name)
        {
            return isset($this->_datastore[$name]);
        }

        public function requestType()
        {
            return $_SERVER['REQUEST_METHOD'];
        }


    }

}
