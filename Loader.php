<?php

namespace Score
{
    use Score\Logger;

    /**
     * Loader settings and functions for autoloading the Score framework
     * @author Ron Rebennack
     * @author Bradley Worrell-Smith
     */
    class Loader
    {
        const EXCEPTION_INCLUDE = "(100201) Score\\Loader: Unable to include/require file: ";

        /**
         * List of namespaces and their locations
         * @var array $_spaces
         */
        protected static $_spaces = null;

        /**
         * List of fallback callbacks
         * @var array $_fallback
         */
        protected static $_fallback = array();

        /**
         * Is the Loader registered with the SPL Autoloader
         * @var bool
         */
        protected static $_registered = false;

        /**
         * Last in spl_autoload list.  This is used to know if to throw exceptions when not loaded
         * @var bool
         */
        protected static $_last = null;

        /**
         * Use require instead of include to help force loading control
         * @var bool
         */
        protected static $_require = false;

        /**
         * Checks local list of namespaces and appends
         */
        protected static function _checkSpaces()
        {
            if ( is_null(self::$_spaces) )
            {
                self::$_spaces = array();
                self::$_spaces[__NAMESPACE__] = array("path" => __DIR__ . DIRECTORY_SEPARATOR, "delimiter" => "\\");
            }
        }

        /**
         * Check to make sure Loader registered with spl_autoload and set if it is the last in list
         * @return void
         */
        protected static function _checkAutoload()
        {
            if ( !self::$_registered )
            {
                return;
            }

            $xRes = false;
            $xList = spl_autoload_functions();

            if ( is_array($xList) )
            {
                foreach ( $xList as $xKey => $xLoad )
                {
                    if ( $xLoad == array(__CLASS__, "Load") )
                    {
                        $xRes = $xKey;
                        break;
                    }
                }
            }

            if ( $xRes === false )
            {
                spl_autoload_register(__CLASS__ . "::Load");
                self::$_last = true;
            }
            else
            {
                self::$_last = ($xRes == (sizeof($xList) - 1));
            }
        }

        /**
         * Register this Loader with PHP spl autoloader
         */
        public static function Register()
        {
            self::$_registered = true;
            self::_checkAutoload();
            self::Load("Score\\Logger");
            self::Load("Score\\Exception");
        }

        /**
         * Adds a namespace with properties of how the autoloader will work
         * @param string $rootName    Root Name of the Namespace
         * @param string $dir         Directory relative to the class name
         * @param bool $useUnderscore Set to true only if the Class name schema follows underscores. ex(Score_Loader)
         * @param bool $replaceExists Replace namespace if previously set.  Resolves with multiple libs
         */
        public static function addNamespace($rootName, $dir, $useUnderscore = false, $replaceExists = true)
        {
            self::Register();
            self::_checkSpaces();

            if (!$replaceExists && array_key_exists($rootName, self::$_spaces))
            {
                return;
            }

            $xDir = \Score\Server::GetFilePath($dir);

            if ($xDir == false)
            {
                $xDir = $dir;
            }

            self::$_spaces[$rootName] = array("path" => $xDir, "delimiter" => ($useUnderscore ? "_" : "\\"));
        }

        /**
         * Add a Fallback callback if local list fails or if there is a custom runner
         * @param type $callback
         */
        public static function addFallback($callback)
        {
            self::$_fallback[] = $callback;
        }

        /**
         * Turn Require option on/off.  If true, require() will be used to open files on load. False will use include()
         * @param bool $on
         */
        public static function setRequired($on = false)
        {
            self::$_require = ($on ? true : false);
        }

        /**
         * Log if Logger is loaded
         * @param string $uniqueName
         * @param mixed $message
         * @param mixed $type
         */
        protected static function _log($uniqueName, $message, $type = null)
        {
            if (class_exists("Score\Logger", false))
            {
                if (empty($type))
                {
                    $type = Logger::TYPE_INFO;
                }

                Logger::ByGlobal($uniqueName, $message, $type);
            }
        }

        /**
         * Auto loader called by PHP if something fails
         * @param string $name Class Name Called
         * @param bool $trapError Trap Error/Execptions
         * @return bool Was it included and readable
         * @throws \Score\Exception
         * @throws \Exception
         */
        public static function Load($name, $trapError = true)
        {
            self::_log(__METHOD__, "Class: $name");
            self::_checkSpaces();

            if (class_exists($name, false) || interface_exists($name, false))
            {
                return true;
            }

            $Loaded = false;
            $classPath = "";
            $name = ltrim($name, "\\"); // Remove leading slash to check root name

            foreach (self::$_spaces as $key => $nsData)
            {
                if (substr($name, 0, strlen($key) + 1) != ($key . $nsData["delimiter"]))
                {
                    continue;
                }

                $sub = substr($name, strlen($key) + 1);
                $bits = explode($nsData["delimiter"], $sub); // substr($name, strlen($xName) + 1));

                $baseFile = array_pop($bits) . ".php";

                // Get path without trailing slash and re-add (fix for windows based path)
                $classPath = rtrim($nsData["path"], "\\/") . DIRECTORY_SEPARATOR;
                $classPath .= implode(DIRECTORY_SEPARATOR, $bits);
                $classPath = rtrim($classPath, "\\/") . DIRECTORY_SEPARATOR . $baseFile;

                if (!file_exists($classPath) && !stream_resolve_include_path($classPath))
                {
                    $classPath = "";
                }
            }

            if (empty($classPath))
            {
                $dirs = explode(PATH_SEPARATOR, get_include_path());
                foreach ($dirs as $cdir)
                {
                    $check = $cdir . DIRECTORY_SEPARATOR . $name . ".php";

                    if (file_exists($check))
                    {
                        $classPath = $check;
                        break;
                    }
                    else
                    {
                        self::_log(__METHOD__, "NOT FOUND: $check");
                    }
                }
            }

            if (!empty($classPath))
            {
                $Loaded = self::_load($name, $classPath, self::$_require);
            }

            if ( !$Loaded )
            {
                foreach ( self::$_fallback as $xCallback )
                {
                    $res = call_user_func($xCallback, $name);

                    if ( $res )
                    {
                        $Loaded = true;
                        break;
                    }
                }
            }

            if ( $Loaded )
            {
                self::_log(__METHOD__, "LOADED: $name");
            }
            else
            {
                self::_log(__METHOD__, "NOT FOUND: $name");
                self::_checkAutoload();

                try
                {
                    // If last in spl_autoload, throw exception
                    if ( self::$_last )
                    {
                        if ( class_exists("\Score\Exception", false) )
                        {
                            foreach (debug_backtrace() as $trace)
                            {
                                if (empty($trace["file"]) || $trace["function"] == "WriteLog" || $trace["function"] == "__construct")
                                {
                                    continue;
                                }
    
                                $line = $trace["line"] . '@' . $trace["file"];
    
                                if (!empty($trace["function"]))
                                {
                                    $line .= ' ' . $trace["function"];
                                }
    
                                self::_log(__METHOD__, "  $line", Logger::TYPE_VERBOSE);
                            }
    
                            //throw new \Score\Exception(self::EXCEPTION_INCLUDE . $name);
                        }
                        else
                        {
                            //throw new \Exception(self::EXCEPTION_INCLUDE . $name);
                        }
                    }
                }
                catch (\Exception $err)
                {
                    if (!$trapError)
                    {
                        throw $err;
                    }
                }
            }

            return $Loaded;
        }

        /**
         * Load the class and check for exceptions
         * @param string $name
         * @param string $path
         * @return boolean
         */
        protected static function _load($name, $path, $useRequire = false)
        {
            try
            {
                if ($useRequire)
                {
                    require_once($path);
                }
                else
                {
                    include_once($path);
                }
            }
            catch ( \Exception $err ) // Use general exception here in case of error in include
            {
                self::_log(__METHOD__, "Exception: " . $err->getMessage());
                return false;
            }

            return (class_exists($name, false) || interface_exists($name, false) || (function_exists("trait_exists") && trait_exists($name, false)));
        }

        /**
         * Get the stack of Namespaces set with Loader
         * @param string $name  Unique name of Namespace
         * @return array|boolean
         */
        public static function getNamespace($name = null)
        {
            if ( empty($name) )
            {
                return self::$_spaces;
            }

            if ( !array_key_exists($name, self::$_spaces) )
            {
                return false;
            }

            return self::$_spaces[$name];
        }
    }
}
