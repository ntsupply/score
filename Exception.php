<?php

namespace Score
{
    use Score\Logger;

    class Exception extends \Exception
    {
        public function __construct($message = null, $code = 0, Exception $previous = null)
        {
            /**
             * If $message is formatted correctly, we will separate the code from the message.
             */
            if (preg_match('/\((?P<Code>\d{6}?)\)\s(?P<Message>.*+)/x', $message, $regs))
            {
                $code = $regs['Code'];
                $message = $regs['Message'];
            }

            if (!is_string($message))
                $message = Logger::PrintVar($message);

            /* Removed Logger here because if logger is loaded, it will listen for UNPROTECTED Exceptions
             *    Logger::ByGlobal(__METHOD__, "($code) $message");
             */

            parent::__construct($message, $code, $previous);
        }
    }
}
