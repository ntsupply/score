<?php

namespace Score
{
    class Image extends \Score\Image\Resource
    {
        public static $acceptableTypes = [
            \IMAGETYPE_PNG,
            \IMAGETYPE_JPEG,
            \IMAGETYPE_GIF,
            \IMAGETYPE_BMP,
            \IMAGETYPE_WEBP
        ];

        public function addLayer(\Score\Image\Layer $layer, $dst_x = 0, $dst_y = 0)
        {
            $layer->apply($this, $dst_x, $dst_y);
        }

        public function render($type = \IMAGETYPE_PNG, $file = null)
        {
            $extension = explode('/', image_type_to_mime_type($type))[1];
            
            if (in_array($type, self::$acceptableTypes))
            {
                return call_user_func("\image" . strtolower($extension), $this->resource(), $file);
            }

            return false;
        }


        public static function fromString($data, $base64 = false)
        {
            $raw = ($base64 ? base64_decode($data) : $data);
            $image = imagecreatefromstring($raw);

            $img = new static();
            $img->_reset($image);

            return $img;
        }

        public static function fromImageResource($image)
        {
            $img = new static();
            $img->_reset($image);

            return $img;
        }

        public static function fromFile($file)
        {
            $img = new static();
            $type = exif_imagetype($file);
            $extension = explode('/', mime_content_type($file))[1];

            if (in_array($type, self::$acceptableTypes))
            {
                $img->_reset(call_user_func("\imagecreatefrom" . strtolower($extension), $file));
            }

            return $img;
        }

        public static function quickPixel()
        {
            header('Content-Type: image/png');
            header('Cache-Control: no-cache');
            header('Pragma: no-cache');
            echo(base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII='));
            exit();
        }
    }
}
