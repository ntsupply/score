<?php

namespace Score\Data
{
    /**
     * XML Serializer
     * Serializer that drills down through arrays and objects
     * @todo Change to use XMLDoc or SimpleXML in case of large objects or streaming
     * @author Bradley Worrell-Smith
     * @author Sean Barton
     * @link http://stackoverflow.com/questions/137021/php-object-as-xml-document
     * @link http://www.sean-barton.co.uk/2009/03/turning-an-array-or-object-into-xml-using-php/
     */
    class XMLSerializer
    {
        const EXCEPTION_NOTOBJECT = "(100425) Not an Object";
        const EXCEPTION_NOTARRAY = "(100426) Not an Array";

        /**
         * Generate XML from Object
         * @param object $obj  This must be an object instance
         * @param string $root Root name of the node. Default is the Class Name
         * @return string
         * @throws \Score\Exception
         */
        public static function FromObj($obj, $root = null)
        {
            if (!is_object($obj))
            {
                throw new \Score\Exception(self::EXCEPTION_NOTOBJECT);
            }

            $array = get_object_vars($obj);
            if (empty($root))
            {
                $root = explode("\\", get_class($obj));
                $root = end($root);
            }

            return self::FromArray($array, $root);
        }

        /**
         * Generate XML from Array
         * @param array $array This must be an array
         * @param string $root Root name of the XML Document
         * @param string $node Node override name
         * @return string
         * @throws \Score\Exception
         */
        public static function FromArray(array $array, $root = 'root', $node = null)
        {
            if (!is_array($array))
            {
                throw new \Score\Exception(self::EXCEPTION_NOTARRAY);
            }

            $xml = '<?xml version="1.0" encoding="UTF-8" ?>';

            $xml .= '<' . $root . '>';
            $xml .= self::_fromArray($array, $node);
            $xml .= '</' . $root . '>';

            return $xml;
        }

        /**
         * The worker that drills objects and arrays
         * @param mixed $data Data to serialize
         * @param mixed $node The parent node
         * @return string XML response
         */
        private static function _fromArray($data, $node = null)
        {
            $xml = '';

            if (is_object($data))
            {
                $data = get_object_vars($data);
            }

            if (is_array($data))
            {
                foreach ($data as $key => $value)
                {
                    if (is_numeric($key))
                    {
                        $key = $node;
                    }

                    $drill = self::_fromArray($value, $key);

                    if (is_array($value))
                    {
                        $xml .= '<' . $key . '>' . $drill . '</' . $key . '>';
                    }
                    else
                    {
                        $xml .= $drill;
                    }
                }
            }
            else
            {
                $xml = '<' . $node . '>' . htmlspecialchars($data, ENT_QUOTES) . '</' . $node . '>';
            }

            return $xml;
        }

        public function __toString()
        {
            return self::FromObj($this);
        }
    }
}