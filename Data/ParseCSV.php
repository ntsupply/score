<?php

namespace Score\Data
{
    /**
     * ParseCSV Class to handle what fgetcsv could not.
     * @author Bradley Worrell-Smith
     * @todo Make Qualifier be able to handle Left & Right. Ex: array("[", "]")
     */
    class ParseCSV implements \Iterator
    {
        const MODE_PREPARSE = 1;
        const MODE_ONTHEFLY = 2;

        const FLAG_PARSE = 0;
        const FLAG_START_FIELD = 1;
        const FLAG_READ_FIELD = 2;
        const FLAG_SAVE_FIELD = 3;

        protected $_delimiter;
        protected $_qualifier;
        protected $_quantifier;
        protected $_mode;
        protected $_nullString = null;

        protected $_filters = array();
        protected $_handle = null;
        protected $_data = array();
        protected $_lineNum = -1;
        protected $_position = 0;
        protected $_newLine = "\n";

        /**
         * ParseCSV Constructor
         * @param string $file       Path to file
         * @param string $delimiter  Delimiter of fields
         * @param string $qualifier  Enclosure for fields in case a field contains a delimiter
         * @param string $quantifier Character to escape the Qualifier
         * @param int    $parseMode       MODE_PREPARSE or MODE_ONTHEFLY; On the fly: faster for large files. Preparse runs after this function is done.
         * @param string $nullString Use in case you want a field value to be checked and save as a null. Ex: "?" = NULL
         */
        public function __construct($file, $delimiter = ",", $qualifier = "\"", $quantifier = "\\", $parseMode = self::MODE_PREPARSE, $nullString = null)
        {
            $this->_mode = $parseMode;
            $this->_delimiter = $delimiter;
            $this->_qualifier = $qualifier;
            $this->_quantifier = $quantifier;
            $this->_nullString = $nullString;
            $this->_FileSize = filesize($file);

            if (!empty($this->_nullString))
            {
                $this->AddFilter(array($this, "_NullFilter"));
            }

            if (!$this->_handle = fopen($file, "r"))
            {
                return;
            }

            if ($parseMode == self::MODE_PREPARSE)
            {
                $this->_doParse();
            }
        }

        /**
         * Deconstruct to close the file pointer
         */
        public function __destruct()
        {
            fclose($this->_handle);
        }

        /**
         * Runs the parser
         * @return mixed true/false or Row entry if MODE_ONTHEFLY
         */
        protected function _doParse()
        {
            $row = array();
            $field = null;
            $isNewLine = true;
            $fieldNum = -1;
            $lastByte = null;
            $fieldFlag = self::FLAG_PARSE;
            $isWhitespace = true;
            $isQualified = false;

            if (!is_resource($this->_handle) || feof($this->_handle))
            {
                return false;
            }

            $currByte = null;
            while (!feof($this->_handle))
            {
                $lastByte = $currByte;
                $currByte = fread($this->_handle, 1);
                $cr = false;

                while ($currByte == "\r")
                {
                    $currByte = fread($this->_handle, 1);
                    $cr = true;
                }

                $currPos = ftell($this->_handle);

                if (($cr || $currByte == $this->_newLine || feof($this->_handle)) && !$isQualified)
                {
                    $fieldFlag = self::FLAG_SAVE_FIELD;
                    $isNewLine = true;

                    if ($cr && $currByte != $this->_newLine && !feof($this->_handle))
                    {
                        fseek($this->_handle, $currPos - 1);
                    }
                }
                elseif ($isWhitespace && $currByte != ' ' && (ord($currByte) < 126))
                {
                    $isNewLine = false;
                    $isWhitespace = false;

                    if ($currByte == $this->_delimiter)
                    {
                        if ($lastByte == $this->_delimiter)
                        {
                            $fieldNum++;
                            $fieldFlag = self::FLAG_SAVE_FIELD;
                        }
                        else
                        {
                            $isWhitespace = true;
                            continue;
                        }
                    }

                    $fieldFlag = self::FLAG_START_FIELD;

                    if ($currByte == $this->_qualifier)
                    {
                        $isQualified = true;
                        continue;
                    }
                }

                if ($fieldFlag == self::FLAG_START_FIELD)
                {
                    $fieldNum++;
                    $fieldFlag = self::FLAG_READ_FIELD;
                }

                if ($fieldFlag == self::FLAG_READ_FIELD)
                {
                    if (($isQualified && $currByte == $this->_qualifier) || (!$isQualified && $currByte == $this->_delimiter))
                    {
                        if ($lastByte == $this->_quantifier)
                        {
                            $lastByte = null;
                            $field = substr_replace($field, $currByte, -1, 1);
                            continue;
                        }
                        elseif ($currByte == $this->_qualifier || ($this->_qualifier == $this->_quantifier && $currPos < $this->_FileSize))
                        {
                            $xNext = fread($this->_handle, 1);

                            if ($xNext != $currByte)
                            {
                                $fieldFlag = self::FLAG_SAVE_FIELD;
                                fseek($this->_handle, $currPos);
                            }
                        }
                        else
                        {
                            $fieldFlag = self::FLAG_SAVE_FIELD;
                        }
                    }
                }

                if ($fieldFlag == self::FLAG_READ_FIELD)
                {
                    $field .= $currByte;
                }

                if ($fieldFlag == self::FLAG_SAVE_FIELD)
                {
                    if (!$isWhitespace)
                    {
                        foreach (array_reverse($this->_filters) as $xFilter)
                        {
                            if (is_callable($xFilter))
                            {
                                call_user_func_array($xFilter, array(&$field));
                            }
                        }

                        $row[$fieldNum] = $field;
                    }

                    if ($isNewLine)
                    {
                        if (feof($this->_handle) && (empty($row) || (sizeof($row) == 1 && $row[0] == '')))
                        {
                            break;
                        }

                        $this->_lineNum++;
                        $this->_data[$this->_lineNum] = $row;

                        if ($this->_mode == self::MODE_ONTHEFLY)
                        {
                            return $row;
                        }
                        else
                        {
                            $row = array();
                            $fieldNum = -1;
                        }
                    }

                    $field = "";
                    $fieldFlag = self::FLAG_PARSE;
                    $lastByte = null;
                    $isWhitespace = true;
                    $isQualified = false;
                }
            }

            return true;
        }

        /**
         * Null Character/String Filter
         * @param string $field String/Field to be filtered By reference
         */
        protected function _NullFilter(&$field)
        {
            if (!empty($this->_nullString) && $field == $this->_nullString)
            {
                $field = null;
            }
        }

        /**
         * Rewind the Iterator to the first element
         */
        public function rewind()
        {
            $this->_position = 0;
            reset($this->_data);
        }

        /**
         * Return the current element
         * @return mixed
         */
        public function current()
        {
            $xRow = current($this->_data);

            if ($xRow === false && $this->_mode == self::MODE_ONTHEFLY)
            {
                $xRow = $this->_doParse();
            }

            if ($xRow !== false)
            {
                return $xRow;
            }

            return false;
        }

        /**
         * Return the key of the current element
         * @return scalar scalar on success, integer 0 on failure.
         */
        public function key()
        {
            return key($this->_data);
        }

        /**
         * Move forward to next element
         * @return void Any returned value is ignored.
         */
        public function next()
        {
            $row = next($this->_data);

            if ($row === false && $this->_mode == self::MODE_ONTHEFLY)
            {
                $row = $this->_doParse();
            }

            if ($row !== false)
            {
                ++$this->_position;
                if ($this->_position < sizeof($this->_data))
                {
                    return $this->_data[$this->_position];
                }
            }

            return false;
        }

        /**
         * Is the current position valid
         * @return void Any returned value is ignored.
         */
        public function valid()
        {
            return ($this->current() !== false);
        }

        /**
         * Adds a Callback to the Filter Stack to filter fields as it is parsed
         * @param mixed $iCallback Callback to your own function/method
         */
        public function AddFilter($iCallback)
        {
            $this->_filters[] = $iCallback;
        }

        /**
         * Get Row of Data.
         * @return array If MODE_PREPARSE then it will be all data, otherwise just the current Row
         */
        public function Data()
        {
            if ($this->_mode == self::MODE_PREPARSE)
            {
                return $this->_data;
            }
            else
            {
                return $this->current();
            }
        }
    }
}
