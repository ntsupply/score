<?php

namespace Score\Data
{
    /**
     * Read XML into array.  This utilized php's XMLReader.  This is faster and uses
     * less memory than php's SimpleXML and converting it to an array.
     * @author Ron Rebennack
     * @author Bradley Worrell-Smith
     */
    class XMLArray
    {
        const TYPE_URI = "uri";
        const TYPE_FILE = "file";
        const TYPE_STRING = "string";

        const TAG_ROOT = "@ROOT@";
        const TAG_TEXT = "@TEXT@";
        const TAG_DATA = "@DATA@";
        const TAG_HOLD = "@HOLD@";

        const READ_DOM = "dom";
        const READ_ASSOC = "assoc";

        /**
         * Array to store DOM info
         * @var array
         */
        protected $_dom = array();
        /**
         * Associative Array with data
         * @var array
         */
        protected $_assoc = array();
        /**
         * Reader used
         * @var XMLReader
         */
        protected $_reader = null;

        /**
         * Read XML into an Array
         * @param string $iSource   Path to source XML
         * @param bool iIncludeRoot Include the root node?
         * @param string $iEncoding Encode format
         * @return array|bool
         */
        public static function getDataByURI($iSource, $iIncludeRoot = true, $iEncoding = 'UTF-8')
        {
            $new = new self();
            if ($new->load($iSource, self::TYPE_URI, $iEncoding))
            {
                return $new->getData($iIncludeRoot);
            }

            return false;
        }

        /**
         * Read XML into an Array
         * @param string $iSource   Path to source XML
         * @param bool iIncludeRoot Include the root node?
         * @param string $iEncoding Encode format
         * @return array|bool
         */
        public static function getDataByString($iSource, $iIncludeRoot = true, $iEncoding = 'UTF-8')
        {
            $new = new self();
            if ($new->load($iSource, self::TYPE_STRING, $iEncoding))
            {
                return $new->getData($iIncludeRoot);
            }

            return false;
        }

        /**
         * Read XML into an Array
         * @param string $iSource   Path to source XML
         * @param bool iIncludeRoot Include the root node?
         * @param string $iEncoding Encode format
         * @return array|bool
         */
        public static function getDataByFile($iSource, $iIncludeRoot = true, $iEncoding = 'UTF-8')
        {
            $new = new self();
            if ($new->load($iSource, self::TYPE_FILE, $iEncoding))
            {
                return $new->getData($iIncludeRoot);
            }

            return false;
        }

        /**
         * Read XML into an Array
         * @param string $iSource   Path to source XML
         * @param string $iType     Type to read from.
         * @param string $iEncoding Encode format
         * @return bool
         */
        public function load($iSource, $iType = self::TYPE_URI, $iEncoding = 'UTF-8')
        {
            $res = false;
            $this->_dom = array();
            $this->_assoc = array();
            $this->_reader = new \XMLReader();

            if ($iType == self::TYPE_URI || $iType == self::TYPE_FILE)
            {
                $res = $this->_reader->open($iSource, $iEncoding, LIBXML_NOERROR | LIBXML_NOWARNING | 1 | 1<<19);
            }

            if ($iType == self::TYPE_STRING)
            {
                $res = $this->_reader->xml($iSource, $iEncoding);
            }

            // $reader->setParserProperty(_reader::VALIDATE, true);
            // $reader->isValid();
            return ($res ? true : false);
        }

        /**
         * Return Array from current position in Reader
         * @param string $iName Name of Node
         * @return array
         */
        protected function _readDomNode($iName)
        {
            $node = array("name" => $iName, "attr" => array(), "text" => null, "elements" => array());
            $attr = null;

            while ($this->_reader->read())
            {
                if ($this->_reader->nodeType == \XMLReader::END_ELEMENT)
                {
                    break;
                }

                if ($this->_reader->nodeType == \XMLReader::ELEMENT)
                {
                    if ($this->_reader->hasAttributes)
                    {
                        $attr = $this->_readDomAttr();
                    }

                    $hold = $this->_readDomNode($this->_reader->name);

                    $hold["attr"] = $attr;
                    $node["elements"][] = $hold;
                }

                if ($this->_reader->nodeType == \XMLReader::TEXT)
                {
                    $node["text"] = $this->_reader->value;
                }
            }

            return ($iName == null ? $node["elements"] : $node);
        }

        /**
         * Return Addribute Array from current position in Reader
         * @return array
         */
        protected function _readDomAttr()
        {
            $attr = array();

            while ($this->_reader->moveToNextAttribute())
            {
                $attr[$this->_reader->name] = $this->_reader->value;
            }

            $this->_reader->moveToElement();
            return $attr;
        }

        /**
         * Return Array from current position in Reader
         * @param string $iName Name of Node
         * @return array
         */
        protected function _readDataNode($iType, $iName, $iAttr = null)
        {
            $value = array("type" => $iType, "name" => $iName, "value" => array(), "last" => null, "attr" => $iAttr, "stacked" => false);

            while ($this->_reader->read())
            {
                $attr = null;

                if ($this->_reader->nodeType == \XMLReader::END_ELEMENT)
                {
                    break;
                }

                if ($this->_reader->nodeType == \XMLReader::ELEMENT)
                {
                    $hold = array("type" => self::TAG_HOLD, "name" => $this->_reader->name, "value" => null);

                    if ($this->_reader->hasAttributes)
                    {
                        $attr = $this->_readDomAttr();
                    }

                    if (!$this->_reader->isEmptyElement)
                    {
                        $hold = $this->_readDataNode(self::TAG_DATA, $this->_reader->name, $attr);
                    }

                    if ($hold["type"] != self::TAG_HOLD)
                    {
                        if ($iAttr)
                        {
                            $value["value"]["@attr"] = $iAttr;
                        }

                        if (!array_key_exists($hold["name"], $value["value"]))
                        {
                            $value["value"][$hold["name"]] = $hold["value"];
                        }
                        elseif ($value["stacked"])
                        {
                            $value["value"][$hold["name"]][] = $hold["value"];
                        }
                        else
                        {
                            $value["value"][$hold["name"]] = array($value["value"][$hold["name"]], $hold["value"]);
                            $value["stacked"] = true;
                        }

                        if ($value["last"] === null)
                        {
                            $value["last"] = $hold["name"];
                        }
                    }
                }

                if ($this->_reader->nodeType == \XMLReader::TEXT)
                {
                    $value["type"] = self::TAG_TEXT;
                    $value["value"] = $this->_reader->value;
                }
            }

            return $value;
        }

        /**
         * Get DOM information in array
         * @return array
         */
        public function getDom()
        {
            if (!$this->_reader)
            {
                return null;
            }

            if (empty($this->_dom))
            {
                $this->_dom = $this->_readDomNode(null);
            }

            return $this->_dom;
        }

        /**
         * Get an array from XML
         * @param bool iIncludeRoot Include the root node?
         * @return array
         */
        public function getData($iIncludeRoot = true)
        {
            if (!$this->_reader)
            {
                return null;
            }

            if (empty($this->_assoc))
            {
                $this->_assoc = $this->_readDataNode(self::TAG_ROOT, null);
                $this->_assoc = $this->_assoc["value"];
            }

            if ($iIncludeRoot)
            {
                return $this->_assoc;
            }

            return end($this->_assoc);
        }
    }
}