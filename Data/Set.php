<?php

namespace Score\Data
{
    /**
     * DataSet to handle data for a reponse
     * @author Ron Rebennack
     * @author Bradley Worrell-Smith
     */
    class Set
    {
        const STAT_GOOD = "GOOD";
        const STAT_INFO = "INFO";
        const STAT_ERROR = "ERROR";
        const STAT_NODATA = "NODATA";
        const STAT_PARTIAL = "PART";
        const STAT_WARNING = "WARNING";
        const STAT_INVALID = "INVALID";

        /**
         * Resulted Data
         * @var mixed 
         */
        public $Data = null;
        /**
         *
         * @var string Status of the data
         */
        public $Status = "";
        /**
         * Error message or information about the data
         * @var string 
         */
        public $Message;
        /**
         * Hidden flags and variables not to expose upon serialization
         * @var array
         */
        protected $_flags = array();

        /**
         * Accessor for hidden flags
         * @param type $name
         * @param type $value
         */
        public function __set($name, $value)
        {
            $this->_flags[$name] = $value;
        }

        /**
         * Accessor for hidden flags
         * @param type $name
         * @return mixed
         */
        public function __get($name)
        {
            if (array_key_exists($name, $this->_flags))
            {
                return $this->_flags[$name];
            }

//  Not sure if triggering error is the best thing to do
//          trigger_error(sprintf("Undefined flag %s in %s", $name, __METHOD__), E_USER_NOTICE);

            return null;
        }

        /**
         * Accessor for hidden flags
         * @param type $name
         */
        public function __isset($name)
        {
            return isset($this->_flags[$name]);
        }

        /**
         * Accessor for hidden flags
         * @param type $name
         */
        public function __unset($name)
        {
            unset($this->_flags[$name]);
        }

        /**
         * Get a list of all the hidden flags
         * @return array
         */
        public function getFlags()
        {
            return $this->_flags;
        }

        /**
         * Convert object to JSON encoded data
         * @param int $iOptions     These options are the JSON_Constants
         * @param string $iCallback Prefix the JSON for a callback for Javascript. Scrub this before calling.
         * @return string
         * @link http://www.php.net/manual/en/json.constants.php
         */
        public function toJSON($iOptions = \JSON_UNESCAPED_UNICODE, $iCallback = null)
        {
            $json = json_encode($this, $iOptions);

            switch (json_last_error())
            {
                case JSON_ERROR_NONE:
                    break;
                case JSON_ERROR_DEPTH:
                    \Score\Logger::ByGlobal(__METHOD__, 'Maximum stack depth exceeded', \Score\Logger::TYPE_ERROR);
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    \Score\Logger::ByGlobal(__METHOD__, ' - Underflow or the modes mismatch', \Score\Logger::TYPE_ERROR);
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    \Score\Logger::ByGlobal(__METHOD__, 'Unexpected control character found', \Score\Logger::TYPE_ERROR);
                    break;
                case JSON_ERROR_SYNTAX:
                    \Score\Logger::ByGlobal(__METHOD__, 'Syntax error, malformed JSON', \Score\Logger::TYPE_ERROR);
                    break;
                case JSON_ERROR_UTF8:
                    \Score\Logger::ByGlobal(__METHOD__, 'Malformed UTF-8 characters, possibly incorrectly encoded', \Score\Logger::TYPE_ERROR);
                    break;
                default:
                    \Score\Logger::ByGlobal(__METHOD__, 'Unknown error', \Score\Logger::TYPE_ERROR);
                    break;
            }

            if (isset($iCallback))
            {
                $json = $iCallback . "($json);";
            }

            return $json;
        }

        /**
         * Take this object at return it as XML string
         * @param type $iRoot Root node name for XML document.
         * @return string Object converted to XML
         */
        public function toXML($iRoot = null)
        {
            return \Score\Data\XMLSerializer::FromObj($this, $iRoot);
        }

        /**
         * Initalize this class with data
         * @param mixed  $iData    Most likey an array of data
         * @param mixed  $iStatus  Use the built-in Constants or make your own version
         * @param string $iMessage String for if the status results in an error.
         * @return \Score\Data\Set
         */
        public function Init($iData, $iStatus = self::STAT_GOOD, $iMessage = null)
        {
            $this->Data = $iData;
            $this->Status = $iStatus;
            $this->Message = $iMessage;

            return $this;
        }

        /**
         * Quickly set the Data and message properties
         * @param mixed $data
         * @param string $message
         * @return \Score\Data\Set
         */
        public function &SetDataMessage($data = null, $message = null)
        {
            if (!is_null($data))
            {
                $this->Data = $data;
            }

            if (!empty($message))
            {
                $this->Message = $message;
            }

            return $this;
        }

        /**
         * Quickly set to Status to GOOD with Data and Message
         * @param mixed $data
         * @param string $message
         * @return \Score\Data\Set
         */
        public function &SetGood($data = null, $message = null)
        {
            $this->SetDataMessage($data, $message);
            $this->Status = self::STAT_GOOD;

            return $this;
        }

        /**
         * Quickly set to Status to ERROR with Data and Message
         * @param string $message
         * @param mixed $data
         * @return \Score\Data\Set
         */
        public function &SetError($message, $data = null)
        {
            $this->SetDataMessage($data, $message);
            $this->Status = self::STAT_ERROR;

            return $this;
        }

        /**
         * Quickly set to Status to WARN with Data and Message
         * @param string $message
         * @param mixed $data
         * @return \Score\Data\Set
         */
        public function &SetWarning($message, $data = null)
        {
            $this->SetDataMessage($data, $message);
            $this->Status = self::STAT_WARNING;

            return $this;
        }

        /**
         * Quickly set to Status to NODATA with Data and Message
         * @param string $message
         * @param mixed $data
         * @return \Score\Data\Set
         */
        public function &SetNoData($message = null, $data = null)
        {
            $this->SetDataMessage($data, $message);
            $this->Status = self::STAT_NODATA;

            return $this;
        }

        /**
         * Quickly set to Status to INVALID with Data and Message
         * @param string $message
         * @param mixed $data
         * @return \Score\Data\Set
         */
        public function &SetInvalid($message = null, $data = null)
        {
            $this->SetDataMessage($data, $message);
            $this->Status = self::STAT_INVALID;

            return $this;
        }

        /**
         * Make new instance with status of good
         * @param mixed $iData
         * @param string $iMessage
         * @return \Score\Data\Set
         */
        public static function MakeGood($iData = null, $iMessage = null)
        {
            $retRes = new self();
            return $retRes->SetGood($iData, $iMessage);
        }

        /**
         * Make new instance with status of error
         * @param string $iMessage
         * @param mixed $iData
         * @return \Score\Data\Set
         */
        public static function MakeError($iMessage, $iData = null)
        {
            $retRes = new self();
            return $retRes->SetError($iMessage, $iData);
        }

        /**
         * Make new instance with status of Warning
         * @param string $iMessage
         * @param mixed $iData
         * @return \Score\Data\Set
         */
        public static function MakeWarning($iMessage, $iData = null)
        {
            $retRes = new self();
            return $retRes->SetWarning($iMessage, $iData);
        }

        /**
         * Make new instance with status of NoData
         * @param string $iMessage
         * @param mixed $iData
         * @return \Score\Data\Set
         */
        public static function MakeNoData($iMessage = null, $iData = null)
        {
            $retRes = new self();
            return $retRes->SetNoData($iMessage, $iData);
        }

        /**
         * Make new instance with status of Invalid
         * @param string $iMessage
         * @param mixed $iData
         * @return \Score\Data\Set
         */
        public static function MakeInvalid($iMessage = null, $iData = null)
        {
            $retRes = new self();
            return $retRes->SetInvalid($iMessage, $iData);
        }
    }
}
