<?php

namespace Score\Data
{
    use \Score\Data\ParseCSV;

    /**
     * A DataTable like class to iterate data with rows of columns
     * @author Ron Rebennack
     * @author Bradley Worrell-Smith
     * @todo Need more data validation and control for Data and Headers
     */
    class Table implements \SeekableIterator, \Countable, \ArrayAccess
    {
        const EXCEPTION_MISSINGCOL = "(100401) Score\Data\Table: Missing required column(s)";
        const EXCEPTION_MISMATCH = "(100402) Score\Data\Table: Row imported doesn't match header info";
        const EXCEPTION_NETNOINFO = "(100403) Score\Data\Table: .Net DataTable Missing 'serverInfo' starter";
        const EXCEPTION_NETNOCOLS = "(100404) Score\Data\Table: .Net DataTable Missing Column Names";
        const EXCEPTION_NETNODATA = "(100405) Score\Data\Table: .Net DataTable Missing Data Contents";
        const EXCEPTION_BADDATA = "(100406) Score\Data\Table: Invalid data assigned";
        const EXCEPTION_BADROWOBJ = "(100407) Score\Data\Table: Invalid data row object handler";
        const EXCEPTION_BADOFFSET = "(100408) Score\Data\Table: Invalid offset type";
        const EXCEPTION_BADCOL = "(100409) Score\Data\Table: Invalid column";

        /**
         * Index Update Types
         */
        const IDXUT_REMOVE = -1;
        const IDXUT_ADD = 1;

        /**
         * Array or rows storing columns of data
         * @var array
         */
        protected $_data = array();

        /**
         * Array of header names
         * @var array
         */
        protected $_headers = array();

        /**
         * Current position in the array
         * @var int
         */
        protected $_position = null;

        /**
         * Indexes for data to allow filters and finds to speed up. Each Index contains keys of data with values or row numbers
         * @var array
         */
        protected $_indices = array();

        /**
         * Store Status of rows that are set and unset
         * @var array 
         */
        protected $_status = array("new" => array(), "set" => array(), "unset" => array());

        /**
         * Rewind the Iterator to the first element
         */
        public function rewind()
        {
            $this->_position = 0;
            reset($this->_data);
        }

        /**
         * Return the current element
         * @return type
         */
        public function current()
        {
            $item = current($this->_data);

            if ( $item !== false && is_array($item) )
            {
                return array_combine($this->_headers, $item);
            }

            return false;
        }

        /**
         * Return the key of the current element
         * @return scalar scalar on success, integer 0 on failure.
         */
        public function key()
        {
            return $this->_position;
        }

        /**
         * Is this the first record when traversing array
         * @return bool
         */
        public function isFirst()
        {
            if ( empty($this->_data) )
            {
                return null;
            }

            return ($this->_position == 0);
        }

        /**
         * Is this the last record when traversing array
         * @return bool
         */
        public function isLast()
        {
            if ( empty($this->_data) )
            {
                return null;
            }

            return ($this->_position == (count($this->_data) - 1));
        }

        /**
         * Move forward to next element
         * @return void Any returned value is ignored.
         */
        public function next()
        {
            $item = next($this->_data);

            if ( $item !== false && is_array($item) )
            {
                ++$this->_position;

                return array_combine($this->_headers, $item);
            }

            return false;
        }

        /**
         * Is the current position valid
         * @return void Any returned value is ignored.
         */
        public function valid()
        {
            return ($this->current() !== false);
        }

        /**
         * Move backward to previous element
         * @return bool Any returned value is ignored.
         */
        public function prev()
        {
            $item = prev($this->_data);

            if ( $item === false )
            {
                $item = end($this->_data);
            }

            if ( $item !== false && is_array($item) )
            {
                --$this->_position;

                return array_combine($this->_headers, $item);
            }

            return false;
        }

        /**
         * Traverse to the last record and return
         * @return mixed Last record in the Data Table or false if no record
         */
        public function end()
        {
            $item = end($this->_data);
            $this->_position = count($this->_data) - 1;

            if ( $item !== false && is_array($item) )
            {
                return array_combine($this->_headers, $item);
            }

            return false;
        }

        /**
         * Seek the records to the position given
         * @param int $iPosition Row number
         * @return mixed Record at given position or false if no record
         */
        public function seek($iPosition)
        {
            reset($this->_data);

            if ( $iPosition > (count($this->_data) - 1) || $iPosition < 0 )
            {
                return false;
            }

            $this->_position = $iPosition;
            return $this->_position;
        }

        /**
         * Find the first record that matches the given criteria
         * @param array $iFind       Associative array matching name and value
         * @param bool  $iSetPosition Set the seek position to the row if found, otherwise rewind
         * @param bool  $iGetRow      Return the full row of the first found record
         * @return mixed
         */
        public function findFirst($iFind, $iSetPosition = false, $iGetRow = false)
        {
            $xIdx = 0;
            $xKeys = array();
            $xRowNum = false;
            $xRowData = null;

            foreach ( array_keys($iFind) as $xName )
            {
                if ( array_key_exists($xName, $this->_indices) )
                {
                    $xIdx++;
                }

                $xNum = array_search($xName, $this->_headers);

                if ( $xNum === false )
                {
                    return false;
                }

                $xKeys[$xName] = $xNum;
            }

            $this->rewind();

            if ( $xIdx == count($iFind) )
            {
                $all = array();
                $intersect = false;

                foreach ( $iFind as $xName => $xVal )
                {
                    if (!isset($this->_indices[$xName][$xVal]))
                    {
                        break;
                    }

                    $all[] = $this->_indices[$xName][$xVal];
                }

                if ( count($all) == 1 )
                {
                    $intersect = $all[0];
                }
                elseif (count($all) > 1)
                {
                    $intersect = call_user_func_array('array_intersect', $all);
                }

                if ( !empty($intersect) )
                {
                    $xRowNum = $intersect[0];
                    $xRowData = $this->_data[$xRowNum];
                }
            }
            else
            {
                foreach ( $this->_data as $xNum => $xData )
                {
                    $xFound = 0;

                    foreach ( $iFind as $xName => $xVal )
                    {
                        if ( $xData[$xKeys[$xName]] == $xVal )
                        {
                            $xFound++;
                        }
                    }

                    if ( $xFound == count($iFind) )
                    {
                        $xRowNum = $xNum;
                        $xRowData = $xData;
                        break;
                    }
                }
            }

            if ( $iSetPosition && $xRowNum !== false)
            {
                $this->seek($xRowNum);
            }
            else
            {
                $this->rewind();
            }

            if ( $iGetRow && $xRowNum !== false )
            {
                return array_combine($this->_headers, $xRowData);
            }

            return $xRowNum;
        }

        /**
         * Get the headers for the table
         * @return array
         */
        public function getHeaders()
        {
            return $this->_headers;
        }

        /**
         * Check the column headers to see if the Data Table has all the required ones
         * @param type $iNeeded
         * @param type $iThrowException  Throw an Exception
         * @return bool Did it check out okay?
         */
        public function headerCheck($iNeeded, $iThrowException = false)
        {
            $okay = true;

            if ( !is_array($iNeeded) )
            {
                $iNeeded = array($iNeeded);
            }

            foreach ( $iNeeded as $key )
            {
                if ( !in_array($key, $this->_headers) )
                {
                    $okay = false;
                    break;
                }
            }

            if ( $iThrowException && !$okay )
            {
                throw new \Score\Exception(self::EXCEPTION_MISSINGCOL);
            }

            return $okay;
        }

        /**
         * Get the Data
         * @param int $iOffset
         * @return array
         */
        public function getData($iOffset = null)
        {
            if ( is_null($iOffset) || !is_int($iOffset) )
            {
                return $this->_data;
            }

            return $this->offsetGet($iOffset);
        }

        /**
         * Set a row's data
         * @param array $iArray
         * @param int $iOffset
         * @throws \Exception
         */
        public function setData(array $iArray, $iOffset = null)
        {
            if (is_null($iOffset))
            {
                $iOffset = $this->key();
            }

            if (!is_int($iOffset))
            {
                throw new \Score\Exception(self::EXCEPTION_BADOFFSET);
            }

            $iOffset = (int)$iOffset;
            $this->offsetSet($iOffset, $iArray);
        }

        /**
         * Add a new row from the given position. Ex: $dtCodes[] = array("abc", "123")
         * @param int $offset  Position
         * @param array $value Array of column values
         * @return void
         * @throws Score\Exception
         */
        public function offsetSet($offset, $value)
        {
            if ( !(is_array($value) || $value instanceof ArrayAccess) )
            {
                throw new \Score\Exception(self::EXCEPTION_BADDATA);
            }

            $isNew = true;
            $new = $this->getNewRow();
            $new = array_values($new);

            if ( is_int($offset) && isset($this->_data[$offset]) )
            {
                $new = $this->_data[$offset];
                $isNew = false;
            }

            $match = false;

            foreach ( $value as $key => $val )
            {
                if ( is_int($key) )
                {
                    continue;
                }

                if (($pos = array_search($key, $this->_headers)) !== false)
                {
                    $match = true;
                    $new[$pos] = $val;
                }
            }

            if ( !$match )
            {
                throw new \Score\Exception(self::EXCEPTION_MISMATCH);
            }

            if ( is_null($offset) )
            {
                $offset = count($this->_data);
            }
            else
            {
                $this->_updateIndexRow($offset, null, null, self::IDXUT_REMOVE);
            }

            $this->_status[($isNew ? "new" : "set")][] = array("offset" => $offset);
            $this->_data[$offset] = $new;
            $this->_updateIndexRow($offset);
        }

        /**
         * Whether a offset exists
         * @param mixed $offset The Position number
         * @return boolean Returns true on success or false on failure.
         */
        public function offsetExists($offset)
        {
            return isset($this->_data[$offset]);
        }

        /**
         * Remove the record of the given position Ex: unset($dtCodes[3])
         * @param type $offset The Position number
         */
        public function offsetUnset($offset)
        {
            $this->_status["unset"][] = array("offset" => $offset, "data" => $this->_data[$offset]);
            unset($this->_data[$offset]);
        }

        /**
         * Offset to retrieve
         * @param mixed $offset The Position number
         * @return mixed Can return all value types.
         */
        public function offsetGet($offset)
        {
            return isset($this->_data[$offset]) ? array_combine($this->_headers, $this->_data[$offset]) : null;
        }

        /**
         * Get the number of rows, Impliments Countable. Can be used with count. Ex: count($dtCodes)
         * @return int Number of rows
         */
        public function count()
        {
            return count($this->_data);
        }

        /**
         * DEPREACIATED: Return numer of rows.
         * @return type
         */
        public function totalRows()
        {
            return $this->count();
        }

        /**
         * Add an Index
         * @param string $iColumn
         * @return void
         * @throws \Exception
         */
        public function addIndex($iColumn)
        {
            if ( !is_string($iColumn) )
            {
                throw new \Score\Exception(self::EXCEPTION_BADCOL);
            }

            if ( array_key_exists($iColumn, $this->_indices) )
            {
                return;
            }

            $this->_indices[$iColumn] = array();
            $this->updateIndex($iColumn);
        }

        /**
         * Remove Index
         * @param string $iColumn
         * @return void
         * @throws \Exception
         */
        public function delIndex($iColumn = null)
        {
            if (is_null($iColumn))
            {
                $this->_indices = array();
                return;
            }

            if (!is_string($iColumn))
            {
                throw new \Score\Exception(self::EXCEPTION_BADCOL);
            }

            if (!array_key_exists($iColumn, $this->_indices))
            {
                return;
            }

            unset($this->_indices[$iColumn]);
        }

        /**
         * Get the Index Data
         * @param string $iColumn Index Name/Column
         * @return array
         */
        public function getIndexData($iColumn = null)
        {
            if ( empty($iColumn) )
            {
                return $this->_indices;
            }

            return $this->_indices[$iColumn];
        }

        /**
         * Update index information for specified column
         * @param string $iColumn
         * @throws \Exception
         */
        public function updateIndex($iColumn = null)
        {
            if ( empty($iColumn) )
            {
                $iColumn = array_keys($this->_indices);
            }

            if ( !is_array($iColumn) )
            {
                $iColumn = array($iColumn);
            }

            foreach ( $iColumn as $currCol )
            {
                if ( !is_string($currCol) )
                {
                    throw new \Exception(self::EXCEPTION_BADCOL);
                }

                $this->_indices[$currCol] = array();
            }

            foreach ( $this->_data as $num => $row )
            {
                $this->_updateIndexRow($num, $iColumn, $row);
            }
        }

        /**
         * Update Index information based on a specific Row
         * @param int $iRowNum
         * @param string $iColumn
         * @param array $iRowData
         * @param int $iUpdateType
         */
        protected function _updateIndexRow($iRowNum, $iColumn = null, $iRowData = null, $iUpdateType = self::IDXUT_ADD)
        {
            if ( empty($iColumn) )
            {
                $iColumn = array_keys($this->_indices);
            }

            if ( !is_array($iColumn) )
            {
                $iColumn = array($iColumn);
            }

            foreach ( $iColumn as $currCol )
            {
                $cell = null;
                $colNum = array_search($currCol, $this->_headers);

                if ( empty($iRowData) )
                {
                    $cell = $this->_data[$iRowNum][$colNum];
                }
                else
                {
                    $cell = $iRowData[$colNum];
                }

                if ( !array_key_exists($cell, $this->_indices[$currCol]) )
                {
                    $this->_indices[$currCol][$cell] = array();
                }

                if ( $iUpdateType == self::IDXUT_REMOVE )
                {
                    $key = array_search($iRowNum, $this->_indices[$currCol][$cell]);

                    if ( $key !== false )
                    {
                        unset($this->_indices[$currCol][$cell][$key]);
                    }
                }

                if ( $iUpdateType == self::IDXUT_ADD )
                {
                    $this->_indices[$currCol][$cell][] = $iRowNum;

                    // Lets assume that the data is being manipulated directly and re-sort
                    sort($this->_indices[$currCol][$cell]);
                }
            }
        }

        /**
         * Default blank array with keys from header
         * @return array
         */
        public function getNewRow()
        {
            $new = array();

            foreach ( $this->_headers as $key )
            {
                $new[$key] = null;
            }

            return $new;
        }

        /**
         * Load contents by passing arrays of infromation
         * @param array $iDataArray Array of rows. If $iHeaders is not pass, assumes first row is $iHeaders
         * @param array $iHeaders   Array of column headers that should match the Data Rows
         * @return void
         */
        public function loadByArray($iDataArray = null, $iHeaders = null)
        {
            if ( !empty($iDataArray) )
            {
                if ( !is_array($iDataArray[0]) )
                {
                    throw new \Exception(self::EXCEPTION_BADDATA);
                }

                if ( is_array($iDataArray) && empty($iHeaders) )
                {
                    $this->_headers = array_keys($iDataArray[0]);
                }
                elseif ( is_array($iDataArray) && is_array($iHeaders) )
                {
                    $this->_headers = $iHeaders;
                }

                foreach ( $iDataArray as $row )
                {
                    $this->_data[] = array_values($row);
                }

                $this->updateIndex();
            }
        }

        /**
         * Load the contents of a CSV file into the Data Table
         * @param type $iFile       Location of the file to import
         * @param type $iDelimiter  What seperates each field
         * @param type $iQualifier  For Text Fields to be enapsulated by
         * @param type $iQuantifier Abosution or escape for Qualifiers? Scenario: "My Name is \"Brad\""
         * @return void
         */
        public function loadByCSV($iFile, $iDelimiter = ",", $iQualifier = '"', $iQuantifier = '\\', $iNullString = null)
        {
            if ( !file_exists($iFile) )
            {
                return;
            }

            $parser = new ParseCSV($iFile, $iDelimiter, $iQualifier, $iQuantifier, ParseCSV::MODE_ONTHEFLY, $iNullString);

            foreach ( $parser as $row )
            {
                $cnt++;
                if ( empty($this->_headers) )
                {
                    $this->_headers = $row;
                    continue;
                }

                if ( count($row) != count($this->_headers) )
                {
                    throw new \Score\Exception(self::EXCEPTION_MISMATCH);
                }

                $this->_data[] = $row;
            }

            $this->updateIndex();
        }

        /**
         * Take and object created by DotNet DataTable and translate to local Data Table set
         * @param mixed $iDataTable Object to convert/load
         */
        public function loadByDotNetTable($iDataTable)
        {
            if ( is_object($iDataTable) )
            {
                $iDataTable = get_object_vars($iDataTable);
            }

            if ( empty($iDataTable["serverInfo"]) )
            {
                throw new \Score\Exception(self::EXCEPTION_NETNOINFO);
            }

            $iDataTable = get_object_vars($iDataTable["serverInfo"]);

            $this->_headers = $iDataTable["columnNames"];
            if ( empty($this->_headers) )
            {
                throw new \Score\Exception(self::EXCEPTION_NETNOCOLS);
            }

            $this->_data = $iDataTable["initialData"];
            if ( empty($this->_data) )
            {
                throw new \Score\Exception(self::EXCEPTION_NETNODATA);
            }

            $this->updateIndex();
        }
    }
}
