<?php

namespace Score\Data
{
    use Score\Helper;

    /**
     * Parse PHP Class using reflection
     *
     * @author Bradley Worrell-Smith
     */
    class ParseClass
    {
        const EXCEPTION_BADCLASS = "(100420) Invalid Class Specified";

        public $data;

        public function __construct($iClass)
        {
            $xFile = null;

            if (is_object($iClass))
            {
                $xReflect = new \ReflectionClass($iClass);
                $xFile = $xReflect->getFileName();
            }

            if (is_string($iClass))
                $xFile = $iClass;

            if (!is_file($xFile))
                throw new \Score\Exception(self::EXCEPTION_BADCLASS);

            $xFile = file_get_contents($xFile);

            $this->data = self::ParseIt($xFile);
        }

        public static function getDocFromComment($iComment)
        {
            $xData = trim(preg_replace('%(^\s*?/\*\*|^\s*?\*/|^\s*?\*\s)%smx', ' ', $iComment));

            $xOff = 0;
            $xDoc = array();
            $xMatches = null;

            if (preg_match('/\A(?P<desc>.*?)(?P<end>@|\Z)/smx', $xData, $xMatches))
                $xDoc[] = array("tag" => "description", "data" => rtrim($xMatches["desc"]));

            while (preg_match('%(@(?P<tag>[a-z]+)\s+(?P<data>.*?)\s*(?=$|@[a-z]+\s)|<code>(?P<code>.*?)</code>)%smx', $xData, $xMatches, PREG_OFFSET_CAPTURE, $xOff) !== false)
            {
                if (empty($xMatches))
                    break;

                if (array_key_exists("code", $xMatches))
                {
                    $xDoc[] = array("tag" => "code", "data" => $xMatches["code"][0]);
                    $xOff = $xMatches["code"][1]++;
                }

                if (array_key_exists("tag", $xMatches))
                {
                    $xDoc[] = array("tag" => $xMatches["tag"][0], "data" => $xMatches["data"][0]);
                    $xOff = $xMatches["data"][1]++;
                }
            }

            return $xDoc;
        }
        
        public static function ParseIt($iPHPText, $iParseDocBlock = true)
        {
            if (empty($iPHPText))
                return false;

            $oData = array();
            $xTokens = null;

            try
            {
                $xTokens = token_get_all($iPHPText);
            }
            catch (\Exception $err)
            {
                return false;
            }

            $xCap = array(
                T_NAMESPACE => array("name" => "namespace", "cap" => array(T_STRING, T_NS_SEPARATOR), "list" => false, "data" => null, "active" => false),
                T_CLASS => array("name" => "class", "cap" => array(T_STRING), "list" => false, "data" => null, "active" => false),
                T_IMPLEMENTS => array("name" => "implements", "cap" => array(T_STRING, T_NS_SEPARATOR, ","), "list" => true, "data" => null, "active" => false),
                T_VARIABLE => array("name" => "variables", "cap" => array(T_VARIABLE), "list" => false, "data" => null, "active" => false),
            );

            foreach ($xTokens as &$xItem)
            {
                if (!is_array($xItem))
                    $xItem = array($xItem, $xItem, null);

                if (is_numeric($xItem[0]))
                    $xItem["token"] = token_name($xItem[0]);

                echo(var_export($xItem, true)."\n");
                $xStop = false;

                if (array_key_exists($xItem[0], $xCap))
                    $xStop = true;

                foreach ($xCap as $xT => &$xCI)
                {
                    if ($xCI["active"])
                    {
                        if (array_search($xItem[0], $xCI["cap"]) !== false && !$xStop)
                            $xCI["data"] .= $xItem[1];
                        else if (is_null($xItem[2]) || $xStop)
                        {
                            if ($xCI["list"] === true)
                                $xCI["data"] = array_filter(array_map('trim', explode(",", $xCI["data"])));
                            else
                                $xCI["data"] = trim($xCI["data"]);

                            Helper::StackValue($oData, $xCI["data"], $xCI["name"]);
//                                $oData[$xCI["name"]] = trim($xCI["data"]);

                            $xCI["data"] = null;
                            $xCI["active"] = false;

                            continue;
                        }
                    }

                    if ($xT == $xItem[0])
                    {
                        $xCI["active"] = true;

                        if (array_search($xItem[0], $xCI["cap"]) !== false)
                            $xCI["data"] .= $xItem[1];

                        continue;
                    }
                }
            }

            return $oData;
        }

        public static function ReflectIt($iClass, $iParseDocBlock = true)
        {
            if (empty($iPHPText))
                return false;

            $oData = array();
            $xReflect = new \ReflectionClass($iClass);
            $oData["class"] = $xReflect->getName();
            $oData["comment"] = $xReflect->getDocComment();
            $oData["constants"] = $xReflect->getConstants();
            $oData["interfaces"] = array();
            $oData["methods"] = array();

            if ($iParseDocBlock)
                $oData["docblock"] = self::getDocFromComment($xReflect->getDocComment());

            foreach ($xReflect->getInterfaces() as $xName => $xItem)
            {
                $oData["interfaces"][] = $xName;
            }

            foreach ($xReflect->getMethods() as $xItem)
            {
                $xDetail = array();
                $xDetail["name"] = $xItem->name;
                $xDetail["static"] = $xItem->isStatic();
                $xDetail["scope"] = "public";
                $xDetail["comment"] = $xItem->getDocComment();

                if ($iParseDocBlock)
                    $xDetail["docblock"] = self::getDocFromComment($xItem->getDocComment());

                if ($xItem->isProtected())
                    $xDetail["scope"] = "protected";
                if ($xItem->isPrivate())
                    $xDetail["scope"] = "private";

                $oData["methods"][] = $xDetail;
            }

            return $oData;
        }

        public function __toString()
        {
            return var_export($this->data, true);
        }
    }
}