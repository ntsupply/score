<?php

namespace Score
{
    /**
     * Logger Class for Score Framework
     * @author Bradley Worrell-Smith
     * @todo Add DB Logger option
     * @todo Add Formatter log message
     */
    class Logger
    {
        /**
         * Stack of Loggers
         * All references to _LoggerStack for now are using 'Logger' and not 'self' so that if inheritied and
         * customized, everything falls under 1 stack. This goes for HandleError, HandleException, HandleFatal!
         * @staticvar array _LoggerStack
         */
        public static $_LoggerStack;

        protected $_file = null;
        protected $_errMsg = null;
        protected $_level = null;
        protected $_types = null;
        protected $_cache = "";
        protected $_mt = false;

        const TYPE_ERROR = "E";
        const TYPE_WARN = "W";
        const TYPE_INFO = "I";
        const TYPE_VERBOSE = "V";

        const LT_ADD = 1;
        const LT_DEL = -1;
        const LT_SET = 0;

        const LISTEN_SCOPE = 1;
        const LISTEN_EXCEPTION = 2;
        const LISTEN_ERROR = 3;

        /**
         * Start a new instance of CoreLogger
         * @param string $fileName Path to log file
         * @param int    $logLevel Level # to log events
         * @param type $fileName
         * @param type $logLevel
         * @param type $logTypes
         * @param type $useMicroTime
         * @return type
         */
        public function __construct($fileName = null, $logLevel = null, $logTypes = null, $useMicroTime = false)
        {
            if (empty($logTypes))
            {
                $logTypes = array(self::TYPE_ERROR, self::TYPE_WARN, self::TYPE_INFO);
            }

            $this->_mt = $useMicroTime;
            $this->_file = $fileName;
            $this->LogLevel($logLevel);
            $this->LogType($logTypes, self::LT_SET);
            
            if (!file_exists($fileName))
            {
                if (!is_writeable(dirname($fileName)))
                {
                    $this->_file = null;
                    $this->_errMsg = "Unable to write to " . dirname($fileName);
                    return;
                }

                touch($fileName);
            }

            if (!is_writeable($fileName))
            {
                $this->_file = null;
                $this->_errMsg = "Unable to write to $fileName";
                return;
            }

            if (!is_array(Logger::$_LoggerStack))
            {
                Logger::$_LoggerStack = array();

                register_shutdown_function(__CLASS__ . "::HandleFatal");
                set_error_handler(__CLASS__ . "::HandleError", E_ALL & E_WARNING);

                if (version_compare(PHP_VERSION, '7.0.0') >= 0)
                {
                    set_exception_handler(__CLASS__ . "::HandleThrowable");
                }
                else
                {
                    set_exception_handler(__CLASS__ . "::HandleException");
                }
            }
        }

        /**
         * Used to remove any instances from the global stack
         */
        public function __destruct()
        {
            foreach (Logger::$_LoggerStack as $key => $logItem);
            {
                if ($this === $logItem["logger"])
                {
                    unset(Logger::$_LoggerStack[$key]);
                }
            }
        }

        public function LogLevel($level = null)
        {
            if (!is_null($level))
            {
                $this->_level = $level;
            }

            return $this->_level;
        }

        public function LogType($type, $mode = self::LT_ADD)
        {
            if (!is_array($type))
            {
                $type = array($type);
            }

            if ($mode === self::LT_SET)
            {
                $this->_types = $type;
            }

            if ($mode === self::LT_ADD)
            {
                $this->_types = array_unique(array_merge($this->_types, $type));
            }

            if ($mode === self::LT_DEL)
            {
                $this->_types = array_diff($base, array("E", "I"));
            }
        }

        /**
         * Convert Error Number to Enum/Constant
         * @author russthom at fivegulf dot com
         * @param int $errno
         * @return string
         */
        public static function getErrorConst($errno)
        {
            switch($errno)
            {
                case E_ERROR: // 1 //
                    return 'E_ERROR';
                case E_WARNING: // 2 //
                    return 'E_WARNING';
                case E_PARSE: // 4 //
                    return 'E_PARSE';
                case E_NOTICE: // 8 //
                    return 'E_NOTICE';
                case E_CORE_ERROR: // 16 //
                    return 'E_CORE_ERROR';
                case E_CORE_WARNING: // 32 //
                    return 'E_CORE_WARNING';
                case E_CORE_ERROR: // 64 //
                    return 'E_COMPILE_ERROR';
                case E_CORE_WARNING: // 128 //
                    return 'E_COMPILE_WARNING';
                case E_USER_ERROR: // 256 //
                    return 'E_USER_ERROR';
                case E_USER_WARNING: // 512 //
                    return 'E_USER_WARNING';
                case E_USER_NOTICE: // 1024 //
                    return 'E_USER_NOTICE';
                case E_STRICT: // 2048 //
                    return 'E_STRICT';
                case E_RECOVERABLE_ERROR: // 4096 //
                    return 'E_RECOVERABLE_ERROR';
                case E_DEPRECATED: // 8192 //
                    return 'E_DEPRECATED';
                case E_USER_DEPRECATED: // 16384 //
                    return 'E_USER_DEPRECATED';
            }

            return "E_UNKOWN";
        }

        /**
         * Handles Fatal Shutdown Errors from register_shutdown_function
         */
        public static function HandleFatal()
        {
            $lastErr = error_get_last();
            self::HandleError($lastErr["type"], $lastErr["message"], $lastErr["file"], $lastErr["line"]);
        }

        /**
         * Handles General PHP Errors from set_error_handler
         */
        public static function HandleError($errno, $errstr, $errfile, $errline)
        {
            foreach (Logger::$_LoggerStack as $logItem)
            {
                if ($logItem["type"] != self::LISTEN_ERROR || ($errno & $logItem["listen"]) == 0)
                {
                    continue;
                }

                $logItem["logger"]->WriteLog(self::getErrorConst($errno), " $errline of $errfile\n$errstr", self::TYPE_ERROR);
            }
        }

        /**
         * Handle Exceptions from set_exception_handler
         */
        public static function HandleException(\Exception $exception)
        {
            foreach (Logger::$_LoggerStack as $logItem)
            {
                if ($logItem["type"] != self::LISTEN_EXCEPTION)
                {
                    continue;
                }

                $instance =& $logItem["logger"];
                $instance->WriteLog("EXCEPTION", sprintf("%d of %s\n%s", $exception->getLine(), $exception->getFile(), $exception->getMessage()), self::TYPE_ERROR);
            }
        }

        /**
         * Handle Exceptions from set_exception_handler
         */
        public static function HandleThrowable(\Throwable $thrown)
        {
            foreach (Logger::$_LoggerStack as $logItem)
            {
                if ($logItem["type"] != self::LISTEN_EXCEPTION)
                {
                    continue;
                }

                $instance =& $logItem["logger"];
                $instance->WriteLog("THROWABLE", sprintf("%d of %s\n%s", $thrown->getLine(), $thrown->getFile(), $thrown->getMessage()), self::TYPE_ERROR);
            }
        }

        /**
         * Get any error messages by the logger
         * @return string
         */
        public function getErrMsg()
        {
            return $this->_errMsg;
        }

        /**
         * (DEPRECIATED) Register to Global Logger stack
         * Use ListenFor()
         * @param string $iListenFor  String Name to listen for in Scope
         */
        public function register($iListenFor = null)
        {
            $this->ListenFor(self::LISTEN_SCOPE, $iListenFor);
        }

        /**
         * Register to Global Logger stack
         * Different types of logging to listen for
         * - Logger::LISTEN_SCOPE - Used for namespace and classes that call ByGlobal within their scope
         *   Registered Loggers will can catch calls from Log Events like Score\Logger::ByGlobal("UniqueName", "Yadda Message")
         *   @example $Logger->ListenFor(Logger::LISTEN_SCOPE, "Score\Data\Table"); // Specific Listener
         *   @example $Logger->ListenFor(Logger::LISTEN_SCOPE, "Score\Data\"); // Everything under Score\Data
         *   @example $Logger->ListenFor(Logger::LISTEN_SCOPE, "Score\Data\Table", true); // Remove Specific Listener
         *
         * - Logger::LISTEN_EXCEPTION - Watch and log specific exceptions.  By default Score\Exception is registered.
         *   @example $Logger->ListenFor(Logger::LISTEN_EXCEPTION); // Listen for any general \Exception
         *
         * - Logger::LISTEN_ERROR - Log general non-exception errors from PHP
         *   @example $Logger->ListenFor(Logger::LISTEN_ERROR, E_ERROR);
         *
         * @param int $iType
         * @param string $iOpt  String Name to listen for
         * @param bool $iRemove  Remove the given item from the logger stack
         */
        public function ListenFor($iType = self::LISTEN_SCOPE, $iOpt = null, $iRemove = false)
        {
            // Check for duplicates and handle if being removed
            if ( !empty(self::$_LoggerStack) )
            {
                foreach (Logger::$_LoggerStack as $key => $item)
                {
                    if ($item["logger"] != $this || $item["type"] != $iType)
                    {
                        continue;
                    }

                    if ($iType != self::LISTEN_ERROR && $item["listen"] != $iOpt)
                    {
                        continue;
                    }

                    unset(Logger::$_LoggerStack[$key]);
                }
            }

            if ($iRemove)
            {
                return;
            }

            if ($iType == self::LISTEN_ERROR)
            {
                $iOpt = (empty($iOpt) ? E_ALL : (int)$iOpt);
            }
            else
            {
                $iOpt = (string)$iOpt;
            }

            $newLogger = array();
            $newLogger["logger"] = &$this;
            $newLogger["type"] = $iType;
            $newLogger["listen"] = $iOpt;

            Logger::$_LoggerStack[] = $newLogger;
        }

        /**
         * Quickly Write to log and auto detect Message type
         * @param mixed $iMessage String, Number, or Object
         * @return null
         */
        public function Q($iMessage)
        {
            if (!is_string($iMessage) && !is_numeric($iMessage))
            {
                $iMessage = self::PrintVar($iMessage);
            }

            $this->WriteLog("DEBUG", $iMessage, "I");
        }

        /**
         * (DEPRECIATED) Log by level number.  It is recommded to use a logType on
         * the constructor and WriteLog respectivly.
         * @param string $iMessage     Message to be logged
         * @param int    $iLevel       Level numer to log
         * @param string $iMessageType Message Types TYPE_INFO, TYPE_WARN, TYPE_ERROR
         * @return type
         */
        public function ByLevel($iMessage, $iLevel = 1, $iMessageType = self::TYPE_INFO)
        {
            if (is_numeric($this->_level) && $this->_level >= $iLevel)
            {
                $this->WriteLog("LVL$iLevel", $iMessage, $iMessageType);
            }
        }

        /**
         * Call Gloabl Logging info
         *
         * @param string $iUniqueName  Unique Name or Class Path to Log info
         * @param string $iMessage     Message to be logged
         * @param string $iMessageType Message Types TYPE_INFO, TYPE_WARN, TYPE_ERROR
         * @static
         * @example Score\Logger::ByGlobal("Score\Data\Table", "Yadda Message");
         */
        public static function ByGlobal($iUniqueName, $iMessage, $iMessageType = self::TYPE_INFO)
        {
            if (!is_array(Logger::$_LoggerStack))
            {
                return;
            }

            foreach (Logger::$_LoggerStack as $logItem)
            {
                if ($logItem["type"] != self::LISTEN_SCOPE)
                {
                    continue;
                }

                $xListen = $logItem["listen"];

                if (!is_null($logItem) && strlen($xListen) > 0 && $xListen != substr($iUniqueName, 0, strlen($xListen)))
                {
                    continue;
                }

                $xLogger = $logItem["logger"];
                $xLogger->WriteLog($iUniqueName, $iMessage, $iMessageType);
            }
        }

        /**
         * Call WriteLog to every logger in the Gloabl stack.  This is used when something
         * super important needs to be logged even though you are not specficially listening for it.
         *
         * @param string $iFlag        Unique Name or Class Path to Log info
         * @param string $iMessage     Message to be logged
         * @param string $iMessageType Message Types TYPE_INFO, TYPE_WARN, TYPE_ERROR
         * @example Score\Logger::WriteStack("Score\Data\Table", "Yadda Message");
         */
        public static function WriteStack($iFlag, $iMessage, $iMessageType = self::TYPE_INFO)
        {
            if (!is_array(Logger::$_LoggerStack))
            {
                return;
            }

            foreach (Logger::$_LoggerStack as $logItem)
            {
                $xLogger = $logItem["logger"];
                $xLogger->WriteLog($iFlag, $iMessage, $iMessageType);
            }
        }

        /**
         * Create a new instance and write a quick message to the logfile
         * @param string $iLogFile Path to logfile
         * @param mixed $iMessage String, Number, or Object
         * @return null
         */
        public static function WriteToLog($iLogFile, $iMessage, $iMessageType = self::TYPE_INFO)
        {
            $xLogger = new self($iLogFile);
            $xLogger->WriteLog("DEBUG", $iMessage, $iMessageType);
            return $xLogger;
        }

        /**
         * Writes Logging information
         * @param string $iFlag Flag or category for logging
         * @param string $iMessage Message to be logged
         * @param string $iMessageType Message Types TYPE_INFO, TYPE_WARN, TYPE_ERROR
         * @return type null
         */
        public function WriteLog($iFlag, $iMessage = "", $iMessageType = self::TYPE_INFO)
        {
            $lines = array();

            if (!empty($this->_types) && array_search($iMessageType, $this->_types) === false)
            {
                return;
            }

            if (is_string($iMessage))
            {
                $lines = explode("\n", $iMessage);
            }
            else
            {
                $lines[] = self::PrintVar($iMessage);
            }

            switch ($iMessageType)
            {
                case self::TYPE_ERROR:
                    $type = "Erro";
        
                    foreach (array_reverse(debug_backtrace()) as $trace)
                    {
                        if (empty($trace["file"]) || $trace["function"] == "WriteLog" || $trace["function"] == "__construct")
                        {
                            continue;
                        }

                        $xDude = $trace["file"];

                        if (!empty($trace["function"]))
                        {
                            $xDude .= sprintf(" called '%s.%s'", (array_key_exists('class', $trace) ? $trace['class'] : ""), $trace["function"]);
                        }

                        // Lets let the profiler know the error
                        $snap = new \Score\Profiler\Snap($iFlag, $trace["file"] . "@" . $trace["line"], \Score\Profiler\Snap::TYPE_ERROR);
                        $snap->annotate(implode("\n", $lines));
                        unset($snap);
    
                        $lines[] = sprintf("\t %s at line: %s", $xDude, $trace["line"]);
                    }

                    break;

                case self::TYPE_WARN:
                    $type = "Warn";
                    break;

                case self::TYPE_VERBOSE:
                    $type = "Verb";

                default:
                    $type = "Info";
                    break;
            }

            if (empty($this->_file))
            {
                $this->_cache .= implode("\n", $lines) . "\n";
                return;
            }
            else
            {
                $fp = fopen($this->_file, "a");
                if (!$fp)
                {
                    return;
                }

                foreach ($lines as $line)
                {
                    if ($this->_mt)
                    {
                        $t = explode(" ", microtime());
                        $dt = date('Y-m-d H:i:s', $t[1]) . substr((string)$t[0],1,4);
                    }
                    else
                    {
                        $dt = date('Y-m-d H:i:s');
                    }

                    fwrite($fp, sprintf("[%s] %s [%s] - %s\n", $type, $dt, $iFlag, $line));
                }

                fclose($fp);
            }
        }

        /**
         * Printable friendly representation of the variable or Object.
         * @param type $iVar
         * @return string
         */
        public static function PrintVar($iVar)
        {
            if (is_string($iVar))
            {
                return ('"' . str_replace(array("\x00", "\x0a", "\x0d", "\x1a", "\x09"), array('\0', '\n', '\r', '\Z', '\t'), $iVar) . '"');
            }

            if (is_bool($iVar))
            {
                return ($iVar ? "true" : "false");
            }

            if (is_array($iVar))
            {
                $result = 'array( ';
                $comma = '';

                foreach ($iVar as $key => $value)
                {
                    $result .= $comma . self::printVar($key) . ' => ' . self::printVar($value);
                    $comma = ', ';
                }

                $result .= ' )';
                return ($result);
            }

            return (var_export($iVar, true));             // anything else, just let php try to print it
        }
    }
}
