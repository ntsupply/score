<?php

namespace Score\Profiler
{
    abstract class RecorderAdapter
    {
        abstract public function record(\Score\Profiler $profiler, \Score\Profiler\Snap $snap);
        public function shutdown() { }
    }
}