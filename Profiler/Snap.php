<?php

namespace Score\Profiler
{
    class Snap
    {
        public $type;
        public $start;
        public $end;
        public $mem;
        public $peak;
        public $label;
        public $caller;
        public $vars;
        public $messages = [];

        const TYPE_SNAP = "snap";
        const TYPE_ERROR = "error";
        const TYPE_SUMMARY = "summary";

        public function __construct($label = null, $caller = null, $type = self::TYPE_SNAP, array $vars = [])
        {
            $this->type = $type;
            $this->label = $label;
            $this->caller = $caller;
            $this->start = microtime(true);
            $this->mem = memory_get_usage();
            $this->vars = $vars;
        }

        public function stop()
        {
            if (is_null($this->end))
            {
                $this->end = microtime(true);
                $this->peak = memory_get_peak_usage();
            }
        }

        public function annotate($msg, array $vars = [], $microtime = null)
        {
            if (empty($microtime))
            {
                $microtime = microtime(true);
            }

            $this->messages[] = [
                "t" => $microtime,
                "m" => $msg,
                "v" => $vars
            ];
        }

        public function __toString()
        {
            return \Score\Strings::EncodeJsonUTF8(get_object_vars($this), \JSON_FORCE_OBJECT);
        }

        public function __destruct()
        {
            $this->stop();
            \Score\Profiler::track($this);
        }

        /**
         * 
         * @param string $label 
         * @param array $vars
         * @return \Score\Profiler\Snap
         */
        public static function q($label, array $vars = [])
        {
            $trace = debug_backtrace()[0];
            $caller = $trace["file"] . "@" . $trace["line"];

            return new self($label, $caller, self::TYPE_SNAP, $vars);
        }
    }
}