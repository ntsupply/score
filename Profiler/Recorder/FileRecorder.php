<?php

namespace Score\Profiler\Recorder
{
    class FileRecorder extends \Score\Profiler\RecorderAdapter
    {
        public $dir;
        public $file;

        public function __construct($dir)
        {
            if (\Score\Server::SetupFilePath($dir, \Score\Server::MK_DIR))
            {
                $this->dir = $dir;
            }
        }

        public function record(\Score\Profiler $profiler, \Score\Profiler\Snap $snap)
        {
            if (!empty($this->dir))
            {
                if (empty($this->file))
                {
                    $file = $this->dir . '/' . $profiler->name . '/' . $profiler->signature . '.json';

                    if (\Score\Server::SetupFilePath($file, \Score\Server::MK_FILE, \Score\Server::DEFAULT_CHMOD, true))
                    {
                        $this->file = $file;
                        file_put_contents($this->file, "[");
                    }
                }

                if (is_writeable($this->file))
                {
                    error_log("$snap,", 3, $this->file); // Stringify Snap object should be json.. then comma for each entry
                }
            }
        }

        public function shutdown(\Score\Profiler $profiler)
        {
            $type = "web";

            if (\Score\Server::IsAjax())
            {
                $type = "ajax";
            }
            elseif (\Score\Console::IsConsole())
            {
                $type = "console";
            }

            $file = $this->dir . '/' . $profiler->key . '.json';

            \Score\Server::SetupFilePath($file, \Score\Server::MK_FILE, \Score\Server::DEFAULT_CHMOD, true);

            $fc = file_get_contents($file);
            $fd = @json_decode($fc);
            $wr = false;

            if (empty($fc) || !$fd || json_last_error() != \JSON_ERROR_NONE)
            {
                $fd = (object)[
                    "profiles" => []
                ];

                $wr = true;
            }

            if (!empty($this->file))
            {
                $recent = (object)[
                    "type" => $type,
                    "id" => basename($this->file, ".json")
                ];
    
                if (is_array($fd->profiles))
                {
                    $fd->profiles = (object)$fd->profiles;
                }

                if (!property_exists($fd->profiles, $profiler->name))
                {
                    $fd->profiles->{$profiler->name} = [];
                }
                else
                {
                    $fd->profiles->{$profiler->name} = array_slice($fd->profiles->{$profiler->name}, -20);
                }
                
                $fd->profiles->{$profiler->name}[] = $recent;
                
                file_put_contents($this->file, "0]", FILE_APPEND);
                $wr = true;
            }

            if ($wr)
            {
                file_put_contents($file, json_encode($fd));
            }
        }
    }
}