<?php 

namespace Score\Profiler
{
    class Summary extends \Score\Profiler\Snap
    {
        public $url;
        public $agent;
        public $mem_limit;
        public $method;
    }
}