<?php 

namespace Score\Helper
{
    class CallableFunction
    {
        public $type;
        public $callback;

        public function __construct(string $type, callable $callable)
        {
            $this->type = $type;
            $this->callback = $callable;
        }

        /**
         * Undocumented function
         *
         * @param string $name -  
         * @param array $options - array of options with hooks 
         * @return CallableFunction
         */
        public static function getCallback(string $name, $options)
        {
            foreach ($options as &$opt)
            {
                if ($opt instanceof \Score\Helper\CallableFunction && $opt->type == $name)
                {
                    return $opt;
                }
            }

            return false;
        }
        
        public function call(...$more)
        {
            return call_user_func_array($this->callback, $more);
        }
    }
}