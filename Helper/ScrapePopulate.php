<?php

namespace Score\Helper
{
    trait ScrapePopulate
    {
        public function _populate(array $data)
        {
            $list = array_filter(get_object_vars($this), function ($key) { return $key[0] !== '_'; }, ARRAY_FILTER_USE_KEY);

            foreach ($data as $key => $val)
            {
                if (array_key_exists($key, $list))
                {
                    $this->$key = $val;
                }
            }

            return true;
        }

        public function _scrape($allowList = null)
        {
            if (is_string($allowList))
            {
                $allowList = explode(",", $allowList);
            }

            if (empty($allowList))
            {
                $allowList = array_keys(get_object_vars($this));
            }

            return \Score\Arrays::FilterKeys(\get_object_vars($this), $allowList);
        }
    }
}
