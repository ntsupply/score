<?php

namespace Score
{
    /**
     * Session helper
     * @todo Add session_save_handler and db save ability
     * @author Bradley Worrell-Smith
     */
    class Session
    {
        const EXCEPTION_DISABLED = "(100901) Sessions are disabled.";

        protected $_md5;
        protected $_namespace;
        protected $_encode;

        /**
         * Constructor
         * @param string $Namespace Name of grouping/tracking of data for site/app
         */
        public function __construct($namespace, $encode = false)
        {
            $this->_md5 = md5($namespace);
            $this->_namespace = $namespace;
            $this->_encode = $encode;

            self::Start();

            if (!isset($_SESSION[$this->_md5]) || !is_array($_SESSION[$this->_md5]))
            {
                $_SESSION[$this->_md5] = array();
            }
        }

        /**
         * Checks to see if session is active.
         * @return boolean
         * @throws \Score\Exception in the event sessions are disabled
         */
        public static function isActive()
        {
            if (strnatcmp(phpversion(),'5.4.0') >= 0)
            {
                if (session_status() == PHP_SESSION_DISABLED)
                {
                    throw new \Score\Exception(self::EXCEPTION_DISABLED);
                }

                if (session_status() === PHP_SESSION_NONE)
                {
                    return false;
                }
            }
            else
            {
                // Pre 5.4 - This isn't perfect, but it should cut down on warnings
                if (session_id() === "")
                {
                    return false;
                }
            }

            return true;
        }

        /**
         * Start tracking session information
         */
        public static function Start()
        {
            if (!self::isActive())
            {
                if (php_sapi_name() == 'cli')
                {
                    $sessDir = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "php_session";
                    \Score\Server::SetupFilePath($sessDir, \Score\Server::MK_DIR, 0775);
                    session_save_path($sessDir);
                }

                if (!isset($_SESSION))
                {
                    session_start();
                }
            }
        }

        /**
         * Destroy all sessions and data
         */
        public static function Destroy()
        {
            if (self::isActive())
            {
                session_unset();
                session_destroy();
                session_write_close();
                setcookie(session_name(),'',0,'/');
                session_regenerate_id(true);
            }
        }

        /**
         * Namespace of Session
         * @return string
         */
        public function getNamespace()
        {
            return $this->_namespace;
        }

        /**
         * Session ID from PHP
         * @return string
         */
        public static function getPHPID()
        {
            if (self::isActive())
            {
                return session_id();
            }
        }

        /**
         * Sets a session variable
         * @param string name of variable
         * @param mixed value of variable
         * @return void
         */
        public function __set($name, $value)
        {
            if (self::isActive())
            {
                $_SESSION[$this->_md5][$name] = $value;
            }
        }

        /**
         * Sets a session variable
         * @param string name of variable
         * @param mixed value of variable
         * @return void
         */
        public function set($name, $value)
        {
            $this->__set($name, $value);
        }

        /**
         * Sets a session variable
         * @param string name of variable
         * @param mixed value of variable
         * @return void
         */
        public static function globalSet($name, $value)
        {
            if (self::isActive())
            {
                $_SESSION[$name] = $value;
            }
        }

        /**
         * Remove a session variable
         * @param string name of variable
         * @return void
         */
        public static function globalDel($name)
        {
            if (self::isActive())
            {
                unset($_SESSION[$name]);
            }
        }

        /**
         * Fetches a session variable
         * @param string name of variable
         * @return mixed value of session varaible
         */
        public function __get($name)
        {
            if (!self::isActive())
            {
                return null;
            }

            if (isset($_SESSION[$this->_md5][$name]))
            {
                return $_SESSION[$this->_md5][$name];
            }

            return false;
        }

        /**
         * Fetches a session variable
         * @param string name of variable
         * @return mixed value of session varaible
         */
        public function get($name)
        {
            return $this->__get($name);
        }

        /**
         * Fetches all session data
         * @return array of session data
         */
        public function getAll()
        {
            if (!self::isActive())
            {
                return null;
            }

            return $_SESSION[$this->_md5];
        }

        /**
         * Fetches a session variable
         * @param string name of variable
         * @return mixed value of session varaible
         */
        public static function globalGet($name)
        {
            if (!self::isActive())
            {
                return null;
            }

            if (isset($_SESSION[$name]))
            {
                return $_SESSION[$name];
            }

            return false;
        }

        /**
         * Deletes a session variable
         * @param string name of variable
         * @return boolean
         */
        public function __unset($name)
        {
            if (isset($_SESSION[$this->_md5][$name]))
            {
                unset($_SESSION[$this->_md5][$name]);
                return true;
            }

            return false;
        }

        /**
         * DEPRECIATED - Use del() method
         * @param string name of variable
         * @return boolean
         */
        public function Delete($name)
        {
            return $this->__unset($name);
        }

        /**
         * Deletes a session variable
         * @param string name of variable
         * @return boolean
         */
        public function del($name)
        {
            return $this->__unset($name);
        }

        /**
         * Deletes a session variable
         * @param string name of variable
         * @return boolean
         */
        public function delAll()
        {
            $_SESSION[$this->_md5] = array();
            return true;
        }

        /**
         * Does the name exist?
         * @param string $name
         * @return boolean
         */
        public function __isset($name)
        {
            if (self::isActive() && isset($_SESSION[$this->_md5][$name]))
            {
                return true;
            }

            return false;
        }

        /**
         * Does the name exist?
         * @param string $name
         * @return boolean
         */
        public function Exists($name)
        {
            return $this->__isset($name);
        }
    }
}