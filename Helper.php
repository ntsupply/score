<?php

namespace Score
{
    use \Score\Strings;

    /**
     * Get Values from system, strings, and sources.
     * @author Ron Rebennack
     * @author Bradley Worrell-Smith
     */
    class Helper
    {
        const URL_FULL = false;
        const URL_BASE = true;
        const URL_PATH = 2;
        const URL_BASE_NOSVR = 1;

        const SANITIZE_STRIPTAGS = 1;
        const SANITIZE_URLDECODE = 2;

        /**
         * Register the global Helper functions
         */
        public static function Register($iPrefix = "Do")
        {
            static $registered;

            if ($registered !== true)
            {
                $GLOBALS[$iPrefix . "QQ"] = function ($iString, $iQuoteType = Strings::QUOTE_DOUBLE) {
                    return Strings::Quote($iString, $iQuoteType);
                };

                $GLOBALS[$iPrefix . "VEE"] = function ($iObj) {
                    echo(var_export($iObj, true));
                };

                $GLOBALS[$iPrefix . "ECO"] = function ($iObj) {
                    echo("\n<!-- " . var_export($iObj, true) . " -->");
                };

                $GLOBALS[$iPrefix . "VET"] = function ($iObj) {
                    return var_export($iObj, true);
                };

                $GLOBALS[$iPrefix . "IIF"] = function ($iCriteria, $iTrue, $iFalse) {
                    return ($iCriteria ? $iTrue : $iFalse);
                };

                $GLOBALS[$iPrefix . "BIF"] = function ($iCriteria, $iTrue) {
                    return ($iCriteria ? $iTrue : "");
                };

                $registered = true;
            }

            return $registered;
        }

        /**
         * Returns an array of the RGB value of the passed color
         * @param string $iColor Hex color
         * @return array [R,G,B]
         */
        public static function ColorHexToRBG($iColor)
        {
            if ($iColor[0] == "#")
            {
                $iColor = substr($iColor, 1);
            }

            if (strlen($iColor) == 6)
            {
                list($xRed, $xGrn, $xBlu) = array($iColor[0] . $iColor[1],
                    $iColor[2] . $iColor[3],
                    $iColor[4] . $iColor[5]);
            }
            elseif (strlen($iColor) == 3)
            {
                list($xRed, $xGrn, $xBlu) = array($iColor[0] . $iColor[0], $iColor[1] . $iColor[1], $iColor[2] . $iColor[2]);
            }
            else
            {
                return null;
            }

            $xRed = hexdec($xRed);
            $xGrn = hexdec($xGrn);
            $xBlu = hexdec($xBlu);

            return array($xRed, $xGrn, $xBlu);
        }

        /**
         * Get the Hex value of RGB values
         * @param int $iRed
         * @param int $iGreen
         * @param int $iBlue
         * @return string Hex value of color
         */
        public static function ColorRGBToHex($iRed, $iGreen = -1, $iBlue = -1)
        {
            if (is_array($iRed) && sizeof($iRed) == 3)
            {
                list($iRed, $iGreen, $iBlue) = $iRed;
            }

            $iRed = intval($iRed);
            $iGreen = intval($iGreen);
            $iBlue = intval($iBlue);

            $iRed = dechex($iRed < 0 ? 0 : ($iRed > 255 ? 255 : $iRed));
            $iGreen = dechex($iGreen < 0 ? 0 : ($iGreen > 255 ? 255 : $iGreen));
            $iBlue = dechex($iBlue < 0 ? 0 : ($iBlue > 255 ? 255 : $iBlue));

            $color = (strlen($iRed) < 2 ? '0' : '') . $iRed;
            $color .= (strlen($iGreen) < 2 ? '0' : '') . $iGreen;
            $color .= (strlen($iBlue) < 2 ? '0' : '') . $iBlue;

            return '#' . $color;
        }

        /**
         * Returns bool value reading strings as if they were bool values.  true/false
         * @param mixed $iValue
         * @return bool
         */
        public static function GetBool($iValue)
        {
            return self::GetBoolean($iValue);
        }

        /**
         * Returns bool value reading strings as if they were bool values.  true/false
         * @param mixed $iValue
         * @return bool
         */
        public static function GetBoolean($iValue)
        {
            if (empty($iValue))
            {
                return false;
            }

            if (is_bool($iValue))
            {
                return (bool) $iValue;
            }

            if (is_numeric($iValue))
            {
                return ($iValue == 1);
            }

            $iValue = strtolower($iValue);

            if ($iValue == "true" || $iValue == "y" || $iValue == "yes")
            {
                return true;
            }

            return false;
        }

        /**
         * Get the Root Domain of a full hostname
         * @param string $host Hostname (Ex: ssl.www.domain.com)
         * @return string
         */
        public static function RootDomain($host)
        {
            if (preg_match("/[^\.\/]+\.[^\.\/]+$/", $host, $matches));
            {
                return $matches[0];
            }

            return $host;
        }

        /**
         * Get the Current URL of page
         * @param mixed $urlOption Base Option Website
         * @return string
         */
        public static function CurrentPageURL($urlOption = self::URL_FULL)
        {
            $server = $_SERVER["HTTP_HOST"];
            if (empty($server))
            {
                $_SERVER["SERVER_NAME"];
            }
            $pageURL = "";
            $protocol = "http";

            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on")
            {
                $protocol .= "s";
            }

            if (isset($_SERVER["HTTP_X_FORWARDED_SERVER"]) && $_SERVER["HTTP_X_FORWARDED_SERVER"])
            {
                $server .= $_SERVER["HTTP_X_FORWARDED_SERVER"];
            }

            if ($urlOption !== self::URL_BASE_NOSVR)
            {
                $pageURL = "$protocol://$server" . ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != 443 ? ":" . $_SERVER["SERVER_PORT"] : '');
            }

            if ($urlOption == self::URL_FULL)
            {
                $pageURL .= $_SERVER["REQUEST_URI"];
            }
            else
            {
                $urlscript = str_replace('?' . $_SERVER["QUERY_STRING"], "", $_SERVER["REQUEST_URI"]);
                $offset = 0;

                if ($urlOption === self::URL_PATH)
                {
                    $offset = 1;
                }

                if (strpos($urlscript, "/", $offset) !== FALSE)
                {
                    $pageURL .= substr($urlscript, 0, strrpos($urlscript, "/") + $offset);
                }
                else
                {
                    $pageURL .= $urlscript;
                }
            }

            return $pageURL;
        }

        /**
         * Rewrite a URL with some smarts
         * @param string $location   What url to Rewrite
         * @param array $excludeQuery Exclude/Filter specific query vars (example: id=12345)
         * @param bool|array $includeQuery Append this to the URL (true=Include $_GET, false=Include None, array()=Include)
         */
        public static function RewriteUrl($location, $excludeQuery = array(), $includeQuery = true)
        {
            // Setup some defaults
            if (empty($location) === true)
            {
                $location = '//' . $_SERVER['HTTP_HOST'];
            }

            if ($includeQuery === true)
            {
                $vars = $_GET;
            }
            elseif (is_array($includeQuery))
            {
                $vars = $includeQuery;
            }
            else
            {
                $vars =  array();
            }

            // Breakout the pieces
            $components = parse_url(trim($location));

            if (empty($components['scheme']))
            {
                $components['scheme'] = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? "https" : "http");
            }

            if (empty($components["host"]))
            {
                $components["host"] = $_SERVER["HTTP_HOST"];
            }

            $url = $components['scheme'] . '://' . $components['host'] . $components['path'];

            // location urls and merge included vars
            if (!empty($components['query']))
            {
                $urlvars = array();
                parse_str($components['query'], $urlvars);
                $vars = array_merge($urlvars, $vars);
            }

            // Filter out vars
            if (!empty($excludeQuery))
            {
                foreach ($excludeQuery as $entry)
                {
                    if (isset($vars[$entry]))
                    {
                        unset($vars[$entry]);
                    }
                }
            }

            // Rebuild
            if (!empty($vars))
            {
                $url .= '?';
            }

            $url .= http_build_query($vars, '', '&');

            if (!empty($components['fragment']))
            {
                $url .= '#' . $components['fragment'];
            }

            return $url;
        }

        /**
         * Remove protocol like 'http:' or 'https:' from a url string
         * @param string $url
         * @return string
         */
        public static function StripURLScheme($url)
        {
            return str_replace(parse_url($url, PHP_URL_SCHEME) . ":", "", $url);
        }

        /**
         * Not sure why/if this function is needed 
         * @param mixed $iKey
         * @param mixed $iValue
         * @return array value of key and value
         */
        public static function OutKeyValue($iKey, $iValue)
        {
            return array($iKey => $iValue);
        }

        /**
         * Calculate the Age based on a pass date
         * @param string $iDate Date string in format of YYYY-MM-DD
         * @return int
         */
        public static function CalcAge($iDate)
        {
            list($xYear, $xMon, $xDay) = explode("-", $iDate);

            $xDay = date("d") - $xDay;
            $xMon = date("m") - $xMon;
            $xYear = date("Y") - $xYear;

            if ($xMon < 0)
                $xYear--;
            elseif (($xMon == 0 ) && ($xDay < 0))
                $xYear--;

            return $xYear;
        }

        /**
         * Stack the value to the variable passed. If not set, then set. If set,
         * then check if an array, then make array if need and append;
         * @param mixed $iVar   Base variable changed by reference
         * @param mixed $iValue New value to stack into $iVar
         * @param string|int $iIndex Unique Name/Number if $iVar is array
         * @return void
         */
        public static function StackValue(&$iVar, $iValue, $iIndex = null)
        {
            if (!empty($iIndex) && is_array($iVar)) // $iVar must be Assoc Array so do differently
            {
                
                if (array_key_exists($iIndex, $iVar) && !empty($iVar[$iIndex]))
                {
                    if (!is_array($iVar[$iIndex]))
                    {
                        $iVar[$iIndex] = array($iVar[$iIndex]);
                    }

                    $iVar[$iIndex][] = $iValue;
                }
                else
                {
                    $iVar[$iIndex] = $iValue;
                }

                return;
            }

            // Do for normal $iVar
            if (!empty($iVar))
            {
                if (!is_array($iVar))
                {
                    $iVar = array($iVar);
                }

                $iVar[] = $iValue;
            }
            else
            {
                $iVar = $iValue;
            }
        }

        /**
         * Covert object to Array Recursively
         * @param mixed $object
         * @return array
         * @author tbobker http://www.sitepoint.com/forums/showthread.php?438748-convert-object-to-array&s=2220b5071904a4cc20c11e52bf4a279d&p=4573332&viewfull=1#post4573332
         */
        public static function ObjectToArray($object)
        {
            if(!is_object($object) && !is_array($object))
            {
                return $object;
            }

            if(is_object($object) )
            {
                $object = get_object_vars($object);
            }

            return array_map(__METHOD__, $object);
        }

        /**
         * Get the Constants defined in a class
         * @param mixed $iClass Class object or string of class name
         * @param string $iBegins Constant names that begin with specific string
         * @return bool|array False if error or Array of Constants
         */
        public static function GetClassConstants($iClass, $iBegins = null)
        {
            if (is_object($iClass))
            {
                $iClass = get_class($iClass);
            }

            $reflect = new \ReflectionClass($iClass);
            $constants = $reflect->getConstants();

            if (!empty($iBegins))
            {
                foreach ($constants as $name => $value)
                {
                    if (substr($name, 0, strlen($iBegins)) != $iBegins)
                    {
                        unset($constants[$name]);
                    }
                }
            }

            return $constants;
        }

        /**
         * Sanitize data to prevent XSS and eval injections
         * @param mixed $data
         * @param int $options
         */
        public static function Sanitize(&$data, $options = 3)
        {
            if(is_array($data))
            {
                foreach ($data as &$item)
                {
      	            self::Sanitize($item);
                }
            }
        	else
            {
                if (($options & self::SANITIZE_URLDECODE) != 0)
                {
                    $data = urldecode($data);
                }

                if (($options & self::SANITIZE_STRIPTAGS) != 0)
                {
                    $data = strip_tags($data);
                }

                $data = htmlentities($data, ENT_QUOTES, 'UTF-8');
            }
        }

        /**
         * Stupid function that I didn't want to loose that takes a maximum number and returns a new
         * maximum that is rounded up and tick rate that is used for scaling to the new max
         * @param mixed $maxNum
         * @return array  0=>Max,1=Tickrate
         */
        public static function getScale($maxNum)
        {
            $tick = pow(10, floor(log10($maxNum)));
            $max = ceil($maxNum / $tick) * $tick;

            return array($max, $tick);
        }

        /**
         * Didn't think I would have to keep doing this type of browser checking
         * but thanks to Doug, I found this on StackOverflow
         * @link http://stackoverflow.com/questions/5302302/php-if-internet-explorer-6-7-8-or-9/#11741586 IE PHP version check
         * @return boolean|int
         */
        public static function getIEVersion()
        {
            $version = false;

            preg_match('%MSIE (.*?);%ix', $_SERVER['HTTP_USER_AGENT'], $matches);

            if(count($matches) < 2)
            {
                preg_match('%Trident/\d{1,2}\.\d{1,2};\srv:(\d{1,2}\.\d{1,2})%six', $_SERVER['HTTP_USER_AGENT'], $matches);
            }

            if (count($matches) > 1)
            {
                //Then we're using IE
                return trim($matches[1]);
            }

            return false;
        }
    }
}
